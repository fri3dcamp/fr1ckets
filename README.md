### Setup

When running with a missing/empty database, the database (`db/fr1ckets.db`) is created and loaded with whatever is in `app/src/fr1ckets.sql` upon booting.

To load initial data (products, a fallback client) into the database, the `snippets/load_initial_data.py` program talks to a live server containing an empty database, and can be used as such:
```
$ cd snippets
$ ./load_initial_data.py 
need either a products or a clients file, or both
usage: ./load_initial_data.py [-b api_base] [-p file.json] [-c file.json] [-v file.json]
  -b/--base api_base		url to use as api base (default http://localhost:10000)
  -p/--products file.json	load products from specified file
  -c/--clients file.json	load clients from specified file
  -v/--vouchers file.json	load vouchersfrom specified file
$ ./load_initial_data.py -p products.json -c clients.json -v vouchers.json
loaded 10 products
loaded 2 clients
loaded 3 vouchers
$ 
```

### Tests

Testing lives in `app/src/tests.py`. To run:

```
docker compose exec app /src/tests.py
```

(Tests set up a temporary database to run in, and load from `app/src/fr1ckets.sql`. Tests should not need stuffed data, but supply their own.)

### Local preview

Run `docker compose up` om de applicatie te starten.

`docker-compose up --build` om te rebuilden en te starten

Site is dan bereikbaar op [http://localhost:10000/shop/](http://localhost:10000/shop/)

Om templates en src live geüpdate te hebben, moet er een docker-compose.override.yml zijn met volgende inhoud:

```
services:
  app:
    volumes:
      - ./app/src:/src:ro
      - ./db:/db:rw
    command: python /src/wsgi.py
```

### Upkeep

When dusting this thing off, some things to bump are:

* pegging the app Dockerfile to a newer Python

### Python venv for running snippets

`python3.9 -m venv _venv --upgrade-deps`

Then `source _venv/bin/activate` and `pip install -r requirements.txt`

### Misc

#### DB maintenance

There are some scripts in `./scripts` which'll parse the relevant config knobs and talk to the current database that the app is using, which should follow any db changes in the future:
* `./scripts/db_shell.sh` drops you into a shell,
* `./scripts/db_backup.sh` .dumps to stdout,
* `./scripts/db_restore.sh` removes the current database, and fills it from stdin.

Currently `./db` is a bind mount containing just an SQLite database, so all the common stanzas apply:
* `sqlite3 db/fr1ckets.db .dump > my_backup.sql`
* `sqlite3 db/fr1ckets.db < db/fr1ckets.sql`
* etc
