#!/usr/bin/env bash
set -eu

FILE=`mktemp`

die() {
	rm $FILE
}
trap die EXIT TERM INT

cat > $FILE <<BOOM
{
  "name" : "a tshirt",
  "description" : "garment, +1 INT",
  "price" : 1234,
  "variants" : [
    {
      "name" : "size",
      "description" : "]radius of belly, radius of universe[",
      "options" : [
        {
          "name" : "large",
          "slug" : "L",
          "price_modifier" : 100
        },
        {
          "name" : "small",
          "slug" : "S",
          "price_modifier" : -100
        }
      ]
    }
  ]
}
BOOM

cat -n $FILE
jq < $FILE

curl -X POST -H 'Content-type: application/json' --data-binary  @$FILE http://localhost:10000/api/admin/products

