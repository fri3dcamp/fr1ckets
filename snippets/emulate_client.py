#!/usr/bin/env python3
# a demo client making a purchase
# fr1ckets, the fri3dcamp devs, 2022
import requests
import getopt
import sys
from json import dumps, loads
from string import ascii_letters
from random import choice

BASE = None
VERBOSE = False
EMAIL = None


def usage():
	print("usage: {sys.argv[0]} -b base_api_url [ -v ] [ -e email ]")
	print("  -b/--base    base URL of API, e.g. http://localhost:8080/")
	print("  -v/--verbose be verbose")
	print("  -e/--email   email address to use, random by default")


def post(endpoint, json=None):
	url = f"{BASE}{endpoint}"
	if VERBOSE:
		print("")
		print(f"=== {url} ===")
		if (json):
			print(">>> in >>>")
			print(dumps(json, indent='  '))

	rep = requests.post(url, json=json)

	if VERBOSE:
		print(f"{url} {rep.status_code}")
		print("<<< out <<<")
		if (rep.status_code // 100) == 2:
			print(f"{dumps(rep.json(), indent='  ')}")
			print("")
		else:
			print(f"{rep.text}")

	return rep.json()

class ComplexPrice:
	def __init__(self, p={}):
		self.p = p

	def __add__(self, other):
		me = self.p
		you = other.p
		return ComplexPrice({
			k: (me[k] if k in me else 0) + (you[k] if k in you else 0)
			for k in set(me.keys()) | set(you.keys())
		})

	def total(self):
		return sum(self.p.values())

	def dict(self):
		return self.p

def main():
	try:
		opts, args = getopt.getopt(
			sys.argv[1:],
			"b:ve:",
			["base", "verbose", "email"]
		)
	except getopt.GetoptError as err:
		print(err)
		usage()
		sys.exit(1)

	global BASE, VERBOSE, EMAIL

	for o, a in opts:
		if o in ('-b', '--base'):
			BASE = a
		elif o in ('-v', '--verbose'):
			VERBOSE = True
		elif o in ('-e', '--email'):
			EMAIL = a
		else:
			print(f"unknown option {o}")
			sys.exit(1)

	if not BASE:
		usage()
		sys.exit(1)

	if not EMAIL:
		EMAIL = ''.join([choice(ascii_letters) for _ in range(16)])
		EMAIL = f"{EMAIL}@client.invalid"

	menu = post("/api/shop/get", json={'email': EMAIL})

	if menu['client']['can_order'] is False or len(menu['products']) == 0:
		print("empty?")
		sys.exit(1)

	order = {
		'disclaimer_have_accompanying_adult': True,
		'disclaimer_be_excellent': True,
		'disclaimer_animal': True,
		'email': EMAIL,
		'notes': 'have some notes',
		'products': [],
	}

	order_total = 0
	for product in menu['products']:
		for i in range(choice([ 1, 2 ])):
			out = {
				'product_handle': product['handle'],
				'variants': [],
				'fields': [],
			}
			price = ComplexPrice(product['price'])

			for variant in product['variants']:
				random_option = choice(variant['options'])
				out['variants'].append({
					'name': variant['name'],
					'value': random_option['slug'],
				})
				price += ComplexPrice(random_option['price_modifier'])

			for field in product['fields']:
				print(field)
				if field['required'] is False:
					if choice([True, False]):
						value = f""
					else:
						value = f"my {field['name']} is {''.join([choice(ascii_letters) for _ in range(4)])}"
				else:
					value = f"my {field['name']} is {''.join([choice(ascii_letters) for _ in range(4)])}"
				out['fields'].append({
					'name': field['name'],
					'value': value,
				})

			order['products'].append(out)
			#order['vouchers'] = ['voucher_camper', 'voucher_ticket']
			order_total += price.total()

			#print(f"price: {price:03d} {out}")

	if VERBOSE:
		print(f"i am ordering these items for a total of {order_total}")
		for o in order['products']:
			p = list(filter(lambda p: p['handle'] == o['product_handle'], menu['products']))[0]
			print(f" * {p['description']}:")
			for v in o['variants']:
				print(f"   for variant {v['name']} the value {v['value']}")
			for f in o['fields']:
				print(f"   for field {f['name']} the value {f['value']}")

	quote = post('/api/shop/quote', json=order)
	if VERBOSE:
		print(f"i quoted these items for a total of {quote['total']}")
		for d in quote['products']:
			print(f" * {d['description']} for {ComplexPrice(d['price']).total()}:")
			for v in d['variants']:
				print(f"   for variant {v['name']} the value {v['value']}")
			for f in d['fields']:
				print(f"   for field {f['name']} the value {f['value']}")


	session = post('/api/shop/order', json=order)
	#print(f"total price: {order_total}")
	#session = post('/api/shop/session', json=session_handle)

	if VERBOSE:
		print(f"i ordered these items for a total of {session['total']}")
		for d in session['products']:
			print(f" * {d['description']} for {ComplexPrice(d['price']).total()}:")
			for v in d['variants']:
				print(f"   for variant {v['name']} the value {v['value']}")
			for f in d['fields']:
				print(f"   for field {f['name']} the value {f['value']}")

	if VERBOSE:
		print("comparing the quote to the purchase:")
		print(f"the amount looks {'ok' if quote['total'] == session['total'] else 'BAD'}")
		print(f"the number of items looks {'ok' if len(quote['products']) == len(session['products']) else 'BAD'}")


	if VERBOSE:
		print("comparing the purchase to my own count:")
		print(f"the amount looks {'ok' if order_total == session['total'] else 'BAD'}: {order_total} {session['total']}")
		print(f"the number of items looks {'ok' if len(order['products']) == len(session['products']) else 'BAD'}")






if __name__ == '__main__':
	main()
