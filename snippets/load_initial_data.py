#!/usr/bin/env python3
import json
import requests
import sys
import getopt


FILE_PRODUCTS = None
FILE_CLIENTS = None
FILE_VOUCHERS = None
FILE_SHIFTS = None
BASE = 'http://localhost:10000'


def usage():
	print(f"usage: {sys.argv[0]} [-b api_base] [-p file.json] [-c file.json]")
	print(f"  -b/--base api_base		url to use as api base (default {BASE})")
	print("  -p/--products file.json	load products from specified file")
	print("  -c/--clients file.json	load clients from specified file")
	print("  -v/--vouchers file.json	load vouchers from specified file")
	print("  -s/--shifts file.json	load community shifts from specified file")


def main():
	try:
		opts, args = getopt.getopt(
			sys.argv[1:],
			"b:p:c:v:s:",
			["base", "products", "clients", "vouchers", "shifts"]
		)
	except getopt.GetoptError as err:
		print(err)
		usage()
		sys.exit(1)

	global FILE_PRODUCTS
	global FILE_CLIENTS
	global FILE_VOUCHERS
	global FILE_SHIFTS
	global BASE

	for o, a in opts:
		if o in ('-b', '--base'):
			BASE = a
		elif o in ('-p', '--products'):
			FILE_PRODUCTS = a
		elif o in ('-c', '--clients'):
			FILE_CLIENTS = a
		elif o in ('-v', '--vouchers'):
			FILE_VOUCHERS = a
		elif o in ('-s', '--shifts'):
			FILE_SHIFTS = a
		else:
			print(f"unknown option {o}")
			usage()
			sys.exit(1)

	if not (FILE_PRODUCTS or FILE_CLIENTS or FILE_VOUCHERS or FILE_SHIFTS):
		print("need at least one file specified")
		usage()
		sys.exit(1)

	if FILE_PRODUCTS:
		products = []
		with open(FILE_PRODUCTS) as f:
			products = json.load(f)
			for product in products:
				requests.post(f'{BASE}/api/admin/products', json=product)
		print(f"loaded {len(products)} products")

	if FILE_CLIENTS:
		clients = []
		with open(FILE_CLIENTS) as f:
			clients = json.load(f)
			for client in clients:
				requests.post(f'{BASE}/api/admin/clients', json=client)
		print(f"loaded {len(clients)} clients")

	if FILE_VOUCHERS:
		vouchers = []
		with open(FILE_VOUCHERS) as f:
			vouchers = json.load(f)
			for voucher in vouchers:
				ret = requests.post(f'{BASE}/api/admin/vouchers', json=voucher)
				if (ret.status_code // 100) != 2:
					print(f"{ret.status_code} error creating voucher {voucher}: {ret.text}")
		print(f"loaded {len(vouchers)} vouchers")

	if FILE_SHIFTS:
		shifts = []
		with open(FILE_SHIFTS) as f:
			shifts = json.load(f)
			for shift in shifts:
				ret = requests.post(f'{BASE}/api/admin/shifts', json=shifts)
				if (ret.status_code // 100) != 2:
					print(f"{ret.status_code} error creating voucher {voucher}: {ret.text}")
		print(f"loaded {len(shifts)} shifts")


	sys.exit(0)

if __name__ == '__main__':
	main()
