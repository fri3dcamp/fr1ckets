#!/bin/sh
# nets a shell on the database
# fr1ckets, the fri3dcamp devs, 2023
set -eu

DDIR=`dirname $0`/../
. $DDIR/env

docker compose -f $DDIR/docker-compose.yml run app sqlite3 $APP_DB_PATH
