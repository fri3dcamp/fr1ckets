#!/bin/sh
# dump the database to stdout
# fr1ckets, the fri3dcamp devs, 2023
set -eu

DDIR=`dirname $0`/../
. $DDIR/env

docker compose -f $DDIR/docker-compose.yml run -T app sqlite3 $APP_DB_PATH .dump
