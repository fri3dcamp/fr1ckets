#!/bin/sh
# load the database from stdin
# fr1ckets, the fri3dcamp devs, 2023
set -eu

DDIR=`dirname $0`/../
. $DDIR/env

# nuke and rebuild is fishy, but .dump doesn't drop tables
docker compose -f $DDIR/docker-compose.yml run -T app rm $APP_DB_PATH
docker compose -f $DDIR/docker-compose.yml run -T app sqlite3 $APP_DB_PATH < /dev/stdin
