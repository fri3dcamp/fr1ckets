/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
      "./app/src/fr1ckets/templates/*.{html,jinja}",
      "./app/src/fr1ckets/templates/**/*.{html,jinja}",
      "./app/src/fr1ckets/templates/**/**/*.{html,jinja}",
      "./app/src/fr1ckets/static/community/**/*.{html,js}"
  ],
  theme: {
    extend : {
        backgroundImage: {
            'pattern': "url('/static/img/headerimg_2024.svg')",
            'logo_white': "url('/static/img/logo_white.svg')",
			'icon_calendar': "url('/static/img/noun-calendar-2080597.svg')",
			'icon_ticket': "url('/static/img/noun-ticket-6348808.svg')",
			'icon_star': "url('/static/img/noun-star-6524459.svg')",
			'icon_check': "url('/static/img/noun-check-2066975.svg')",
			'icon_food': "url('/static/img/noun-restaurant-9502.svg')",
			'icon_note': "url('/static/img/noun-note-2036469.svg')",
			'icon_person': "url('/static/img/noun-person-6372680.svg')",
			'icon_person_white': "url('/static/img/noun-person-6372680-white.svg')",
			'icon_envelope': "url('/static/img/noun-envelope-1248432.svg')",
			'icon_invoice': "url('/static/img/noun-invoice-6574679.svg')",
			'icon_trash': "url('/static/img/noun-trash-6738761.svg')",
			'icon_trash_white': "url('/static/img/noun-trash-6738761-white.svg')"
        },
        colors: {
            'fri3d_A': 'rgba(136,53,201, 1)',
            'fri3d_B': 'rgba(255,173,100, 1)',
            'fri3d_B_light': '#ffe3ca',
            'fri3d_A_light': 'rgba(192,133,255, 1)',
            'fri3d_C': 'rgba(60, 232, 179, 1)',
            'fri3d_C_dark': 'rgba(47,173,131, 1)',
            'fri3d_red' : 'rgba(255,62,62,1)',
            'fri3d_darkgrey' : 'rgba(45,45,45,1)'
        },
    },
  },
  plugins: [
  ],
}
