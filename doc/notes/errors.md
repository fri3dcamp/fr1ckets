The code here and there throws errors upon shenanigans (say an OrderNotPossibleError for
a cart which is empty or malformed). Currently flask-restful just matches the name of
the exception to an entry in the errors dict (app/src/fr1ckets/__init__.py) and returns
what it finds there. This does not allow for dynamic errors (with dynamic errors which
point out exactly what went wrong).

Errors which could do with dynamic messages  could come from either our logic or the
marshmallow validation. This might be solved with Werkzeug's HTTPException and then
meshing that with marshmallow, but leaving this for when there is a need.

Note that running with "python /src/wsgi.py" (from e.g. a docker-compose.override.yml)
will not load show these errors, it'll just show the stack trace. And the testing code
just sees the actual Exception in any case.
