## general

Calls are given arguments as JSON in the call body (Content-Type: application/json).

Calls return data as JSON in the response body.

Errors are reported as JSON too, with a non-2xx coded HTTP reply. They follow this format:
```
{
  'code': <int, the http status code (same as the http response>,
  'message': <string, which error this is, lookupable>,
  'error': <object, only used in some errors>,
}
```

Whenever a monetary value is placed an integer is used, since such amounts are assumed to be expressed in cents.

## shop

The shop is reached at `/api/shop/*`.

### `/api/shop/get`

Get the shop and its products, depending on the supplied email address.

Accepts:
```
{
  'email' : <string, email address>,
}
```

Returns:
```
{
  'client' : {
    'email' : <string, the supplied email address>,
    'can_order' : <bool, can this email place an order currently>,
    'can_order_from' : <string, ISO 8601, UTC, point in time from which this email can place an order>,
  },
  'products' : [
    <first product, see below>,
    <second product>,
    ...,
  ],
}
```

The email does not have to exist in database. If it is not found, the fallback is used for timing information.

Products will contain prices, they are structured as follows:
```
{
  'vat_rate': <integer price at this vat rate, vat included, in cents, can be negative!>,
  ...
}
```
With `vat_rate` a stringified number representing the VAT rate.

Products look like this:
```
{
  'handle' : <string, unique key>,
  'name' : <string, name of the product>,
  'price' : <price as discussed above>,
  'description' : <string, human-readable description of product>,
  'category' : <string, a category indicating vouchers of this category apply to this product>,
  'variants': [
    {
      'name' : <string, name of the variant>,
      'description' : <string, human-readable description of this variant>,
      'options' : [
        {
          'name' : <string, name of this option>,
          'slug' : <string, **WHAT IS THIS?**>,
          'price_modifier' : <price as discussed above, modification of this choice on base price>,
        },
        ...
      ],
      'fields': [
        {
          'name' : <string, name of this field>,
          'description' : <string, human-readable description of this field>,
          'required' : <boolean, required fields cannot be left blank>,
        },
        ...
      ],
    },
    ...
  ],
}
```

An annotated example:
```
{
  "handle": "01HH0XEQ4NMWQ9TJ7BB2PA8T91",
  "name": "ticket",
  "price": {        // our base ticket price is 100€ (NON-REALISTIC EXAMPLE)
    '12': 2000,     // 20€ of which is VATted at 12%
    '21': 8000      // and 80€ at 21% 
  },
  "description": "a ticket for Fri3d Camp",
  "category": "ticket",
  "variants": [
    {
      "name": "bits",
      "description": "this ticket grants access to Fri3d Camp for one person",
      "options": [
        {
          "name": "0-3 years",
          "slug": "3bit",
          "price_modifier": { // basically a 20€ ticket:
            '12' : -2000,     // base is 20€ at 12%, this modifier is -20€, so 0€
                              // because kids this young don't eat bbq (as an example)
            '21' : -6000,     // base is 80€ at 21%, this modifier is -60€, so 20€
          }
        },
        {
          "name": "3-6 years",
          "slug": "4bit",
          "price_modifier": {
          "price_modifier": { // basically a 30€ ticket:
            '12' : -1000,     // base is 20€ at 12%, this modifier is -10€, so 10€
            '21' : -6000,     // base is 80€ at 21%, this modifier is -60€, so 20€
          }

        },
        {
          "name": "6-12 years",
          "slug": "5bit",
          "price_modifier": { // basically a 40€ ticket:
            '12' : -1000,     // base is 20€ at 12%, this modifier is -10€, so 10€
            '21' : -5000,     // base is 80€ at 21%, this modifier is -50€, so 30€
          }
        },
        {
          "name": "12-25 years",
          "slug": "6bit",
          "price_modifier": { // basically a 60€ ticket:
                              // note, base is 20€ at 12%, we're not modifying that here
            '21' : -6000,     // base is 80€ at 21%, this modifier is -20€, so 60€
          }

        },
        {
          "name": "+25 years",
          "slug": "7bit",
          "price_modifier": {
            // no updates, this is base price
          }
        },
        {
          "name": "business",
          "slug": "8bit",
          "price_modifier": { // price doubling, but only via the 21% part
            // not touching the 12% part, leaving that at 20€
            '21' : 10000      // add 100€ to base 80€ at 21%
          }
        }
      ]
    },
    {
      "name": "food",
      "description": "food preferences",
      "options": [
        {
          "name": "omnivore",
          "slug": "omnivore",
          "price_modifier": {}  // no price modifier, but you could have multiple variants doing that
        },
        {
          "name": "vegetarian",
          "slug": "vegetarian",
          "price_modifier": {}
        }
      ]
    }
  ],
  "fields": [
    {
      "name": "name",
      "description": "required by the camping ground",
      "required": true
    },
    {
      "name": "notes",
      "description": "anything we can help this person with, f.e. dietary restrictions",
      "required": false
    }
  ]
}
```

### `/api/shop/voucher`

Check a voucher.

Accepts:
```
{
  'code' : <string with voucher code>,
}
```

Returns:
```
{
  'found' : <bool, true if found, false if claimed or not found>,
  'applicable_to' : <string, product[].category of products that this voucher applies to ("missing" if unspecified)>,
}
```


### `/api/shop/order`

Place an order.

Accepts:
```
{
  'email' : <string, email address>,
  'notes': <string, order notes from customer>,
  'disclaimer_pay_on_time' : <boolean, needs to be true>,
  'disclaimer_have_accompanying_adult' : <boolean, needs to be true>,
  'disclaimer_be_excellent' : <boolean, needs to be true>,
  'vouchers' : [    // may be empty
    <string containing a voucher>,
    <string containing another voucher>,
    ...
  ],
  'products' : [
    {
      'product_handle' : <string, a product handle as returned by /api/shop/get>,
      'variants' : [
        {
          'name' : <string, name of the variant we are choosing an option for>,
          'value' : <string, name **OR SLUG??** of the chosen option for this variant>,
        },
        ...
      ],
      'fields' : [
        {
          'name' : <string, name of the field we are filling in here>,
          'value' : <string, value for this field, if the field is required this must have nonzero length>,
        },
        ...
      ],
    },
    ...
  ],
}
```

Possible error messages are:
 * EBADVALUES, found bad values while parsing the request, more information in the extra field
 * EDISCLAIMERS, all needed disclaimers where not found or not set to true
 * ETOOSOON, too soon for this email to place an order
 * ECARTEMPTY, there were no products listed in the request
 * EVOUCHERBAD, one (or more) vouchers in the order were not useable, see below
 * EPRODUCTUNKNOWN, the referenced product does not exist, the offending handle is in the extra field,
   thrown on the first such encountered product
 * EVARIANTUNKNOWN, an unknown product variant was encountered
 * EVARIANTNEEDED, a product did not mention a needed variant choice
 * EFIELDNEEDED, a required field was not filled in

For the EVOUCHERBAD error, it will look like this;
```
{
  'code' : 400,
  'message': 'EVOUCHERBAD',
  'extra': {
    'expired': [    // only exists if it has contents
      <code of expired voucher>,
      ...
    ],
    'notfound': [   // only exists if it has contents
      <code of not found voucher>,
      // practically there will only be one code in here, the first one to cause
      // an exception, yell at me if you need this listed out like the others
      ...
    ],
    'superfluous':  // only exists if it has contents
      <code of voucher for a product not in this order/quote>,
      ...
    ],
  },
}
```

Returns a structure containing payment total and an overview of ordered items, each containing specific
fields (mostly coming from the product description) needed to show it to the user.
```
{
  'handle': <string, session handle>,
  'nonce': <string, nonce usable for further linking to this session>,
  'notes': <string, order notes from customer>,
  'total': <integer, total cost of order>,
  'payment_code': <string, a "structured communication" to be specified in bank transfer, just the actual code>,
  'payment_code_human': <string, a "structured communication" to be specified in bank transfer, in +++.../.../...+++ format>,
  'products': [
    {
      'product_handle': <string, handle of product referenced here, for further lookups>,
      'name': <string, name of the referenced product>,
      'description': <string, description of the referenced product>,
      'price': <integer, in cents, price of this item after variant modifications>,
      'voucher': <a string containig the voucher used for this (price will be zero), or none if none>,
      'variants': [
        {
          'name': <string, name of the variant in question>,
          'description': <string, description of the variant in question>,
          'value': <string, the slug of the chosen product variant choice>,
        },
        ...
      ],
      'fields': [
        {
          'name': <string, name of the field in question>,
          'description': <string, description of the field in question>,
          'value': <anything, which was supplied by user>,
        },
        ...
      ],
    },
    ...
  ],
}
```

An example, based on the example product ticket description above, our client example@example.invalid here will order two such tickets, one business and one adult:
```
{
  "email": "example@example.invalid",
  "notes": "and we will bring a cake to the infodesk",
  "products": [
    {
      "product_handle": "01HH0XEQ4NMWQ9TJ7BB2PA8T91",
      "variants": [
        {
          "name": "bits",
          "value": "business"
        },
        {
          "name": "food",
          "value": "omnivore"
        }
      ],
      "fields": [
        {
          "name": "name",
          "value": "some name"
        },
        {
          "name": "notes",
          "value": ""
        }
      ]
    },
    {
      "product_handle": "01HH0XEQ4NMWQ9TJ7BB2PA8T91",
      "variants": [
        {
          "name": "bits",
          "value": "+25 years"
        },
        {
          "name": "food",
          "value": "vegetarian"
        }
      ],
      "fields": [
        {
          "name": "name",
          "value": "some other name"
        },
        {
          "name": "notes",
          "value": "please let my food be gluten free"
        }
      ]
    },
  ]
}
```

### `/api/shop/quote`

This call is a direct copy of the order call above, but does not persist to database.

It accepts the same input as order.

It provides the same output as order, **except** it does not include the 'handle', 'payment_code', 'payment_code_human' and 'nonce' fields, since there is no session created.

### `/api/shop/session/qr/<session_nonce>`

This call generates an SVG containing a SEPA QR payment code for this session.

It returns a string containing `<svg...>...</svg>` as content type `image/svg+xml`. It can be used as the src in an img tag.

A test endpoint for this (which needs no session handle) is `/api/shop/session/qrtest`. It will transfer €0.20 to us.

If the session is not found, a standard error will be returned (message='ENOTFOUND').
