# main Flask app definition
# fr1ckets, the fri3dcamp devs, 2022
from flask import Flask
from flask_restful import Api
from flask_mail import Mail
from flask_basicauth import BasicAuth
from flask_login import LoginManager
from celery import Celery
from werkzeug.exceptions import HTTPException

app = Flask(__name__)
app.config.from_object('config')

api_app = Api(app)
mail_app = Mail(app)
basic_auth_app = BasicAuth(app)
login_manager = LoginManager()
login_manager.init_app(app)

celery_app = Celery(app.import_name)
celery_app.config_from_object('config', namespace='CELERY')


class ContextTask(celery_app.Task):
	def __call__(self, *args, **kwargs):
		with app.app_context():
			return self.run(*args, **kwargs)

celery_app.Task = ContextTask

class ApiError(HTTPException):
	code = 400
	data = {}


	def __init__(self, message='reticulating splines failed', extra=None):
		self.data['code'] = self.code
		self.data['message'] = message
		self.data['extra'] = extra
		super().__init__()

# import stuff that needs an app object
from fr1ckets.model import setup as model_setup
from fr1ckets.api import setup as api_setup, shop, util, orga, community
from fr1ckets.view import shop, community, orga

model_setup.db_check()
