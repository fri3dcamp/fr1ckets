# routines to populate/send mails
# fr1ckets, the fri3dcamp devs, 2024
from flask import render_template, url_for
from fr1ckets import app, ApiError
from fr1ckets.background import tasks
from fr1ckets.model import client, product, conto, session, voucher
from fr1ckets.util import helper
import html2text


def session_created(_session):

	_client = client.get(_session['client_handle'])

	_session.update({
		'days_max': app.config['SESSION_EXPIRING_DAYS'],
		'payment_account': app.config['OUR_BANK_ACCOUNT'],
		'email': _client['email'],
	})

	mail = {}
	mail['subject'] = render_template('mails/order/subject')

	tplvars = helper.get_tpl_vars('session',_session['nonce'],'session')
	tplvars = tplvars['sessions'][0]
	tplvars['client_nonce'] = _client['nonce']

	html = render_template('mails/order/body.jinja', **tplvars)
	text = html2text.html2text(html)

	mail['html'] = render_template('mails/html.jinja', content=html)
	mail['text'] = render_template('mails/text.jinja', content=text)

	if _client['email'].lower().endswith('.invalid'):
		# skip these testing addresses
		app.logger.debug(f"not mailing to this address: {_client['email']}")
		return

	tasks.mail.delay(
		addr_from=app.config['MAIL_ADDRESS_FROM'],
		addr_to=_client['email'],
		subject=mail['subject'],
		content_text=mail['text'],
		content_html=mail['html']
	)


def session_fully_paid(_session):

	_client = client.get(_session['client_handle'])

	_session.update({
		'email': _client['email'],
	})

	mail = {}
	mail['subject'] = render_template('mails/fully_paid/subject')

	tplvars = helper.get_tpl_vars('session',_session['nonce'],'session')
	tplvars = tplvars['sessions'][0]
	tplvars['client_nonce'] = _client['nonce']

	html = render_template('mails/fully_paid/body.jinja', **tplvars)
	text = html2text.html2text(html)

	mail['html'] = render_template('mails/html.jinja', content=html)
	mail['text'] = render_template('mails/text.jinja', content=text)

	if _client['email'].lower().endswith('.invalid'):
		# skip these testing addresses
		app.logger.debug(f"not mailing to this address: {_client['email']}")
		return

	tasks.mail.delay(
		addr_from=app.config['MAIL_ADDRESS_FROM'],
		addr_to=_client['email'],
		subject=mail['subject'],
		content_text=mail['text'],
		content_html=mail['html']
	)

def payment_reminder(_client):

	mail = {}
	mail['subject'] = render_template('mails/payment_reminder/subject')

	html = render_template('mails/payment_reminder/body.jinja',
		client=_client,
		timeout_days=app.config['REMINDER_TIMEOUT_DAYS'])#**tplvars)
	text = html2text.html2text(html)

	print(text)

	mail['html'] = render_template('mails/html.jinja', content=html)
	mail['text'] = render_template('mails/text.jinja', content=text)

	if _client['email'].lower().endswith('.invalid'):
		# skip these testing addresses
		app.logger.debug(f"not mailing to this address: {_client['email']}")
		return

	tasks.mail.delay(
		addr_from=app.config['MAIL_ADDRESS_FROM'],
		addr_to=_client['email'],
		subject=mail['subject'],
		content_text=mail['text'],
		content_html=mail['html']
	)
