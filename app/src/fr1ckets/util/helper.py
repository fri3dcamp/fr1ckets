# General helper functions, mainly for views

from fr1ckets.model import product
from fr1ckets.model import conto
from fr1ckets.model import client
from fr1ckets.model import logic
import datetime
import os
import markdown
from fr1ckets.view import conto as conto_view
from flask import url_for


def humanize_payment_code(code):
	return f'+++{code[0:3]}/{code[3:7]}/{code[7:12]}+++'


def dehumanize_payment_code(code):
	return code.replace('+', '').replace('/', '')


def makeVATString(prices):
	out = "incl. BTW"
	actualprices = {}
	for k,v in prices.items():
		if v > 0:
			actualprices[k] = v
	if not actualprices:
		return out
	if 1 == len(actualprices):
		rate = list(actualprices.keys())[0]
		return f"incl. {rate}% BTW"
	vatrates = []
	for k,v in actualprices.items():
		p = v/100
		vatrates.append(f"€ {p} à {k}%")
	vatstr = ", ".join(vatrates)
	return f"{out} ({vatstr})"

def VATratesToSingleValue(prices):
	out = 0
	for v in prices.values():
		out += v
	return out

def readable_longstring(value):
	chunk = 4
	return '-'.join([value[0+i:chunk+i] for i in range(0, len(value), chunk)])

def readable_date(dt,forceyr=False,addtime=True):
	nowyr = datetime.datetime.today().year
	months = ['januari','februari','maart','april','mei','juni','juli','augustus','september','oktober','november','december']
	m = int(dt.strftime("%m"))
	monthstr = months[m-1]
	yr = dt.year
	d = int(dt.strftime("%d"))
	tm = dt.strftime("%Hu%M")

	out = f"{d} {monthstr}"

	if yr != nowyr or forceyr:
		out = f"{out} {yr}"
	
	if True == addtime:
		out = f"{out} ({tm})"

	return out

def priceFormat(value):
	value = int(value)
	if 0 == value:
		return "€&nbsp;0"
	cents = int(str(value)[-2:])
	if 0 == cents:
		lastpart = ""
	else:
		lastpart = ","+ str(value)[-2:]
	firstpart = str(value)[:-2]
	if "" == firstpart:
		firstpart = "0"
	return f"€&nbsp;{firstpart}{lastpart}"

def get_merch(exclude=['blaster','communicator']):
	out = {}
	m = conto.get_paid_products()
	client_codes = {}
	for i in m:
		if i['client_handle'] not in client_codes:
			c = client.get(i['client_handle'])
			client_codes[i['client_handle']] = c['readable_code']
		description = ""
		try:
			p = product.get(i['product_handle'])
			if p['name'] in ['ticket','room','bed','donation','token','camper'] or p['name'] in exclude:
				continue
		except:
			p = {
				'description' : f"Unknown product (deleted?), product handle: {i['product_handle']}",
				'product_handle' : '0'
			}
		description =  p['description']
		if i['variants']:
			vvals = [v['value'] for v in i['variants']]
			var =  f"({logic.get_variant_name(p['handle'],vvals[0])})"
			description = f"{description} {var}"
		out[f"{client_codes[i['client_handle']]}-{i['readable_code']}"] = {
			'code' : f"{client_codes[i['client_handle']]}-{i['readable_code']}",
			'description' : description
		}
	return [out[k] for k in sorted(out)]

def get_tpl_vars(page='shop',id=None,id_type=None):
	out = {
		'pagettl' : 'Fri3d Camp ticketshop'
	}
	out['id'] = id
	out['id_type'] = id_type
	out['css'] = url_for('static', filename='css/style.css')
	out['favicon_16'] = url_for('static', filename='img/favicon-16x16.png')
	out['favicon_32'] = url_for('static', filename='img/favicon-32x32.png')
	out['favicon_96'] = url_for('static', filename='img/favicon-96x96.png')
	out['favicon_192'] = url_for('static', filename='img/favicon-192x192.png')
	if 'terms' == page:
		out['pagettl'] = out['pagettl'] + ' | Privacyvoorwaarden'
		with open('fr1ckets/static/terms.md','r') as f:
			mdstr = f.read()
		out['content'] = markdown.markdown(mdstr)

	if page in ['conto','session','conto_print']:
		out = out | conto_view.get(id,id_type)
		return out
		
	if 'check' == page:
		out['fields'] = {
			'email' : {
				"name": "email",
				"id": "email",
				"type": "email",
				"label": "E-mailadres"
			}
		}
		return out

	if 'notyet' == page:
		out['fields'] = {
			'email': {
				"name": "email",
				"id": "email",
				"type": "email",
				"label": "E-mailadres"
			},
			'from': 'replaceme',
			'wait_seconds': 'replaceme',
		}
		return out

	if 'shop' == page:
		out['fields'] = {
			'email' : {
				"name": "email",
				"id": "email",
				"type": "email",
				"label": "E-mailadres",
				"value" : ""
			},
			'tickets' : {
				"name": "tickets_n",
				"id": "tickets_n",
				"label": "Aantal tickets",
				"min": 0,
				"max": 20,
				"visibility" : "hidden"
			},
			"ticketholder" : {
				"ticketholder_name" : {
					"name": "ticketname_tpl",
					"id": "ticketname_tpl",
					"type": "text",
					"label": "Naam",
					"help" : "We hebben een naam nodig om je ticket te herkennen.",
					"required" : True
				},
				"ticketholder_email" : {
					"name": "ticketemail_tpl",
					"id": "ticketemail_tpl",
					"type": "email",
					"label": "E-mailadres",
					"help" : "Op dit e-mailadres sturen we praktische informatie ivm je deelname."
				},
				"ticketholder_notes" : {
					"name": "ticketnotes_tpl",
					"id": "ticketnotes_tpl",
					"type": "text",
					"label": "Specifieke opmerkingen of wensen (allergieën, toegankelijkheid, voeding)"
				},
				"ticketholder_age" : {
					"name": "ticketage_tpl",
					"id" : "ticketage_tpl",
					"label": "Leeftijd bij de start van Fri3d Camp",
					"type": "number",
					"help" : "We hebben de leeftijd nodig voor de administratie van De Kluis",
					"required" : True
				},
				"ticketholder_diet" : {
					"name": "ticketdiet_tpl",
					"id" : "ticketdiet_tpl",
					"label": "Deelnemer eet vegetarisch (het vegeratisch eten is ook vegan)"
				},
				"ticketholder_tourist" : {
					"name": "ticketholder_tourist_tpl",
					"id" : "ticketholder_tourist_tpl",
					"label": "Deelnemer kan niet minstens één community-taak opnemen tijdens Fri3d Camp"
				},
				"ticketholder_othertask" : {
					"name": "ticketholder_othertask_tpl",
					"id" : "ticketholder_othertask_tpl",
					"label": "Deelnemer kan moeilijk een (fysiek belastende) taak opnemen tijdens Fri3d Camp, maar wil graag op een andere manier bijdragen"
				},
				"ticketholder_early" : {
					"name": "ticketholder_early_tpl",
					"id" : "ticketholder_early_tpl",
					"label": "Deelnemer helpt graag opbouwen de dagen voor het kamp (we contacteren je nog)"
				},
				"ticketholder_late" : {
					"name": "ticketholder_late_tpl",
					"id" : "ticketholder_late_tpl",
					"label": "Deelnemer helpt graag afbreken de dagen na het kamp (we contacteren je nog)"
				},
				"ticketholder_business" : {
					"name": "ticketholder_business_tpl",
					"id" : "ticketholder_business_tpl",
					"label": " Dit is een businessticket (op factuur)"
				}
			},
			"vouchers" : {
				"name": "vouchers",
				"id": "vouchers",
				"label": "Ik heb één of meerdere vouchers."
			},
			"remarks" : {
				"name" : "remarks",
				"id" : "remarks",
				"label": "Opmerkingen of extra informatie bij je bestelling"
			},
			"ages_confirm" : {
				"name": "ages_confirm",
				"id": "js_lastcheck_age",
				"classes" : ["js_lastcheck"],
				"label": "Ik bevestig dat personen jonger dan 16 vergezeld worden door een verantwoordelijke meerderjarige."
			},
			"excellent_confirm" : {
				"name": "excellent_confirm",
				"id": "js_lastcheck_excellent",
				"classes" : ["js_lastcheck"],
				"label": "Ik ga akkoord met de <a href='https://www.fri3d.be/deelnemen/excellent/' target='_blank'>gedragscode</a> en de <a href=\"/terms\" target=\"_blank\">verkoopsvoorwaarden</a>."
			},
			"animal_confirm" : {
				"name": "animal_confirm",
				"id": "js_lastcheck_animal",
				"classes" : ["js_lastcheck"],
				"label": "Ik weet dat ik geen (huis)dieren mag meebrengen naar Fri3d Camp, met uitzondering van assistentiedieren."
			}
		}
		out['products'] = product.list()

		"""
		# XXX FIXME remove this
		for i in range(len(out['products'])):
			if 'bed' == out['products'][i]['name']:
				out['products'][i]['availability'] = 'sold_out'
			if 'camper' == out['products'][i]['name']:
				out['products'][i]['availability'] = 'unavailable'
			if 'tshirt_kids' == out['products'][i]['name']:
				out['products'][i]['availability'] = 'unavailable'
			if 'drinkfles' == out['products'][i]['name']:
				out['products'][i]['availability'] = 'hidden'
			if 'communicator' == out['products'][i]['name']:
				out['products'][i]['availability'] = 'unavailable'
			if 'donation' == out['products'][i]['name']:
				out['products'][i]['availability'] = 'hidden'
			if 'token' == out['products'][i]['name']:
				out['products'][i]['availability'] = 'hidden'
			if 'room' == out['products'][i]['name']:
				out['products'][i]['availability'] = 'hidden'
		"""


		out['ticketpricing'] = {}

		for p in out['products']:
			p['vatprices'] = p['price']
			p['vatstring'] = makeVATString(p['price'])
			p['price'] = VATratesToSingleValue(p['price'])
			p['img'] = p['name']+'.jpg'
			if not os.path.exists(f"fr1ckets/static/img/product/{p['img']}"):
				p['img'] = p['name']+'.png'
			if 'ticket' == p['name']:
				out['tickethandle'] = p['handle']
				out['ticketpname'] = p['name']
				out['ticketpcategory'] = p['category']
				out['ticketpricing'] = {}
				out['ticketpricing']['base'] = p['price']
				out['ticketpricing']['types'] = []
				for v in p['variants']:
					if 'bits' == v['name']:
						for o in v['options']:
							out['ticketpricing']['types'].append(
								{
									'slug' : o['slug'],
									'start' : o['name'].split('-')[0],
									'price_modifier': VATratesToSingleValue(o['price_modifier'])
								}
							)
	return out
