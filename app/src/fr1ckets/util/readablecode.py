# generate unique human readable codes, avoiding ambiguous (Dutch) phonetics.
# fr1ckets, the fri3dcamp devs, 2024

import random
import math

badwords = ['lul','kut','kak','pis','pipi','sex','pipi','neger','anal','vagin','penis','cum','zat','mofo','ij','bigami']

letters = [ 'bdfghjklmnprstvwxz','aeiou' ]

def create_code(length=6):
	global letters
	out = ''
	for i in range(length):
		out += random.choice(letters[i%2])
	while has_badword(out):
		out = create_code(length)
	return out

def max_combinations(length):
	global letters
	syllables = len(letters[0]) * len(letters[1])
	syl_length = math.floor(length/2)
	max = math.comb((syllables + syl_length - 1) ,syl_length)
	if length%2 == 1:
		max = max*len(letters[0])
	return max

def create_codes(n=10,length=3):
	out = []
	counter = 0
	max = max_combinations(length)
	if max < n:
		raise Exception(f"Impossible to generate {n} codes of length {length} (maximum is {max}, ignoring badwords)")
	while len(set(out)) < n:
		out.append(create_code(length))
		counter += 1
		if counter > n*2:
			raise Exception(f"Taking too long to generate {n} codes of length {length}")
	return list(set(out))

def has_badword(code):
	global badwords
	for b in badwords:
		if b in code:
			return True
	return False

if '__main__' == __name__:
	# max_combinations(6)
	codes = create_codes(10000,5)
	print(codes)
