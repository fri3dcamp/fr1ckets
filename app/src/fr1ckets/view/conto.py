from fr1ckets.model import logic
from fr1ckets.model import conto
from fr1ckets.model import session
from fr1ckets.model import client
from fr1ckets.util import helper
from fr1ckets import app
import datetime

def get_conto_totals(sessions):
	out = {
		'total' : 0,
		'paid' : 0,
		'topay' : 0
	}
	for s in sessions:
		out['total'] += s['total']
		out['paid'] += s['paid']
	out['topay'] = out['total'] - out['paid']
	return out

def make_paylines(contototals):
	return [
		{
			'important' : True,
			'desc' : 'Totaal van alle bestellingen',
			'price' : contototals['total']
		},
		{
			'important' : True,
			'desc' : 'Nog te betalen',
			'price' : contototals['topay']
		}
	]

def add_vatstrings(p):
	p['vatstring'] = helper.makeVATString(p['price'])
	return p

def process_variants_fields(p):
	p['product_type'] = 'extras_other'
	p['label'] = p['description']
	if 'ticket' != p['product_type'] and p['variants']:
		vvals = [v['value'] for v in p['variants']]
		p['product_name'] = f"{p['product_og_name']}|{vvals[0]}"
		p['label'] = f"{p['description']} ({logic.get_variant_name(p['product_handle'],vvals[0])})"
	
	p['icon'] = p['product_og_name'].split('_')[0]

	if 'ticket' == p['product_og_name']:
		p['product_type'] = 'tickets'
		fields_to_elevate = ['name','age','notes','email']
		variants_to_elevate = ['food','late','early','notask','othertask','bits']

		for f in fields_to_elevate:
			for pf in p['fields']:
				if pf['name'] == f:
					p[f] = pf['value']

		for v in variants_to_elevate:
			for pv in p['variants']:
				if pv['name'] == v:
					p[v] = pv['value']
					if 'no' == p[v]:
						p[v] = False
					if 'yes' == p[v]:
						p[v] = True
					if 'vegetarian' == p[v] and 'food' == v:
						p[v] = 'veggie'
					elif 'food' == v:
						p[v] = None
	else:
		if p['product_og_name'] in ['bed','room','camper']:
			p['product_type'] = 'extras_stay'
	p['single_price'] = helper.VATratesToSingleValue(p['price'])
	return p

def combine_identical_products(products):
	keyed = {}
	for p in products:
		if p['product_og_name'] not in keyed:
			keyed[p['product_og_name']] = p
			keyed[p['product_og_name']]['amount'] = 0
			keyed[p['product_og_name']]['combined_price'] = 0
			keyed[p['product_og_name']]['vouchers'] = []
		if p['voucher']:
			keyed[p['product_og_name']]['vouchers'].append(p['voucher'])
		keyed[p['product_og_name']]['combined_price'] += p['single_price']
		keyed[p['product_og_name']]['amount'] += 1
	return keyed.values()

def template_prep(s):
	s['payment_instruction'] = {
		'total' : s['total'], 
		'paid' : s['paid'],
		'outstanding' : 0,
		'qr' : "/api/shop/session/qr/"+s['nonce'],
		'remittance' : s['payment_code_human'],
		'deadline' : helper.readable_date(s['expires_at'],False,False),
		'account': app.config['OUR_BANK_ACCOUNT'],
	}
	s['payment_instruction']['outstanding'] = s['payment_instruction']['total'] - s['payment_instruction']['paid'] 
	s['label'] = helper.readable_date(s['created_at'])
	s['tickets'] = []
	s['extras_stay'] = []
	s['extras_other'] = []
	for p in s['products']:
		p = process_variants_fields(p)
		p = add_vatstrings(p)
		p['taskinfo'] = logic.community_person(p)
		s[p['product_type']].append(p)
	s['extras_stay_split'] =s['extras_stay']
	s['extras_stay'] = combine_identical_products(s['extras_stay'])
	s['extras_other_split'] =s['extras_other']
	s['extras_other'] = combine_identical_products(s['extras_other'])
	del s['products']
	return s

def calc_cattotals(sessions):
	out = {
		'tickets' : 0,
		'extras_stay': 0,
		'extras_other': 0,
		'notes' : 0,
		'tokens' : 0,
		'donations' : 0,
		'all' : 0
	}
	for s in sessions:
		for e in s['extras_other_split']:
			if 'token' == e['product_og_name']:
				out['tokens'] += 1
		for e in s['extras_other_split']:
			if 'donation' == e['product_og_name']:
				out['donations'] += 1
		out['tickets'] += len(s['tickets'])
		out['all'] += len(s['tickets'])
		out['extras_stay'] += len(s['extras_stay_split'])
		out['all'] += len(s['extras_stay_split'])
		out['extras_other'] += len(s['extras_other_split'])
		out['all'] += len(s['extras_other_split'])
		out['a4s'] = out['all'] - (out['tokens'] + out['donations'])
		out['extras_pickup'] = out['extras_other'] - (out['tokens'] + out['donations'])
		if s['notes']:
			out['notes'] += 1
			out['all'] += 1

	return out

def get(nonce,nonce_type='conto'):
	if 'conto' == nonce_type:
		client_nonce = nonce
		session_filter = None
		c = client.find_by_nonce(client_nonce)
	if 'session' == nonce_type:
		sesh = session.find(nonce)
		client_handle = sesh['client_handle']
		session_filter= sesh['handle']
		c = client.get(client_handle)
	client_handle = c['handle']
	sessions = conto.get_sessions_for_client(client_handle)
	allsessions = []
	
	for s in sessions:
		raw_s = session.get(s)
		s_obj = logic.contextualize_session(raw_s)
		s_obj = template_prep(s_obj)

		allsessions.append(s_obj)

	if session_filter:
		allsessions = [s for s in allsessions if s['handle'] == session_filter]
	out = {
		'client_nonce' : c['nonce'],
		'email' : c['email'],
		'paylines' : [],
		'sessions' : [],
		'amount_due' : 0,
		'readable_code' : c['readable_code'],
		'notes' : c['notes']
	}
	out['sessions'] = allsessions
	out['cattotals'] = calc_cattotals(allsessions)
	conto_totals = get_conto_totals(allsessions)
	out['paylines'] = make_paylines(conto_totals)
	out['amount_due'] = conto_totals['topay']

	return out
