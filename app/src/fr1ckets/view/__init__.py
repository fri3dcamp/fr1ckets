# description of file
# fr1ckets, the fri3dcamp devs, 2022
from flask import render_template
from fr1ckets import app
from fr1ckets.util import helper

def handle_404(e):
	return render_template('404.jinja',vars=helper.get_tpl_vars('404'))

app.register_error_handler(404, handle_404)
