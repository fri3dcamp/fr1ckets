# endpoints servicing the ticketshop
# fr1ckets, the fri3dcamp devs, 2022
from flask import g, request, render_template, send_from_directory, redirect, url_for, abort
from fr1ckets import app
from fr1ckets.model import logic, session
from datetime import datetime, timedelta, UTC
from fr1ckets.util import helper
import os

@app.template_filter()
def priceFormat(value):
	return helper.priceFormat(value)

@app.route('/')
def root():
	return redirect(url_for('shop'))

@app.route('/favicon.ico')
def favicon():
	return send_from_directory(os.path.join(app.root_path, 'static/img'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/terms',methods = ['GET'])
def terms():
	tplvars = helper.get_tpl_vars('terms')
	return render_template('terms.jinja', vars=tplvars)

@app.route('/shop',methods = ['POST', 'GET'])
def shop():
	if False == app.config['SHOP_CURRENTLY_OPEN']:
		tplvars = helper.get_tpl_vars()
		return render_template('closed.jinja', vars=tplvars)

	if request.method == 'POST' and '1' == request.form['check']:
		can_order_from, can_order, _, _ = logic.email_can_order(request.form['email'])
		if not can_order:
			# not yet, bucko
			wait = can_order_from - datetime.now(UTC)
			tplvars = helper.get_tpl_vars('notyet')
			tplvars['fields']['from'] = can_order_from	# in UTC! js will have to localize
			tplvars['fields']['wait_seconds'] = wait.total_seconds()
			app.logger.warning("somebody was prohibited from loading the shop")
			return render_template('notyet.jinja', vars=tplvars)
		tplvars = helper.get_tpl_vars('shop')
		tplvars['fields']['email']['value'] = request.form['email']
		app.logger.warning("somebody was allowed to load the shop")
		return render_template('shop.jinja', vars=tplvars)
	else:
		tplvars = helper.get_tpl_vars('check')
		return render_template('check.jinja', vars=tplvars)

@app.route('/conto/<string:conto_nonce>')
def conto(conto_nonce):
	tplvars = helper.get_tpl_vars('conto',conto_nonce,'conto')
	return render_template('conto.jinja', vars=tplvars)


@app.route('/session/<string:session_nonce>', methods=['GET'])
def sesh(session_nonce):
	app.logger.warning("somebody viewed their session")
	try:
		tplvars = helper.get_tpl_vars('session',session_nonce,'session')
		return render_template('conto.jinja', vars=tplvars)
	except session.SessionNotFoundError:
		abort(404)

@app.route('/conto/print/<string:conto_nonce>')
def printconto(conto_nonce):
	# tplvars = helper.get_tpl_vars('conto',conto_nonce,'conto')
	tplvars = helper.get_tpl_vars('conto_print',conto_nonce,'conto')
	tplvars['userfacing'] = True
	return render_template('print_conto.jinja', vars=tplvars)
