# endpoints servicing the ticketshop
# fr1ckets, the fri3dcamp devs, 2022
from flask import g, request, render_template, redirect, url_for, flash
from flask_wtf import FlaskForm
from flask_login import login_user, logout_user, login_required, current_user
from wtforms import StringField, IntegerField, TextAreaField, DateTimeLocalField, DecimalField, PasswordField, SelectMultipleField, BooleanField
from wtforms.validators import DataRequired
from fr1ckets import app, login_manager
from fr1ckets.model import logic, util, voucher, product, client, payment, conto, session, community_shift
from fr1ckets.model.orga import user as orga_user
from datetime import datetime, timedelta, UTC
from fr1ckets.util import helper
from collections import OrderedDict
from functools import wraps
from pprint import pprint
import json

@app.template_filter()
def readable_voucher(value):
	return helper.readable_longstring(value)

@app.template_filter()
def readable_paymentcode(value):
	return helper.humanize_payment_code(value)

@app.template_filter()
def priceFormat(value):
	return helper.priceFormat(value)

@app.template_filter()
def datetime_format(what):
	return what.strftime('%Y-%m-%d %H:%M')


def role_required(required_role):
	"""
	marks a view (already marked with @login_required) to be accessible only
	to users with the specified role
	"""
	def role_required_decorator(fn):
		@wraps(fn)
		def wrapped(*args, **kwargs):
			if current_user and required_role in current_user.roles:
				return fn(*args, **kwargs)
			else:
				flash('You do not have the needed role to view this.')
				return redirect(url_for('orga_home'))
		return wrapped
	return role_required_decorator


@login_manager.user_loader
def orga_user_load(_id):
	"""
	a flask-login dependency, returns a user object for
	the given id, or None if there is no such user
	"""
	try:
		return orga_user.get(_id)
	except orga_user.OrgaUserNotFoundError:
		return None

# the actually available roles
orga_user_roles = [
	'OVERVIEW',		# view general (non-personal-info) data
	'PERSONAL',		# view anything with personal info
	'FINANCIAL',	# perform financial operations (payments)
	'ORGA_ADMIN',	# user/role CRUD
]

class VoucherForm(FlaskForm):
	applicable_to = StringField('applicable_to', validators=[DataRequired()])
	how_many = IntegerField('how_many', validators=[DataRequired()])
	notes = StringField('notes')


@app.route('/orga/vouchers', methods=['GET', 'POST'])
@login_required
@role_required('FINANCIAL')
@util.db_save
def vouchers():
	form = VoucherForm()

	if form.validate_on_submit():
		for i in range(form.how_many.data):
			_voucher = voucher.schema.load({
				'applicable_to': form.applicable_to.data,
				'notes': form.notes.data,
			})
			logic.create_voucher(_voucher)
		return redirect(url_for('vouchers'))

	available_categories = product.available_categories()

	"""
	# no vouchers for uncategorized
	if 'uncategorized' in available_categories:
		available_categories.remove('uncategorized')
	"""

	_vouchers = voucher.list()
	tplvars = helper.get_tpl_vars('orga_vouchers')
	tplvars['vouchers'] = _vouchers
	tplvars['available_categories'] = available_categories

	return render_template('orga/vouchers.jinja',vars=tplvars,form=form,tab='vouchers')


class ClientsForm(FlaskForm):
	emails = TextAreaField('emails', validators=[DataRequired()])
	can_order_from = DateTimeLocalField('can_order_from', format="%Y-%m-%dT%H:%M")
	notes = StringField('notes')


@app.route('/orga/clients', methods=['GET', 'POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def clients():
	form = ClientsForm()

	if form.validate_on_submit():
		try:
			emails = [
				logic.normalize_email(e.strip())
				for e in form.emails.data.split()
			]
		except ValueError:
			print(f"bad email={emails}")
		for email in emails:
			try:
				_client = client.find(email)
				_client['can_order_from'] = form.can_order_from.data
				_client['notes'] = form.notes.data
				client.update(_client)
			except client.ClientNotFoundError:
				_client = client.schema.load({
					'email': email,
					'can_order_from': form.can_order_from.data.isoformat(),
					'notes': form.notes.data,
				})
				client.create(_client)
		return redirect(url_for('clients'))

	_clients = client.list()
	tplvars = helper.get_tpl_vars('orga_clients')

	return render_template('orga/clients.jinja',vars=tplvars,form=form,
		clients=_clients,tab='clients')

class ProductForm(FlaskForm):
	json = TextAreaField('json', validators=[DataRequired()])


def tojson_pretty(data):
	return json.dumps(data, indent=2, default=str)
app.jinja_env.filters['tojson_pretty'] = tojson_pretty


@app.route('/orga/products', methods=['GET', 'POST'])
@login_required
@util.db_save
def products():
	form = ProductForm()

	_products = product.list()

	tplvars = helper.get_tpl_vars('orga_products')
	tplvars['products'] = _products

	return render_template('orga/products.jinja', vars=tplvars,tab='products')


class MassPaymentForm(FlaskForm):
	text = TextAreaField('text', validators=[DataRequired()])


@app.route('/orga/mass_payments', methods=['GET', 'POST'])
@login_required
@role_required('FINANCIAL')
@util.db_save
def mass_payments():
	form = MassPaymentForm()
	results = {
		'manual': [],
		'ok': [],
		'incomplete': [],
		'not_found': [],
		'already_exists': [],
	}

	if form.validate_on_submit():
		try:
			for line in form.text.data.split('\n'):
				line = line.strip()
				fields = line.split('\t')
				if fields[0] != app.config['OUR_BANK_ACCOUNT']:
					continue
				entry = payment.schema.load({
					# don't interpret arrived_at as a date, it can be
					# anything that the bank uses, so long as we can find
					# dupes with it we're good
					'reference': fields[3],
					'payment_code':
						helper.dehumanize_payment_code(fields[10]),
					'amount':
						int(fields[5].replace(',', '').replace('.', '')),
					'notes': 'automated',
				})

				try:
					if logic.process_payment_automated(entry):
						results['ok'].append(line)
					else:
						results['incomplete'].append(line)
				except logic.PaymentExistsError:
					results['already_exists'].append(line)
				except logic.PaymentNotFoundError:
					results['not_found'].append(line)
				except logic.PaymentManualError:
					results['manual'].append(line)
		except Exception as e:
			raise e
			pass

	tplvars = helper.get_tpl_vars('orga_mass_payments')

	return render_template('orga/mass_payments.jinja',
		vars=tplvars,
		form=form,
		results=results,
		tab='payments')


@app.route('/orga/totals', methods=['GET', 'POST'])
@login_required
@role_required('OVERVIEW')
@util.db_save
def totals():

	from pprint import pprint

	totals = {k: {
		'all': {
			'total': 0,
			'paid': 0,
			'voucher_used': 0,
		},
		'variant': {
		},
	} for k in map(lambda p: p['name'], product.list())}

	for _client in client.list():
		session_handles = conto.get_sessions_for_client(_client['handle'])
		for session_handle in session_handles:
			_session = session.get(session_handle)
			_session = logic.contextualize_session(_session)

			if _session['removed']:
				# FIXME jef; check if orga wants this, or in another col
				continue

			for item in _session['products']:

				if item['removed']:
					# FIXME jef; check if orga wants this, or in another col
					continue

				product_name = item['product_og_name']
				total = totals[product_name]

				total['all']['total'] += 1
				if item['voucher']:
					total['all']['voucher_used'] += 1

				if _session['due'] <= 0:
					total['all']['paid'] += 1

				for variant in item['variants']:
					variant_name = variant['name']
					variant_choice = variant['value']

					if variant_name not in total['variant']:
						total['variant'][variant_name] = {}

					if variant_choice not in total['variant'][variant_name]:
						total['variant'][variant_name][variant_choice] = {
							'total': 0,
							'paid': 0,
							'voucher_used': 0,
						}

					total['variant'][variant_name][variant_choice]['total'] += 1
					if item['voucher']:
						total['variant'][variant_name][variant_choice]['voucher_used'] += 1
					if _session['due'] <= 0:
						total['variant'][variant_name][variant_choice]['paid'] += 1

	return render_template('orga/totals.jinja',
		totals=totals,
		vars=helper.get_tpl_vars('orga_mass_payments.jinja'),tab='totals')


class ToggleSessionForm(FlaskForm):
	session_handle = StringField('session_handle', validators=[DataRequired()])


@app.route('/orga/session/toggle_session', methods=['POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def session_toggle_session():
	form = ToggleSessionForm()

	if form.validate_on_submit():
		_session = session.get(form.session_handle.data)
		_session['removed'] = not _session['removed']
		session.update(_session)
	else:
		print(form.errors)

	return redirect(url_for('sessions'))


class RegisterPaymentForm(FlaskForm):
	amount = DecimalField('amount', validators=[DataRequired()])
	session_handle = StringField('session_handle', validators=[DataRequired()])


class SendReminderForm(FlaskForm):
	client_handle = StringField('client_handle', validators=[DataRequired()])

@app.route('/orga/sessions/print', methods=['GET'])
@login_required
@role_required('PERSONAL')
def sessions_print():
	orders = {}
	for c in client.list():
		if c['email'] != 'fallback@something.invalid':
			tplvars = helper.get_tpl_vars('conto_print',c['nonce'],'conto')
			tplvars['userfacing'] = False
			orders[c['readable_code']] = render_template('orga/print_conto.jinja', vars=tplvars)
	sorted_orders = [orders[k] for k in sorted(orders)]
	return render_template('orga/print_contos.jinja',vars=helper.get_tpl_vars('ok'),
		orders=sorted_orders)

@app.route('/orga/mailstocodes/print', methods=['GET'])
@login_required
@role_required('PERSONAL')
def mailstocodes_print():
	return render_template('orga/print_mailstocodes.jinja',vars=helper.get_tpl_vars('ok'),
		lines=logic.emailstocodes())

@app.route('/orga/mailstocodes', methods=['GET'])
@login_required
@role_required('PERSONAL')
def mailstocodes_list():
	return render_template('orga/list_mailstocodes.jinja',vars=helper.get_tpl_vars('ok'),
		lines=logic.emailstocodes())

@app.route('/orga/sessions', methods=['GET', 'POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def sessions():
	form_payment = RegisterPaymentForm()
	form_send_reminder = SendReminderForm()
	form_toggle = ToggleSessionForm()

	totals = {
		'all sessions': 0,
		'removed sessions': 0,
		'clients': 0,
		'total': 0,
		'paid': 0,
		'due': 0,
	}

	clients = [logic.contextualize_client(c) for c in client.list()]
	for _client in clients:
		for _session in _client['sessions']:

			totals['all sessions'] += 1
			if _session['removed']:
				totals['removed sessions'] += 1
			else:
				totals['total'] += _session['total']
				totals['paid'] += _session['paid']
				totals['due'] += _session['due']

	totals['clients'] = len(clients)
	for key in ['total', 'paid', 'due']:
		totals[key] = totals[key]/100

	return render_template('orga/sessions.jinja',
		vars=helper.get_tpl_vars('ok'),
		form_payment=form_payment,
		form_send_reminder=form_send_reminder,
		form_toggle=form_toggle,
		clients=clients,
		totals=totals,
		tab='sessions')


class AddNoteForm(FlaskForm):
	contents = StringField('contents', validators=[DataRequired()])
	session = StringField('session', validators=[DataRequired()])


@app.route('/orga/details/add_note', methods=['POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def details_add_note():
	form = AddNoteForm()
	if form.validate_on_submit():
		new_note = {
			'contents' : form.contents.data,
			'location' : 'details',
			'author' : current_user.email,
		}
		_session = session.find(form.session.data)
		_session['orga_notes'].append(new_note)
		session.update(_session)

	return redirect(url_for('details'))


class ToggleContoEntryForm(FlaskForm):
	conto_handle = StringField('conto_handle', validators=[DataRequired()])


@app.route('/orga/details/toggle_conto_entry', methods=['POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def details_toggle_conto_entry():
	form = ToggleContoEntryForm()

	if form.validate_on_submit():
		_conto_entry = conto.get(form.conto_handle.data)
		_conto_entry['removed'] = not _conto_entry['removed']
		conto.update(_conto_entry)
	else:
		print(form.errors)

	return redirect(url_for('details'))


@app.route('/orga/details', methods=['GET', 'POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def details():
	note_form = AddNoteForm()
	toggle_form = ToggleContoEntryForm()

	session_handles_per_client_handle = OrderedDict()
	for entry in conto.get_all_clients_sessions():
		client_handle = entry['client_handle']
		session_handle = entry['session_handle']
		if client_handle not in session_handles_per_client_handle:
			session_handles_per_client_handle[client_handle] = []
		session_handles_per_client_handle[client_handle].append(session_handle)

	clients = []

	for client_handle, session_handles in session_handles_per_client_handle.items():
		_client = client.get(client_handle)
		_client['sessions'] = []
		for session_handle in session_handles:
			_session = session.get(session_handle)
			_session = logic.contextualize_session(_session)
			_client['sessions'].append(_session)

		clients.append(_client)

	return render_template('orga/details.jinja',
		vars=helper.get_tpl_vars('ok'),
		note_form=note_form,
		toggle_form=toggle_form,
		clients=clients,
		tab='details')


@app.route('/orga/sessions/register_payment', methods=['POST'])
@login_required
@role_required('FINANCIAL')
@util.db_save
def register_payment():
	form = RegisterPaymentForm()

	if form.validate_on_submit():
		_session = session.get(form.session_handle.data)
		_payment = payment.schema.load({
			'amount': int(form.amount.data * 100),
			'notes': 'sessions page',
		})
		_client = client.get(_session['client_handle'])

		logic.process_payment(_client, _session, _payment)

	return redirect(url_for('sessions'))


@app.route('/orga/sessions/send_reminder', methods=['POST'])
@login_required
@role_required('FINANCIAL')
@util.db_save
def send_reminder():
	form = SendReminderForm()

	if form.validate_on_submit():
		_client = client.get(form.client_handle.data)
		logic.remind_client(_client)
	return redirect(url_for('sessions'))


class WeirdScienceProductForm(FlaskForm):
	json = TextAreaField('json', validators=[DataRequired()])


@app.route('/orga/weirdscience/products', methods=['GET', 'POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def weirdscience_products():
	form = WeirdScienceProductForm()

	if form.validate_on_submit():
		parsed_json = json.loads(form.json.data)

		# we don't want to see these keys
		for key in ['handle']:
			if key in parsed_json:
				del parsed_json[key]

		# this will set a new handle, if we need it for creating
		parsing_product = product.schema.load(parsed_json)
		try:
			_product = product.find(parsing_product['name'])
			# updating, use existing product's handle
			del parsing_product['handle']
			_product.update(parsing_product)
			product.update(_product)
		except product.ProductNotFoundError:
			product.create(parsing_product)

		return redirect(url_for('weirdscience_products'))

	_products = product.list()

	return render_template('orga/weirdscience/products.jinja',
		form=form,
		products=_products)


class WeirdScienceSessionForm(FlaskForm):
	json = TextAreaField('json', validators=[DataRequired()])


@app.route('/orga/weirdscience/clients', methods=['GET', 'POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def weirdscience_clients():
	form = WeirdScienceSessionForm()

	if form.validate_on_submit():
		parsed_json = json.loads(form.json.data)

		# we don't want to see these keys
		for key in ['handle']:
			if key in parsed_json:
				del parsed_json[key]

		# this will set a new handle, if we need it for creating
		parsing_client = client.schema.load(parsed_json)
		try:
			_client = client.find(parsing_client['email'])
			# updating, use existing client's handle
			del parsing_client['handle']
			_client.update(parsing_client)
			client.update(_client)
		except client.ClientNotFoundError:
			client.create(parsing_client)

		return redirect(url_for('weirdscience_clients'))

	_clients = client.list()

	return render_template('orga/weirdscience/clients.jinja',
		form=form,
		clients=_clients)


class WeirdScienceSessionForm(FlaskForm):
	json = TextAreaField('json', validators=[DataRequired()])


@app.route('/orga/weirdscience/sessions', methods=['GET', 'POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def weirdscience_sessions():
	form = WeirdScienceSessionForm()

	if form.validate_on_submit():
		parsed_json = json.loads(form.json.data)

		# we don't want to see these keys
		for key in ['handle']:
			if key in parsed_json:
				del parsed_json[key]

		# this will set a new handle, if we need it for creating
		parsing_session = session.schema.load(parsed_json)
		try:
			_session = session.find(parsing_session['nonce'])
			# updating, use existing session's handle
			del parsing_session['handle']
			_session.update(parsing_session)
			session.update(_session)
		except session.SessionNotFoundError:
			session.create(parsing_session)

		return redirect(url_for('weirdscience_sessions'))

	_sessions = session.list()

	return render_template('orga/weirdscience/sessions.jinja',
		form=form,
		sessions=_sessions)


class WeirdScienceVoucherForm(FlaskForm):
	json = TextAreaField('json', validators=[DataRequired()])


@app.route('/orga/weirdscience/vouchers', methods=['GET', 'POST'])
@login_required
@role_required('PERSONAL')
@util.db_save
def weirdscience_vouchers():
	form = WeirdScienceVoucherForm()

	if form.validate_on_submit():
		parsed_json = json.loads(form.json.data)

		# we don't want to see these keys
		for key in ['handle']:
			if key in parsed_json:
				del parsed_json[key]

		# this will set a new handle, if we need it for creating
		parsing_voucher = voucher.schema.load(parsed_json)
		try:
			_voucher = voucher.find(parsing_voucher['code'])
			# updating, use existing voucher's handle
			del parsing_voucher['handle']
			_voucher.update(parsing_voucher)
			voucher.update(_voucher)
		except voucher.VoucherNotFoundError:
			voucher.create(parsing_voucher)

		return redirect(url_for('weirdscience_vouchers'))

	_vouchers = voucher.list()

	return render_template('orga/weirdscience/vouchers.jinja',
		form=form,
		vouchers=_vouchers)

@app.route('/orga/conto/print/<string:conto_nonce>')
@login_required
@role_required('PERSONAL')
def printconto_orga(conto_nonce):
	tplvars = helper.get_tpl_vars('conto_print',conto_nonce,'conto')
	tplvars['userfacing'] = False
	return render_template('orga/print_conto.jinja', vars=tplvars)

@app.route('/orga/merch/print')
def printallcodes():
	merch = helper.get_merch()
	# print(merch)
	tplvars = helper.get_tpl_vars()
	tplvars['merch'] = merch
	# tplvars['userfacing'] = False
	return render_template('orga/print_merchcodes.jinja', vars=tplvars)


class OrgaLoginForm(FlaskForm):
	email = StringField('email', validators=[DataRequired()])
	password = PasswordField('password', validators=[DataRequired()])


@app.route('/orga/login', methods=['GET', 'POST'])
@util.db_save
def orga_login():
	form = OrgaLoginForm()


	# first check if we still need to create the configured admin
	try:
		admin = orga_user.find(app.config['ORGA_ADMIN_EMAIL'])
	except orga_user.OrgaUserNotFoundError:
		admin = orga_user.OrgaUser(
			None, app.config['ORGA_ADMIN_EMAIL'], None,
			orga_user_roles)
		admin.reset_auth(password=app.config['ORGA_ADMIN_PASSWORD'])
		app.logger.warning(f"created admin user with email {app.config['ORGA_ADMIN_EMAIL']} and password {app.config['ORGA_ADMIN_PASSWORD']}")
		orga_user.create(admin)

	if form.validate_on_submit():
		email = form.email.data
		password = form.password.data

		try:
			user = orga_user.find(email)
			if user.check_auth(password):
				login_user(user, remember=True)
				return redirect(url_for('orga_home'))
		except orga_user.OrgaUserNotFoundError:
			flash("no such user")

	return render_template('orga/login.jinja', form=form, vars=helper.get_tpl_vars())

login_manager.login_view = "orga_login"
login_manager.session_protection = "strong"

@app.route('/orga', methods=['GET'])
@app.route('/orga/home', methods=['GET'])
@login_required
def orga_home():
	return render_template('orga/home.jinja', vars=helper.get_tpl_vars(),tab='orga')

@app.route('/orga/logout', methods=['GET'])
@login_required
def orga_logout():
	logout_user()
	return redirect(url_for('orga_login'))

class OrgaUsersAddForm(FlaskForm):
	email = StringField('email', validators=[DataRequired()])
	roles = SelectMultipleField('roles', choices=orga_user_roles)

@app.route('/orga/users/add', methods=['POST'])
@login_required
@role_required('ORGA_ADMIN')
@util.db_save
def orga_users_add():
	form = OrgaUsersAddForm()
	if form.validate_on_submit():
		new_user = orga_user.OrgaUser(
			None, form.email.data, None, form.roles.data)
		new_password = new_user.reset_auth()
		try:
			orga_user.create(new_user)
			flash(f"The new user {form.email.data}'s password is {new_password}")
		except orga_user.OrgaUserExistsError:
			flash(f"A user {form.email.data} already exists")

	return redirect(url_for('orga_users'))

class OrgaUsersChangeForm(FlaskForm):
	email = StringField('email', validators=[DataRequired()])
	roles = SelectMultipleField('roles', choices=orga_user_roles)

@app.route('/orga/users/change', methods=['POST'])
@login_required
@role_required('ORGA_ADMIN')
@util.db_save
def orga_users_change():
	form = OrgaUsersChangeForm()
	if form.validate_on_submit():
		if form.email.data == app.config['ORGA_ADMIN_EMAIL']:
			flash(f"Can't update this user's details, would break system.")
		else:
			existing_user = orga_user.find(form.email.data)
			existing_user.roles = form.roles.data
			orga_user.update(existing_user)
			flash(f"The user {form.email.data}'s details have been changed.")

	return redirect(url_for('orga_users'))

class OrgaUsersRemoveForm(FlaskForm):
	email = StringField('email', validators=[DataRequired()])

@app.route('/orga/users/remove', methods=['POST'])
@login_required
@role_required('ORGA_ADMIN')
@util.db_save
def orga_users_remove():
	form = OrgaUsersRemoveForm()
	if form.validate_on_submit():
		if form.email.data == app.config['ORGA_ADMIN_EMAIL']:
			flash(f"Can't remove this user, would break system.")
		else:
			existing_user = orga_user.find(form.email.data)
			orga_user.delete(existing_user.handle)
			flash(f"The user {form.email.data} has been removed.")

	return redirect(url_for('orga_users'))

class OrgaUsersPasswordForm(FlaskForm):
	email = StringField('email', validators=[DataRequired()])

@app.route('/orga/users/password', methods=['POST'])
@login_required
@role_required('ORGA_ADMIN')
@util.db_save
def orga_users_password():
	form = OrgaUsersPasswordForm()
	if form.validate_on_submit():
		try:
			existing_user = orga_user.find(form.email.data)
			new_password = existing_user.reset_auth()
			orga_user.update(existing_user)
			flash(f"The user {form.email.data}'s password is now {new_password}")
		except orga_user.OrgaUserNotFoundError:
			flash(f"The user {form.email.data} cannot be found.")

	return redirect(url_for('orga_users'))

@app.route('/orga/users', methods=['GET', 'POST'])
@login_required
@role_required('ORGA_ADMIN')
def orga_users():
	users = [orga_user.schema.dump(u) for u in orga_user.list()]

	form_add_user = OrgaUsersAddForm()
	form_remove_user = OrgaUsersRemoveForm()
	form_change_user = OrgaUsersChangeForm()
	form_password_user = OrgaUsersPasswordForm()

	return render_template('orga/users.jinja',
		users=users,
		roles=orga_user_roles,
		form_add_user=form_add_user,
		form_remove_user=form_remove_user,
		form_change_user=form_change_user,
		form_password_user=form_password_user,
		vars=helper.get_tpl_vars(),tab='users')

@app.route('/orga/community', methods=['GET'])
@login_required
@role_required('OVERVIEW')
def orga_community():

	_clients = [
		logic.contextualize_client(_client)
		for _client in client.list()
	]

	shifters = []
	for _client in _clients:
		_shifters = logic.community_people(_client['sessions'], only_paid=False)
		#_shifters = [_shifter for _shifter in _shifters if _shifter['allowed']]
		shifters.extend(_shifters)

	shifters_by_handle = {shifter['conto_handle'] : shifter for shifter in shifters}
	community_shifts = community_shift.list()

	choices_per_shift = community_shift.list_choices()

	timelined_shifts = {}
	for shift in community_shifts:
		shift['signed_up'] = choices_per_shift[shift['handle']]

		shift_date = shift['start'].date()
		if shift_date not in timelined_shifts:
			timelined_shifts[shift_date] = []
		timelined_shifts[shift_date].append(shift)

	for date in timelined_shifts.keys():
		timelined_shifts[date] = sorted(timelined_shifts[date], key=lambda shift: shift['start'])


	return render_template('orga/community.jinja',
		shifts_by_day=timelined_shifts,
		shifters_by_handle=shifters_by_handle,
		vars=helper.get_tpl_vars(), tab='community')

@app.route('/orga/stats/community', methods=['GET'])
@login_required
@role_required('OVERVIEW')
def stats_community():

	_clients = [
		logic.contextualize_client(_client)
		for _client in client.list()
	]

	shifters = []
	for _client in _clients:
		_shifters = logic.community_people(_client['sessions'], only_paid=False)
		#_shifters = [_shifter for _shifter in _shifters if _shifter['allowed']]
		shifters.extend(_shifters)

	stats = {
		'shifts': {
			'important': {
				'total': 0,
				'total_people_needed': 0,
				'total_people_committed': 0,
				'full': 0,
				'empty': 0,
				'occupied': 0,
			},
			'unimportant': {
				'total': 0,
				'total_people_needed': 0,
				'total_people_committed': 0,
				'full': 0,
				'full': 0,
				'empty': 0,
				'occupied': 0,
			},
		},
		'shifters': {
			'expected': {
				'total': 0,
				'committed': 0,
				'uncommitted': 0,
			},
			'allowed': {
				'total': 0,
				'committed': 0,
				'uncommitted': 0,
			},
			'not_allowed': {
				'total': 0,
				'committed': 0,
				'uncommitted': 0,
			},
		},
	}
	community_shifts = community_shift.list()

	choices_per_shift = community_shift.list_choices()
	choices_per_shifter = {}
	for shift_handle, conto_handles in choices_per_shift.items():
		for conto_handle in conto_handles:
			if conto_handle not in choices_per_shifter:
				choices_per_shifter[conto_handle] = []
			choices_per_shifter[conto_handle].append(shift_handle)

	for shifter in shifters:
		if shifter['conto_handle'] in choices_per_shifter:
			shifter['shifts'] = choices_per_shifter[shifter['conto_handle']]
		else:
			shifter['shifts'] = []

		if shifter['expected']:
			_bin = 'expected'
		else:
			if shifter['allowed']:
				_bin = 'allowed'
			else:
				_bin = 'not_allowed'

		stats['shifters'][_bin]['total'] += 1
		_bin_bin = 'committed' if len(shifter['shifts']) else 'uncommitted'
		stats['shifters'][_bin][_bin_bin] += 1

	for shift in community_shifts:
		_bin = 'important' if shift['important'] else 'unimportant'

		stats['shifts'][_bin]['total'] += 1
		stats['shifts'][_bin]['total_people_needed'] += shift['n_total']
		stats['shifts'][_bin]['total_people_committed'] += len(choices_per_shift[shift['handle']])
		if len(choices_per_shift[shift['handle']]) == 0:
			_bin_bin = 'empty'
		elif len(choices_per_shift[shift['handle']]) >= shift['n_total']:
			_bin_bin = 'full'
		else:
			_bin_bin = 'occupied'
		stats['shifts'][_bin][_bin_bin] += 1

	pprint(stats)

	return render_template('orga/stats/community.jinja',
		stats=stats,
		vars=helper.get_tpl_vars(),tab='community')

@app.route('/orga/lists/community', methods=['GET'])
@login_required
@role_required('OVERVIEW')
@util.db_save
def lists_community():

	clients = []

	for _client in client.list():
		sessions = logic.contextualize_client(_client)['sessions']
		community_people = logic.community_people(sessions)

		contains_helpers = True in [p['expected'] for p in community_people]

		if len(community_people) and contains_helpers:
			_client['community_url'] = 'https://tickets.fri3d.be' + url_for('shifts',
				_anchor=_client['nonce'])
			clients.append(_client)

	return render_template('orga/lists/community.jinja',
		vars=helper.get_tpl_vars(),
		clients=clients,
		tab='foo')

@app.route('/orga/lists/community_todo', methods=['GET'])
@login_required
@role_required('OVERVIEW')
@util.db_save
def lists_community_todo():

	clients = []
	choices = community_shift.list_choices()
	choices_by_conto_handle = {}
	for community_shift_handle, conto_handles in choices.items():
		for conto_handle in conto_handles:
			if conto_handle not in choices_by_conto_handle:
				choices_by_conto_handle[conto_handle] = []
			choices_by_conto_handle[conto_handle].append(
				community_shift_handle)

	for _client in client.list():
		sessions = logic.contextualize_client(_client)['sessions']
		community_people = logic.community_people(sessions)

		contains_helpers = True in [p['expected'] for p in community_people]

		todos = [p for p in community_people if p['expected'] and p['conto_handle'] not in choices_by_conto_handle]

		if len(community_people) and contains_helpers and len(todos):
			_client['community_url'] = 'https://tickets.fri3d.be' + url_for('shifts',
				_anchor=_client['nonce'])
			_client['todos'] = todos
			_client['todos_names'] = ', '.join([p['name'] for p in todos])
			clients.append(_client)

	return render_template('orga/lists/community_todo.jinja',
		vars=helper.get_tpl_vars(),
		clients=clients,
		tab='foo')


@app.route('/orga/lists/people', methods=['GET', 'POST'])
@login_required
@role_required('OVERVIEW')
@util.db_save
def lists_people():


	clients = []

	for _client in client.list():
		clients.append(logic.contextualize_client(_client))

	return render_template('orga/lists/people.jinja',
		totals=totals,
		clients=clients,

		vars=helper.get_tpl_vars('orga_mass_payments.jinja'),tab='totals')


class ListClientsForm(FlaskForm):
	exclude_removed_sessions = BooleanField('exclude_removed_sessions')
	exclude_unpaid_sessions = BooleanField('exclude_unpaid_sessions')


@app.route('/orga/lists/clients', methods=['GET', 'POST'])
@login_required
@role_required('OVERVIEW')
@util.db_save
def lists_clients():

	clients = []

	form = ListClientsForm()

	if form.validate_on_submit():
		# build a list of all clients, contextualized
		for _client in client.list():
			clients.append(logic.contextualize_client(_client))

		# strip out all removed sessions, if desired
		if form.exclude_removed_sessions.data:
			for _client in clients:
				_client['sessions'] = [s for s in _client['sessions'] if s['removed'] == False]

		# strip out all not-fully-paid sessions, if desired
		if form.exclude_unpaid_sessions.data:
			for _client in clients:
				_client['sessions'] = [s for s in _client['sessions'] if s['due'] <= 0]

		# now only pick clients which still have sessions
		clients = [c for c in clients if len(c['sessions']) > 0]

	return render_template('orga/lists/clients.jinja',
		form=form,
		clients=clients,
		vars=helper.get_tpl_vars('orga_mass_payments.jinja'),tab='totals')

@app.route('/orga/lists/food', methods=['GET', 'POST'])
@login_required
@role_required('OVERVIEW')
@util.db_save
def lists_food():

	totals = {
		'per_bits': {},
		'totals': {},
	}

	for _client in client.list():
		_client = logic.contextualize_client(_client)

		for _session in _client['sessions']:
			if _session['removed']:
				continue

			for _product in _session['products']:
				if _product['product_og_name'] != 'ticket':
					continue

				if _product['removed']:
					continue


				bits, food = None, None
				for variant in _product['variants']:
					if variant['name'] == 'bits':
						bits = variant['value']
					elif variant['name'] == 'food':
						food = variant['value']

				if bits not in totals['per_bits']:
					totals['per_bits'][bits] = {}
				if food not in totals['per_bits'][bits]:
					totals['per_bits'][bits][food] = 0
				if food not in totals['totals']:
					totals['totals'][food] = 0

				totals['totals'][food] += 1
				totals['per_bits'][bits][food] += 1

	return render_template('orga/lists/food.jinja',
		totals=totals,
		vars=helper.get_tpl_vars('orga_mass_payments.jinja'),tab='totals')

