# endpoints servicing the ticketshop, the community part
# fr1ckets, the fri3dcamp devs, 2022
from flask import g, request, render_template, redirect, url_for, flash
from flask_wtf import FlaskForm
from flask_login import login_user, logout_user, login_required, current_user
from wtforms import StringField, IntegerField, TextAreaField, DateTimeLocalField, DecimalField, PasswordField, SelectMultipleField
from wtforms.validators import DataRequired
from fr1ckets import app, login_manager
from fr1ckets.model import logic, util, voucher, product, client, payment, conto, session
from fr1ckets.model.orga import user as orga_user
from datetime import datetime, timedelta, UTC
from fr1ckets.util import helper
from collections import OrderedDict
from functools import wraps
import json

@app.route('/community/shift')
def shifts():
	return render_template('community/shift.jinja', vars=helper.get_tpl_vars())
