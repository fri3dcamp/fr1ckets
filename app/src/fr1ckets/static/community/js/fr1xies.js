const minute_to_px_ratio = 1.6;          // grid view; each minute is so many rems height
const ruler_tick_granularity = '00:30';   // grid view; ruler tick granularity
const camp_tz = 'Europe/Brussels';
let show_invisible = false;
let camp = {};
let load_url = undefined;

document.addEventListener('DOMContentLoaded', () => {
    let client_handle = document.location.hash.substr(1);

    if (!client_handle.length) {
        document.querySelector('#readable_code_dialog').hidden = false;
        document.querySelector('#readable_code_submit').addEventListener('click', (event) => {
            event.preventDefault();
            let readable_code = document.querySelector('#readable_code_entry').value;
            load_url = `/api/community/shifts/readable_code/${readable_code}`;
            reload_camp();
        });
    } else {
        load_url =`/api/community/shifts/client/${client_handle}`;
        reload_camp();
    }
});
window.addEventListener('resize', () => {
    fr1xies_blit();
});

function reload_camp()
{

    if (!load_url) {
        return;
    }

    fetch(load_url)
        .then((ret) => ret.json())
        .then((data) => fr1xies_parse(data))
        .then((camp) => fr1xies_blit())
        .then(() => {
            // show error
            document.querySelector('#error').hidden = true;
        })
        .catch(() => {
            // hide all except error
            document.querySelector('#people_view').hidden = true;
            document.querySelector('#time_view').hidden = true;
            document.querySelector('#error').hidden = false;
        });

}

/* parse the (server-supplied) data to our internal format:
 * {
 *   start: <luxon datetime of start of first day>,
 *   end: <luxon datetime of end of last day>,
 *   days: [
 *     {
 *       start: <luxon datetime of start of this day>,
 *       end: <luxon datetime of end of this day>,
 *       timespan: <luxon interval of this day>,
 *       posts: [
 *         {
 *           name: <string, name of this post>,
 *           shifts: [
 *             {
 *               start: <luxon datetime of start of this shift>,
 *               end: <luxon datetime of end of this shift>,
 *               timespan: <luxon interval of this shift>,
 *               // meta
 *               n_needed: <int, number of fr1xies needed here>,
 *               ...
 *             },
 *             ...
 *           ]
 *         },
 *         ...
 *       ]
 *     },
 *     ...
 *   ]
 * }
 */
async function fr1xies_parse(data)
{

    camp = {
        days: [],
        people: [],
        // lookup cache
        people_by_code: {},
        shifts_by_handle: {},
        unique_posts: [],
        // outrageous values which'll get tuned down below
        start: luxon.DateTime.fromISO('9999-01-01'),
        end: luxon.DateTime.fromISO('0999-01-01'),
    };

    // save the list of people we have access to
    camp.people = data.people;
    camp.people.forEach((p) => {
        p.shifts = [];
        camp.people_by_code[p.readable_code] = p;
    });

    data.shifts.forEach((s) => {

        let shift = {};

        // internalize it
        shift.start = luxon.DateTime.fromISO(s.start).setZone(camp_tz);
        shift.end = shift.start.plus(luxon.Duration.fromMillis(s.duration*1000));
        shift.timespan = luxon.Interval.fromDateTimes(shift.start, shift.end);
        shift.n_total = s.n_total;
        shift.n_signed_up = s.n_signed_up;
        shift.n_open = s.n_open;
        shift.signed_up = s.signed_up;
        shift.handle = s.handle;
        shift.post = s.post;
        shift.name = s.name;
        shift.visible = s.visible;
        shift.important = s.important;
        shift.has_our_people = false;

        // see if anybody we known subscribed to this shift
        shift.signed_up.forEach((person_code) => {
            if (person_code == 'REDACTED')
                return;
            // one of our people has this shift, note it down in their list
            camp.people_by_code[person_code].shifts.push(shift.handle);
            shift.has_our_people = true;
        });

        // create a day to add this shift to, if needed
        let found_day = camp.days.find((d) => shift.start.toISODate() == d.date);
        if (found_day == undefined) {
            found_day = {
                posts: [],
                date: shift.start.toISODate(),
                start: luxon.DateTime.fromISO('9999-01-01'),
                end: luxon.DateTime.fromISO('0999-01-01'),
            };
            camp.days.push(found_day);
        }
        found_day.start = (shift.start < found_day.start) ? shift.start : found_day.start;
        found_day.end = (found_day.end < shift.end) ? shift.end : found_day.end;
        found_day.timespan = luxon.Interval.fromDateTimes(found_day.start, found_day.end);

        // update those outrageous values above
        camp.start = (shift.start < camp.start) ? shift.start : camp.start;
        camp.end = (camp.end < shift.end) ? shift.end : camp.end;

        // create a post to add this shift to, if needed
        let found_post = found_day.posts.find((p) => s.post == p.post);
        if (found_post == undefined) {
            found_post = {
                post: s.post,
                name: s.name,
                shifts: [],
                visible: false,
            };
            found_day.posts.push(found_post);
        }
        found_post.shifts.push(shift);
        // if a visible shift is added to a post, the post becomes visible
        if (s.visible)
            found_post.visible = true;

        if (!camp.unique_posts.includes(s.post))
            camp.unique_posts.push(s.post);

        camp.shifts_by_handle[shift.handle] = shift;

    });

    camp.days.sort((l, r) => l.start - r.start);

    return camp;

}

/* spin up a <template> with the specified selector, returning
 * a <div> html element containing the cloned template, if an id
 * was supplied that div is assigned that id
*/
function template_spin(template_selector, id=undefined)
{

    let template = document.querySelector(template_selector);
    let clone = template.content.cloneNode(true);

    if (id != undefined) {
        let wrapper = document.createElement('div');
        if (id)
            wrapper.id = id;
        wrapper.appendChild(clone);

        return wrapper;
    } else {
        return clone;
    }

}

/* show the list of people in camp.people */
function fr1xies_blit_people()
{

    // show our main view
    document.querySelector('#people_view').hidden = false;

    let people_list = document.querySelector('#people_list');

    // flush out previous
    while (people_list.hasChildNodes())
        people_list.removeChild(people_list.lastChild);

    camp.people.forEach((person, person_index) => {

        let person_view = template_spin('#person', person.readable_code, wrap=true);

        let name_view = person_view.querySelector('.person_name');
        let expected_view = person_view.querySelector('.person_expected');
        let n_shifts_view = person_view.querySelector('.person_n_shifts');
        let shifts_view = person_view.querySelector('.person_shifts');

        name_view.innerHTML = person.name;
        if (person.expected) {
            expected_view.innerHTML = `heeft aangegeven minstens één shift mee te helpen`;
        } else {
            expected_view.innerHTML = `zou geen shiften meehelpen (maar mag dat altijd natuurlijk)`;
        }

        if (person.expected && (person.shifts.length == 0)) {
            expected_view.classList.add('bg-fri3d_red');
            n_shifts_view.classList.add('bg-fri3d_red');
        }

        n_shifts_view.textContent = person.shifts.length;
        person.shifts.forEach((s) => {
            let shift_view = template_spin('#person_shift');
            let shift = camp.shifts_by_handle[s];
            shift_view.querySelector('.details').innerHTML = `${shift.name} op ${shift.start.toLocal().toLocaleString({ weekday: 'long', month : 'short', day: '2-digit', hour: '2-digit', minute: '2-digit', hourCycle: 'h23' })}, duurt ${Math.round(shift.timespan.length('hours')*10)/10} uur`;
            shifts_view.appendChild(shift_view);
        });

        people_list.appendChild(person_view);
    });

}

function fr1xies_blit()
{

    // show our main view
    document.querySelector('#time_view').hidden = false;

    let needed_width = 50 + camp.unique_posts.length * 150;
    // on request, we minimally need halfish of a 1920px screen
    let min_needed_width = Math.min(needed_width, (1920-10)/2);

    fr1xies_blit_people();

    ((window.innerWidth < min_needed_width) ? fr1xies_blit_list : fr1xies_blit_grid)();

}

/* show an overview of the shifts in list format */
function fr1xies_blit_list()
{

    let time_view = document.querySelector('#time_view');

    while (time_view.hasChildNodes())
        // flush out any previous view
        time_view.removeChild(time_view.lastChild);

    camp.days.forEach((day, day_index) => {

        // load up the generic template for a day, fill in the header
        let generic_day = template_spin('#generic_day', `day_${day_index}`);
        generic_day.querySelector('.generic_day_header').textContent = 
            day.timespan.start.setLocale('nl').toFormat('ccc yyyy-LL-dd');

        let last_shift_start = undefined;

        all_shifts_this_day = [];

        day.posts.forEach((post, post_index) => {
            all_shifts_this_day = all_shifts_this_day.concat(post.shifts);
        });

        // don't show invisible shifts
        if (!show_invisible)
            all_shifts_this_day = all_shifts_this_day.filter((shift) => shift.visible);

        all_shifts_this_day.sort((l, r) => l.start - r.start);

        all_shifts_this_day.forEach((shift, shift_index) => {

            let start = shift.start.toLocal().toLocaleString({hour: '2-digit', minute: '2-digit', hourCycle: 'h23'});
            let end = shift.end.toLocal().toLocaleString({hour: '2-digit', minute: '2-digit', hourCycle: 'h23'});
            let dur = Math.round(shift.timespan.length('hours') * 10) / 10;
            let border = 'border-yellow-900';
            let text = 'text-yellow-900';
            let bg = 'bg-yellow-400';

            if (shift.start.toISO() != last_shift_start) {
                let time_marker = template_spin('#list_shift_time');
                time_marker.querySelector('.list_shift_time').textContent = start;
                generic_day.querySelector('.generic_day_content').appendChild(time_marker);
                last_shift_start = shift.start.toISO();
            }

            let shift_output = document.createElement('div');
            let shift_contents = template_spin('#list_shift');

            shift_contents.querySelector('.list_shift_when').textContent = `${start} tot ${end}`;
            shift_contents.querySelector('.list_shift_dur').textContent = `${dur}u`;
            shift_contents.querySelector('.list_shift_post').textContent = shift.name;
            if (shift.n_open > 0) {
                shift_contents.querySelector('.list_shift_room_number').textContent = shift.n_open;
                shift_contents.querySelector('.list_shift_room').hidden = false;
                shift_contents.querySelector('.list_shift_full').hidden = true;
                if (shift.n_open == shift.n_total) {
                    // totally empty
                    text = 'text-red-900';
                    border = 'border-red-900';
                    bg = 'bg-red-400';
                }
            } else {
                shift_contents.querySelector('.list_shift_room_number').textContent = shift.n_open;
                shift_contents.querySelector('.list_shift_room').hidden = true;
                shift_contents.querySelector('.list_shift_full').hidden = false;
                if (shift.has_our_people) {
                    // full shift, but has our people
                    shift_contents.querySelector('.list_shift_choose').hidden = false;
                } else {
                    // full shift, and none of ours, can't change it
                    shift_contents.querySelector('.list_shift_choose').hidden = true;
                }
                text = 'text-lime-900';
                border = 'border-lime-900';
                bg = 'bg-lime-400';
            }

            if (shift.has_our_people) {
                shift_contents.querySelector('.list_shift_choose').textContent = 'gekozen ✅';
            } else {
                shift_contents.querySelector('.list_shift_choose').textContent = 'schrijf ons op!';
            }
            shift_contents.querySelector('.list_shift_choose').dataset.shiftHandle = shift.handle;
            shift_contents.querySelector('.list_shift_choose').addEventListener('click', (e) => {
                e.preventDefault();
                show_shift_popup(e.target.dataset.shiftHandle);
            });

            [text, border, bg, 'border-2', 'rounded-lg', 'mt-1'].forEach((c) => shift_output.classList.add(c));

            shift_output.appendChild(shift_contents);

            generic_day.querySelector('.generic_day_content').appendChild(shift_output);

        });

        // and add this day to the view
        time_view.appendChild(generic_day);

    });

}

/* show an overview of the shifts in grid format */
function fr1xies_blit_grid()
{

    let time_view = document.querySelector('#time_view');
    let grid_margin = 2; // px

    while (time_view.hasChildNodes())
        // flush out any previous view
        time_view.removeChild(time_view.lastChild);

    function duration_to_offset(duration) {
        // return a CSS string denoting how high this thingy should be given the duration
        let px = duration.as('minutes') * minute_to_px_ratio;
        return px;
    }

    camp.days.forEach((day, day_index) => {

        // load up the generic template for a day, fill in the header
        let generic_day = template_spin('#generic_day', `day_${day_index}`);
        generic_day.querySelector('.generic_day_header').textContent = 
            day.timespan.start.toLocal().toLocaleString({weekday: 'long', month : 'short', day: '2-digit'});


        // we'll assemble the day in day_output, and later on add it to generic_day
        let day_output = document.createElement('div');
        day_output.classList.add('day');

        // the first lane is for the ruler
        let ruler_lane_output = document.createElement('div');
        ruler_lane_output.classList.add('ruler_lane');

        // the ruler itself (we might go to more than one ruler per lane)
        let ruler_output = document.createElement('div');
        ruler_output.classList.add('ruler');
        // scale it to height for all ticks in this day
        ruler_output.style = `height: ${duration_to_offset(day.timespan.toDuration())}px;`;

        // generate ticks, each representing ruler_tick_granularity of time
        let tick_duration = luxon.Duration.fromISOTime(ruler_tick_granularity);
        // right now, start at day start
        let tick_cursor = day.start;

        while (day.timespan.contains(tick_cursor)) {
            // keep generating ticks until we're out of this day
            let tick = document.createElement('div');
            tick.classList.add('tick');
            tick.style = `top: ${duration_to_offset(tick_cursor.diff(day.start))}px;`;
            tick.appendChild(document.createTextNode(tick_cursor.toFormat('HH:mm')));
            ruler_output.appendChild(tick);

            tick_cursor = tick_cursor.plus(tick_duration);
        }

        // add the ruler and its lane to the output
        ruler_lane_output.appendChild(ruler_output);
        day_output.appendChild(ruler_lane_output);

        day.posts.forEach((post, post_index) => {
            // for each post, generate its own lane and start adding the shifts

            // whole post invisible? don't blit it
            if (!show_invisible && !post.visible)
                return;

            // again a lane which could house more than one post (if we start
            // clustering the post's shifts for example)
            let post_lane_output = document.createElement('div');
            post_lane_output.classList.add('post_lane');

            // our one post for now
            let post_output = document.createElement('div');
            post_output.classList.add('post');
            post_output.style = `height: ${duration_to_offset(day.timespan.toDuration())}px`;

            post.shifts.forEach((shift, shift_index) => {
                // now tack on the shifts as individual blocks of appropriate size

                // don't show invisible shifts
                if (!show_invisible && !shift.visible)
                    return;

                let shift_output = document.createElement('div');
                shift_output.classList.add('shift');
                shift_output.style = `top: ${duration_to_offset(shift.start.diff(day.start))}px;\nheight: ${duration_to_offset(shift.timespan.toDuration())-grid_margin}px;`;
                let border = 'border-yellow-900';
                let text = 'text-yellow-900';
                let bg= 'bg-yellow-400';
                let time = shift.start.toLocal().toLocaleString({hour: '2-digit', minute: '2-digit', hourCycle: 'h23'});
                let dur = Math.round(shift.timespan.length('hours') * 10) / 10;

                let shift_contents = template_spin('#grid_shift');

                shift_contents.querySelector('.grid_shift_header').textContent = `${time} (voor ${Math.round(shift.timespan.length('hours')*10)/10}u)`;
                shift_contents.querySelector('.grid_shift_post').textContent = post.name;
                if (shift.n_open > 0) {
                    shift_contents.querySelector('.grid_shift_room_number').textContent = shift.n_open;
                    shift_contents.querySelector('.grid_shift_room').hidden = false;
                    shift_contents.querySelector('.grid_shift_full').hidden = true;
                    if (shift.n_open == shift.n_total) {
                        // totally empty
                        text = 'text-red-900';
                        border = 'border-red-900';
                        bg = 'bg-red-400';
                    }
                } else {
                    shift_contents.querySelector('.grid_shift_room_number').textContent = shift.n_open;
                    shift_contents.querySelector('.grid_shift_room').hidden = true;
                    shift_contents.querySelector('.grid_shift_full').hidden = false;
                    if (shift.has_our_people) {
                        // full shift, but has our people
                        shift_contents.querySelector('.grid_shift_choose').hidden = false;
                    } else {
                        // full shift, and none of ours, can't change it
                        shift_contents.querySelector('.grid_shift_choose').hidden = true;
                    }
                    text = 'text-lime-900';
                    border = 'border-lime-900';
                    bg = 'bg-lime-400';
                }

                if (shift.has_our_people) {
                    shift_contents.querySelector('.grid_shift_choose').textContent = 'gekozen ✅';
                } else {
                    shift_contents.querySelector('.grid_shift_choose').textContent = 'schrijf ons op!';
                }
                shift_contents.querySelector('.grid_shift_choose').dataset.shiftHandle = shift.handle;
                shift_contents.querySelector('.grid_shift_choose').addEventListener('click', (e) => {
                    e.preventDefault();
                    show_shift_popup(e.target.dataset.shiftHandle);
                });

                [text, border, bg, 'border-2', 'rounded-lg'].forEach((c) => shift_output.classList.add(c));

                shift_output.appendChild(shift_contents);

                post_output.appendChild(shift_output);
            });

            post_lane_output.appendChild(post_output);

            day_output.appendChild(post_lane_output);
        });

        // now splice everything into this day
        generic_day.querySelector('.generic_day_content').appendChild(day_output);

        // and add this day to the view
        time_view.appendChild(generic_day);

    });

}


function show_shift_popup(shift_handle)
{

    let shift = camp.shifts_by_handle[shift_handle];

    document.querySelectorAll('.popup').forEach((p) => p.classList.add('hidden'));

    let popup = document.querySelector('#popup_shift');
    let body = document.querySelector('#js_body');
    let dur = Math.round(shift.timespan.length('hours') * 10) / 10;
    let when = shift.start.toLocal().toLocaleString({ weekday: 'long', month : 'short', day: '2-digit', hour: '2-digit', minute: '2-digit', hourCycle: 'h23' })


    popup.querySelector('#popup_shift_close').addEventListener('click', (e) => {
        e.preventDefault();
        popup.classList.add('hidden');
        body.classList.remove('overflow-hidden');
        reload_camp();
    });

    popup.querySelector('.popup_shift_post').textContent = shift.name;
    popup.querySelector('.popup_shift_when').textContent = `op ${when}`;
    popup.querySelector('.popup_shift_duration').textContent = `duurt ${dur} uur`;

    let people_list = popup.querySelector('.popup_shift_people');
    while (people_list.hasChildNodes())
        people_list.removeChild(people_list.lastChild);

    let recalc_checkboxes = () => {

        camp.people.forEach((person) => {

            let checkbox = document.querySelector(`#person_${person.readable_code} .popup_shift_person_check`);

            let person_available = true;
            for (let shift_idx in person.shifts) {
                let other_shift = camp.shifts_by_handle[person.shifts[shift_idx]];
                if (shift.timespan.overlaps(other_shift.timespan)) {
                    person_available = false;
                    break;
                }
            }

            if (shift.signed_up.length >= shift.n_total) {
                person_available = false;
            }

            checkbox.disabled = checkbox.checked ? false : !person_available;

        });

    };

    camp.people.forEach((person) => {
        let person_view = template_spin('#popup_shift_person', `person_${person.readable_code}`);

        let name_view = person_view.querySelector('.popup_shift_person_name');
        let check_view = person_view.querySelector('.popup_shift_person_check');

        name_view.textContent = person.name;

        check_view.checked = shift.signed_up.includes(person.readable_code);
        check_view.dataset.personReadableCode = person.readable_code;
        check_view.dataset.shiftHandle = shift_handle;
        check_view.addEventListener('change', (event) => {
            if (event.target.checked == true) {
                shift.signed_up.push(person.readable_code);
                person.shifts.push(shift_handle);
                recalc_checkboxes();
                fetch(`/api/community/shift/${shift_handle}/join/${person.readable_code}`).then((r) => {
                    if (!r.ok) {
                        let error = popup.querySelector('.popup_shift_error');
                        error.textContent = 'Dit ging mis! Iemand deed misschien hetzelfde. Herlaad even?';
                        error.hidden = false;
                    }
                });
            } else {
                shift.signed_up = shift.signed_up.filter(code => code !== person.readable_code);
                person.shifts = person.shifts.filter(handle => handle !== shift.handle);
                recalc_checkboxes();
                fetch(`/api/community/shift/${shift_handle}/leave/${person.readable_code}`).then((r) => {
                    if (!r.ok) {
                        let error = popup.querySelector('.popup_shift_error');
                        error.textContent = 'Dit ging mis! Iemand deed misschien hetzelfde. Herlaad even?';
                        error.hidden = false;
                    }
                });

            }
        });

        people_list.appendChild(person_view);
    });

    recalc_checkboxes();

    popup.classList.remove('hidden');
    body.classList.add('overflow-hidden');

}
