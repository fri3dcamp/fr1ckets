document.addEventListener('DOMContentLoaded', function(event) {
	document.querySelector("#js_filtersession").addEventListener('change', event => {
		switchSession(event.target.value);
	});
	let selectedsession = document.querySelector("#js_filtersession").value;
	console.log(selectedsession);
	switchSession(selectedsession);
});

function switchSession(t){
	sessions = getSessions();
	for (s in sessions) {
		if (t === sessions[s] || 'all' === t) {
			document.querySelectorAll("."+sessions[s]).forEach( e => {
				e.classList.remove("hidden");
			});
		} else {
			document.querySelectorAll("."+sessions[s]).forEach( e => {
				e.classList.add("hidden");
			});
		}
	}
	console.log(t);

	// showhide tickets
	let hastickets = false;
	document.querySelectorAll("#tickets .js_ticket").forEach( e => {
		if (! e.classList.contains('hidden')) {
			hastickets = true;
		}
	});
	if (true === hastickets) {
		document.querySelector("#tickets").classList.remove('hidden');
	} else {
		document.querySelector("#tickets").classList.add('hidden');
	}

	// showhide stay_options
	let hasoptions = false;
	document.querySelectorAll("#stay_options .js_contoline").forEach( e => {
		if (! e.classList.contains('hidden')) {
			hasoptions = true;
		}
	});
	if (true === hasoptions) {
		document.querySelector("#stay_options").classList.remove('hidden');
	} else {
		document.querySelector("#stay_options").classList.add('hidden');
	}

	// showhide extras
	let hasextras = false;
	document.querySelectorAll("#extras .js_contoline").forEach( e => {
		if (! e.classList.contains('hidden')) {
			hasextras = true;
		}
	});
	if (true === hasextras) {
		document.querySelector("#extras").classList.remove('hidden');
	} else {
		document.querySelector("#extras").classList.add('hidden');
	}

	// showhide remarks
	let hasremarks = false;
	document.querySelectorAll("#remarks .js_remark").forEach( e => {
		if (! e.classList.contains('hidden')) {
			hasremarks = true;
		}
	});
	if (true === hasremarks) {
		document.querySelector("#remarks").classList.remove('hidden');
	} else {
		document.querySelector("#remarks").classList.add('hidden');
	}
}

function getSessions() {
	let sessions = []
	document.querySelectorAll("#js_filtersession option").forEach( e => {
		sessions.push(e.value);
	} );
	return sessions;
}