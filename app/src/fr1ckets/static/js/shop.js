document.addEventListener('DOMContentLoaded', function(event) {

	conto = {};
	known_vouchers = {};
    // beschrijvingen popups
    document.querySelectorAll('.js_opener').forEach(item => {
		item.addEventListener('click', event => {
			event.preventDefault();
			let myhref = item.href.split("#");
			let toshow = document.querySelector("#"+myhref[1])
			toshow.classList.remove("sr-only");
			document.querySelector("#js_body").classList.add("overflow-hidden");
		})
	});

    // image zoom
    document.querySelectorAll('.js_image_zoom').forEach(item => {
		item.addEventListener('click', event => {
			event.preventDefault();
			let imgurl = item.href;
			let toshow = document.querySelector("#js_imgzoom");
            toshow.querySelector("img").src = imgurl;
			toshow.classList.remove("sr-only");
			document.querySelector("#js_body").classList.add("overflow-hidden");
		})
	});

    // popup close
    document.querySelectorAll('.js_imgpopbg').forEach(item => {
		item.addEventListener('click', event => {
			event.preventDefault();
			tel = document.querySelector("#" + item.dataset.target);
			tel.classList.add("sr-only");
			document.querySelector("#js_body").classList.remove("overflow-hidden");
		})
	});
    document.querySelectorAll('.js_popup_hide').forEach(item => {
		item.addEventListener('click', event => {
			event.preventDefault();
			item.classList.add("hidden");
			document.querySelector("#js_body").classList.remove("overflow-hidden");
		})
	});

	// checkboxes at bottom
    document.querySelectorAll('.js_lastcheck').forEach(item => {
		item.addEventListener('change', event => {
			maybeDisableButton();
		})
	});

    // check changes
    document.querySelector("#js_body").addEventListener('click', event => {
        makeConto();
    });
    document.querySelector("input").addEventListener('change', event => {
        makeConto();
    });
    document.querySelector("textarea").addEventListener('change', event => {
        makeConto();
    });
    document.querySelector("select").addEventListener('change', event => {
        makeConto();
    });

	document.querySelector("#vouchers").addEventListener('change', event => {
        makeConto();
    });

	document.querySelector(".js_vouchercode input").addEventListener('keyup', event => {
        voucherFields(false);
    });

	document.querySelector(".js_vouchercode input").addEventListener('change', event => {
        voucherFields(true);
    });
	document.querySelector(".js_vouchercode input").addEventListener('blur', event => {
        voucherFields(true);
    });
	voucherFields(true);
	makeConto();

	// check button
	document.querySelector("#js_check").addEventListener('click', event => {
        event.preventDefault();
		console.log("--- getting a quote for ---")
		console.log(conto);
		createQuote(conto);
    });

	maybeDisableButton();

});

function collectVoucherCodes() {
	allcodes = [];
	document.querySelectorAll('.js_vouchercode').forEach( item => {
		let vval = item.querySelector("input").value.trim();
		if ('' !== vval) {
			allcodes.push(vval);
		}
	} );
	out = allcodes.filter((element, index, out) => out.indexOf(element) === index);

	return out;
}

function voucherFields(checkvals=false) {
	let voucherels = document.querySelectorAll('.js_vouchercode');
	let n_vouchers = voucherels.length;
	let vouchercodes = collectVoucherCodes();

	let target_n = vouchercodes.length + 1;
	

	if (n_vouchers < target_n) {
		for ( var i = 0; i < (target_n - n_vouchers); i++) {
			newel = voucherels[0].cloneNode(true);
			
			newel.querySelector("input").addEventListener('keyup', event => {
				voucherFields(false);
			});
			newel.querySelector("input").addEventListener('change', event => {
				voucherFields(true);
			});
			newel.querySelector("input").addEventListener('blur', event => {
				voucherFields(true);
			});
			document.querySelector("#vouchercodes").append(newel);
			newel.querySelector("input").value = '';
		}
	}
	while (target_n < n_vouchers) {
		document.querySelector('#vouchercodes').lastChild.remove();
		n_vouchers -= 1;
	}
	document.querySelectorAll('.js_vouchercode').forEach( item => {
		item.querySelector("input").value = '';
	} );
	for (var i = 0; i < vouchercodes.length; i++) {
		
		voucherels[i].querySelector("input").value = vouchercodes[i];
	}

	document.querySelectorAll('.js_vouchercode').forEach( item => {
		checkVoucher(item,checkvals);
	} );
}

function showHideVouchers() {
	if ( document.querySelector("#vouchers").checked) {
		document.querySelector("#vouchercodes").classList.remove("hidden");
	} else {
		document.querySelector("#vouchercodes").classList.add("hidden");
	}
}

function setVoucherMessages() {

	document.querySelectorAll('.js_vouchercode').forEach(item => {
		vcode = item.querySelector("input").value.trim();
		if ( known_vouchers.hasOwnProperty(vcode)) {
			item.querySelector(".js_voucher_msg").classList.remove('hidden');
			if (known_vouchers[vcode]['found']) {
				item.querySelector("input").classList.remove("border-red-500");
				item.querySelector("input").classList.remove("text-red-950");
				item.querySelector("input").classList.remove("bg-red-200");
				item.querySelector("input").classList.add("border-green-700");
				item.querySelector("input").classList.add("bg-green-200");
				item.querySelector(".js_voucher_msg").classList.add("bg-green-200");
				item.querySelector(".js_voucher_msg").classList.remove("bg-red-200");
			} else {
				item.querySelector("input").classList.add("border-red-500");
				item.querySelector("input").classList.add("text-red-950");
				item.querySelector("input").classList.add("bg-red-200");
				item.querySelector("input").classList.remove("border-green-700");
				item.querySelector("input").classList.remove("bg-green-200");
				item.querySelector(".js_voucher_msg").classList.remove("bg-green-200");
				item.querySelector(".js_voucher_msg").classList.add("bg-red-200");
			}
			item.querySelector(".js_voucher_msg").textContent = known_vouchers[vcode]['msg'];
		} else {
			item.querySelector("input").classList.remove("border-red-500");
			item.querySelector("input").classList.remove("text-red-950");
			item.querySelector("input").classList.remove("border-green-700");
			item.querySelector("input").classList.remove("bg-red-200");
			item.querySelector("input").classList.remove("bg-green-200");
			item.querySelector(".js_voucher_msg").classList.add('hidden');
			item.querySelector(".js_voucher_msg").textContent = '';
		}
	});
}

async function checkVoucher(el,checkremote=true) {
	
	let vcode = el.querySelector("input").value.trim();
	
	if ('' === vcode) {
		setVoucherMessages();
		return false;
	}
	if ( known_vouchers.hasOwnProperty(vcode)) {

		setVoucherMessages();
		return false;
	}
	if ( true === checkremote ) {
		
		let url = "/api/shop/voucher";
		let other_params = {
			headers : { "content-type" : "application/json; charset=UTF-8"},
			body : JSON.stringify({'code':vcode}),
			method : "POST",
			mode : "cors"
		};

		fetch(url, other_params)
			.then(function(response) {
			if (response.ok) {
				return response.json();
			} else {
				throw new Error("Could not reach the API: " + response.statusText);
			}
		}).then(function(data) {

			known_vouchers[vcode] = {};
			if (data['found']){
				known_vouchers[vcode]['msg'] = "Dit is een geldige voucher voor een " + data['applicable_to'];
			} else {
				known_vouchers[vcode]['msg'] = "Dit is geen geldige voucher";
			}
			known_vouchers[vcode]['found'] = data['found'];
			known_vouchers[vcode]['applicable_to'] = data['applicable_to'];
			
			setVoucherMessages();
			makeConto();

		}).catch(function(error) {
			console.log(error);
			
		});
	}
	return true;
}

async function createQuote(c) {
	c['email'] = document.querySelector("#js_email").dataset.email;

	let url = "/api/shop/quote";
	let other_params = {
		headers : { "content-type" : "application/json; charset=UTF-8"},
		body : JSON.stringify(c),
		method : "POST",
		mode : "cors"
	};

	fetch(url, other_params)
	.then(r =>  r.json().then(data => ({status: r.status, body: data})))
	.then(obj => handleResponse(obj,'quote'));
}

function handleResponse(obj,t) {
	if (200===obj.status) {
		if ('quote' === t) {
			showContoPreview(obj['body']);
		}
		if ('order' === t) {
			showThankYou(obj['body']);
		}
	} else {
		showError(obj);
	}
}

async function createOrder(c) {
	c['email'] = document.querySelector("#js_email").dataset.email;

	let url = "/api/shop/order";
	let other_params = {
		headers : { "content-type" : "application/json; charset=UTF-8"},
		body : JSON.stringify(c),
		method : "POST",
		mode : "cors"
	};

	fetch(url, other_params)
	.then(r =>  r.json().then(data => ({status: r.status, body: data})))
	.then(obj => handleResponse(obj,'order'));
}

function voucherErrorDetail(extra){
	out = "<br/>";
	if ('expired' in extra) {
		out += " Vervallen vouchers: " + extra['expired'].join(', ') + ".";
	}
	if ('notfound' in extra) {
		out += " Onbekende voucher: " + extra['notfound'].join(', ') + ".";
	}
	if ('superfluous' in extra) {
		out += " Vouchers die niet kunnen toegepast worden op een product in de bestelling: " + extra['superfluous'].join(', ') + ".";
	}
	return out;
}

function showError(resp) {

	document.querySelectorAll('.js_popup_hide').forEach(item => {
		item.classList.add('hidden');
	});

	let errorcode = resp['body']['message'];
	let textcontent = "Er was een onbekende fout (" + errorcode + ")"
	switch (errorcode) {
		case 'EVOUCHERBAD':
			textcontent = "Een of meer vouchers kunnen niet toegepast worden.";
			textcontent += voucherErrorDetail(resp['body']['extra']);
			break;
		case 'EBADVALUES':
			textcontent = "Een of meer velden bevatten ongeldige waarden.";
			break;
		case 'EDISCLAIMERS':
			textcontent = "De disclaimers onderaan het bestelformulier werden niet aangevinkt.";
			break;
		case 'ETOOSOON':
			textcontent = "Je mag met dit e-mailadres nog geen bestelling plaatsen.";
			break;
		case 'ECARTEMPTY':
			textcontent = "Bestelling zonder producten.";
			break;
		case 'EPRODUCTUNKNOWN':
			textcontent = "Minstens een product in je bestelling bestaat niet.";
			break;
		case 'EVARIANTUNKNOWN':
			textcontent = "Minstens een productvariant in je bestelling bestaat niet.";
			break;
		case 'EVARIANTNEEDED':
			textcontent = "Verplichte variantkeuze ontbrak bij minstens een product.";
			break;
		case 'EFIELDNEEDED':
			textcontent = "Een of meer verplichte velden ontbrak.";
			break;
		default:
			console.log("Onbekende fout");
	}

	document.querySelector("#js_popup_error_text").innerHTML = textcontent;
	document.querySelector("#js_popup_error").classList.remove('hidden');
    document.querySelector("#js_popup_error_close").addEventListener('click', event => {
		event.preventDefault();
		document.querySelector("#js_popup_error").classList.add("hidden");
		document.querySelector("#js_body").classList.remove("overflow-hidden");
	});

	document.querySelector("#js_body").classList.add("overflow-hidden");

}

function showThankYou(data) {
	let allemails = [];
	allemails.push(document.querySelector("#js_email").dataset.email );

	for (var i = 0; i < data['products'].length; i++) {
		for (var j = 0; j < data['products'][i]['fields'].length; j++) {
			let f = data['products'][i]['fields'][j];
			if ('email' === f['name']) {
				let email = f['value'];
				if ('' !== email) {
					allemails.push(email);
				}
			}
		}
	}
	emails = allemails.filter((element, index, emails) => emails.indexOf(element) === index);

	document.querySelector(".js_thanks_emails").textContent = emails.join(', ');
	document.querySelector(".js_paydeadline").textContent = data['deadline_display']
	document.querySelector("#js_body").classList.add("overflow-hidden");
	document.querySelector("#js_popup_thanks").classList.remove('hidden');
	document.querySelector("#js_popup_thanks_close").setAttribute("href","/session/"+data['nonce'])

}

function VATratesToSingleValue(prices) {
	let out = 0;
	for (const rate in prices) {
		out += prices[rate];
	}
	return out;
}

function makeVATString(prices) {
	let base = "(incl. BTW";
	let actualprices = {};

	for (const rate in prices) {
		if (prices[rate] > 0) {
			actualprices[rate] = prices[rate];
		}
	}
	vatrates_n = Object.keys(actualprices).length;

	if (0 == vatrates_n) {
		return base + ")";
	}
	if (1 == vatrates_n) {
		let rate = Object.keys(actualprices)[0]
		return "(incl. " + rate + "% BTW)";
	}
	let vatrates = [];
	for (const rate in actualprices) {
		p = parseFloat(actualprices[rate]/100);
		vatrates.push("€ " + p + " à "+rate+"%")
	}
	let vatstr = vatrates.join(', ');
	return base + ": "+vatstr + ")";

}

function showContoPreview(data) {
	let popupid ="js_popup_conto"

	let parent = document.querySelector("#js_body");
	let child = document.getElementById(popupid);
	if (null !== child) {
		let throwawayNode = parent.removeChild(child);
	}

	contopop = document.querySelector('#js_popup_conto_template').cloneNode(true);
	contopop.id = popupid;
	contopop.classList.remove("hidden");

	contopop.querySelector("#js_dump").textContent = JSON.stringify(data);

	contopop.querySelector("#js_conto_total").textContent = parseFloat(data['total']) / 100;

	let extras1 = {};
	let extras2 = {};

	for (var i = 0; i < data['products'].length; i++) {
		let p = data['products'][i]
		let tprice = parseFloat(VATratesToSingleValue(p['price'])/100);
		let tvat = makeVATString(p['price']);
		let tlate = 'no';
		let tearly = 'no';
		let notask = 'no';
		let othertask = 'no';
		let notes = '';
		let email = '';
		let tname = '';
		let tage = 0
		let bits = ''
		
		for (var j = 0; j < p['variants'].length; j++) {
			let v = p['variants'][j];
			if ('bits' === v['name']) {
				ttype = v['value'];
				if (ttype === '8bit') {
					task = false;
				}
			}
			if ('food' === v['name']) {
				tfood = v['value'];
			}
			if ('late' === v['name']) {
				tlate = v['value'];
			}
			if ('early' === v['name']) {
				tearly = v['value'];
			}
			if ('notask' === v['name']) {
				notask = v['value'];
			}
			if ('othertask' === v['name']) {
				othertask = v['value'];
			}
			if ('bits' === v['name']) {
				bits = v['value'];
			}
		} 
		for (var j = 0; j < p['fields'].length; j++) {
			let f = p['fields'][j];
			if ('name' === f['name']) {
				tname = f['value'];
			}
			if ('age' === f['name']) {
				tage = f['value'];
			}
			if ('notes' === f['name']) {
				notes = f['value'];
			}
			if ('email' === f['name']) {
				email = f['value'];
			}
		}

		if ('ticket' === p.name) {
			let voucherapplied = false
			if (p.hasOwnProperty('voucher')) {
				voucherapplied = Boolean(p.voucher);
			}
			contopop.querySelector("#js_geentickets").classList.add('hidden');
			ticketline = contopop.querySelector('#js_conto_ticket').cloneNode(true);
			ticketline.classList.remove("hidden");
			ticketline.querySelector(".js_conto_ticket_name").textContent = tname;
			ticketline.querySelector(".js_conto_ticket_age").textContent = tage;
			ticketline.querySelector(".js_conto_ticket_price").textContent = tprice;
			ticketline.querySelector(".js_conto_ticket_vat").textContent = tvat;
			ticketline.querySelector(".js_conto_ticket_type").textContent = ttype;
			if (voucherapplied) {
				
				ticketline.querySelector(".js_conto_has_voucher").classList.remove('hidden');
				
			}
			if ('vegetarian' === tfood) {
				ticketline.querySelector(".js_conto_ticket_veggie").classList.remove('hidden');
			} else {
				ticketline.querySelector(".js_conto_ticket_veggie").classList.add('hidden');
			}
			if ('yes' === tearly) {
				ticketline.querySelector(".js_conto_ticket_early").classList.remove('hidden');
			} 
			if ('yes' === tlate) {
				ticketline.querySelector(".js_conto_ticket_late").classList.remove('hidden');
			}
			if ('9bit' === bits) {
				ticketline.querySelector(".js_conto_ticket_invoice").classList.remove('hidden');
			}
			if ('yes' === notask) {
				ticketline.querySelector(".js_conto_ticket_task_nok").classList.remove('hidden');
				ticketline.querySelector(".js_conto_ticket_task_ok").classList.add('hidden');
			} else {
				ticketline.querySelector(".js_conto_ticket_task_nok").classList.add('hidden');
				ticketline.querySelector(".js_conto_ticket_task_ok").classList.remove('hidden');
			}
			if (parseInt(tage) < 16) {
				ticketline.querySelector(".js_conto_ticket_task_nok").classList.add('hidden');
				ticketline.querySelector(".js_conto_ticket_task_ok").classList.add('hidden');
			}
			if ('yes' === othertask) {
				ticketline.querySelector(".js_conto_ticket_task_difficult").classList.remove('hidden');
			}
			if ('' !== notes) {
				ticketline.querySelector(".js_conto_ticket_notewrapper").classList.remove('hidden');
				ticketline.querySelector(".js_note").textContent = notes;
			}
			if ('' !== email) {
				ticketline.querySelector(".js_conto_ticket_email").textContent = ' - ' + email;
			}
			contopop.querySelector("#js_conto_ticketswrapper").append(ticketline);
		} else if ('room' === p.name || 'bed' === p.name || 'camper' === p.name) {
			if ( ! extras1.hasOwnProperty(p.name)) {
				
				extras1[p.name] = {
					'description' : p.description,
					'n' : 0,
					'price' : 0,
					'voucher' : false,
					'prices' : {}
				}
			}
			extras1[p.name]['n'] += 1;
			extras1[p.name]['price'] += VATratesToSingleValue(p.price);

			for (const r in p.price) {
				if (! extras1[p.name]['prices'].hasOwnProperty(r)) {
					extras1[p.name]['prices'][r] = 0;
				}
				extras1[p.name]['prices'][r] += p.price[r];
			}
			if (p.hasOwnProperty('voucher') && true === Boolean(p.voucher)) {
				extras1[p.name]['voucher'] = true;
			}
		} else {
			if (p.variants.length > 0) {
				pname = p.name + "|" + p.variants[0].value;
				pdesc = p.description + " (" + p.variants[0].value + ")";
			} else {
				pname = p.name;
				pdesc = p.description;
			}
			if ( ! extras2.hasOwnProperty(pname)) {
				
				extras2[pname] = {
					'description' : pdesc,
					'n' : 0,
					'price' : 0,
					'voucher' : false,
					'prices' : {}
				}
			}
			extras2[pname]['n'] += 1;
			extras2[pname]['price'] += VATratesToSingleValue(p.price);
			for (const r in p.price) {
				if (! extras2[pname]['prices'].hasOwnProperty(r)) {
					extras2[pname]['prices'][r] = 0;
				}
				extras2[pname]['prices'][r] += p.price[r];
			}
			if (p.hasOwnProperty('voucher') && true === Boolean(p.voucher)) {
				extras2[pname]['voucher'] = true;
			}
		}
	}

	for (const item in extras1) {

		contopop.querySelector("#js_geenextras1").classList.add('hidden');
		
		itemline = contopop.querySelector('#js_conto_ticketextra').cloneNode(true);
		
		itemline.classList.remove("hidden");
		if (true === extras1[item]['voucher']) {
			
			itemline.querySelector(".js_conto_has_voucher").classList.remove('hidden');
			
		}
		itemline.querySelector(".js_conto_ticketextra_n").textContent = extras1[item]['n'];
		itemline.querySelector(".js_conto_ticketextra_description").textContent = extras1[item]['description'];
		itemline.querySelector(".js_conto_ticketextra_price").textContent = extras1[item]['price'] / 100;
		itemline.querySelector(".js_conto_ticketextra_vat").textContent = makeVATString( extras1[item]['prices']);
		contopop.querySelector("#js_conto_extras1").append(itemline);
	}
	for (const item in extras2) {
		contopop.querySelector("#js_geenextras2").classList.add('hidden');
		
		itemline = contopop.querySelector('#js_conto_ticketextra').cloneNode(true);
		
		itemline.classList.remove("hidden");
		if (true === extras2[item]['voucher']) {
			
			itemline.querySelector(".js_conto_has_voucher").classList.remove('hidden');
			
		}
		itemline.querySelector(".js_conto_ticketextra_n").textContent = extras2[item]['n'];
		itemline.querySelector(".js_conto_ticketextra_description").textContent = extras2[item]['description'];
		itemline.querySelector(".js_conto_ticketextra_price").textContent = extras2[item]['price'] / 100;
		itemline.querySelector(".js_conto_ticketextra_vat").textContent = makeVATString(extras2[item]['prices']);
		contopop.querySelector("#js_conto_extras2").append(itemline);
	}
	// Remarks toevoegen
	if (Boolean(data['notes'])) {
		contopop.querySelector("#js_conto_remarkswrapper").classList.remove("hidden");
		contopop.querySelector("#js_conto_remarks").textContent = data['notes'];
	}

	contopop.classList.remove("hidden");

	contopop.querySelector(".js_popup_conto_confirm").addEventListener('click', event => {
        event.preventDefault();
		
		createOrder(conto);
		contopop.classList.add("hidden");
    });

	contopop.querySelector(".js_popup_conto_close").addEventListener('click', event => {
        event.preventDefault();
		
		contopop.classList.add("hidden");
    });

    document.querySelector("#js_body").append(contopop);

	document.querySelector("#js_body").classList.add("overflow-hidden");
}

function disclaimerFields() {
	return {
		'disclaimer_have_accompanying_adult' : document.querySelector("#js_lastcheck_age").checked,
		'disclaimer_be_excellent' :document.querySelector("#js_lastcheck_excellent").checked,
		'disclaimer_animal' : document.querySelector("#js_lastcheck_animal").checked,
	};
}

function allChecks() {
	out = {
		'disclaimerfields' : disclaimerFields(),
		'alldisclaimers' : true,
		'ticketages' : true,
		'ticketnames' : true,
		'valid' : true
	}
	tickets = document.querySelector("#js_ticketholders_inner").querySelectorAll('[id^="js_ticketholder_"]');
	console.log("nr of tickets " + tickets.length);
	if (0 === tickets.length) {
		out['ticketages'] = true;
		out['ticketnames'] = true;
	}
	for (var i = 0; i < tickets.length; i++) {
		for (var i = 0; i < tickets.length; i++) {
			if (isNaN(parseInt(document.querySelector("#ticketage_"+i).value))) {
				out['ticketages'] = false;
				out['valid'] = false;
			}
			if ("" === document.querySelector("#ticketname_"+i).value) {
				out['ticketnames'] = false;
				out['valid'] = false;
			}
		}
	}

	for (let [key, value] of Object.entries(out['disclaimerfields'])) {
		if (true !== value) {
			out['alldisclaimers'] = false;
			out['valid'] = false;
			break;
		}
	}


	return out
}

function maybeDisableButton() {
	enabled = allChecks();
	if (enabled['valid']) {
		document.querySelector("#js_required_notice").classList.add("hidden");
		document.querySelector("#js_check").removeAttribute("disabled");
	} else {
		if (true === enabled['alldisclaimers']) {
			document.querySelector("#js_required_disclaimers").classList.add("hidden");
		} else {
			document.querySelector("#js_required_disclaimers").classList.remove("hidden");
		}
		if (true === enabled['ticketages']) {
			document.querySelector("#js_required_ages").classList.add("hidden");
		} else {
			document.querySelector("#js_required_ages").classList.remove("hidden");
		}
		if (true === enabled['ticketnames']) {
			document.querySelector("#js_required_names").classList.add("hidden");
		} else {
			document.querySelector("#js_required_names").classList.remove("hidden");
		}
		document.querySelector("#js_required_notice").classList.remove("hidden");
		document.querySelector("#js_check").setAttribute("disabled", "disabled");
	}
}

function collectTickets() {
	let ticketsobj = []
	let totalprice = 0
	tickets = document.querySelector("#js_ticketholders_inner").querySelectorAll('[id^="js_ticketholder_"]');
	for (var i = 0; i < tickets.length; i++) {

		age = parseInt(document.querySelector("#ticketage_"+i).value);
		tourist = document.querySelector("#ticketholder_tourist_"+i).checked;
		biz = document.querySelector("#ticketholder_business_"+i).checked;
		price = calcTicketPrice(age,biz,tourist);
		totalprice += price[0];
		food = "omnivore";
		late = "no";
		early = "no";
		othertask = "no";
		notask = "no";

		if (age < 16) {
			document.querySelector("#wrapper_ticketholder_tourist_"+i).classList.add('hidden');
			document.querySelector("#wrapper_ticketholder_othertask_"+i).classList.add('hidden');
			document.querySelector("#wrapper_ticketholder_late_"+i).classList.add('hidden');
			document.querySelector("#wrapper_ticketholder_early_"+i).classList.add('hidden');
		} else {
			document.querySelector("#wrapper_ticketholder_tourist_"+i).classList.remove;('hidden');
			document.querySelector("#wrapper_ticketholder_othertask_"+i).classList.remove('hidden');
			document.querySelector("#wrapper_ticketholder_late_"+i).classList.remove('hidden');
			document.querySelector("#wrapper_ticketholder_early_"+i).classList.remove('hidden');

		}

		if ( document.querySelector("#ticketholder_early_"+i).checked ) {
			early = "yes";
		};

		if ( document.querySelector("#ticketholder_late_"+i).checked ) {
			late = "yes";
		};

		if ( document.querySelector("#ticketholder_othertask_"+i).checked ) {
			othertask = "yes";
		};

		if ( document.querySelector("#ticketholder_tourist_"+i).checked ) {
			notask = "yes";
		};

		if (document.querySelector("#ticketdiet_"+i).checked) {
			food = 'vegetarian';
		}
		ticketobj = {
			"product_handle" : document.querySelector("#js_ticketholders").dataset.handle,
			"product_name" : document.querySelector("#js_ticketholders").dataset.pname,
			"product_category" : document.querySelector("#js_ticketholders").dataset.pcategory,
			"price" : price[0],
			"tier" : price[1],
			"voucher" : false,
			"fields" : [
				{
					"name" : "name",
					"value" :  document.querySelector("#ticketname_"+i).value
				},
				{
					"name" : "notes",
					"value" :  document.querySelector("#ticketnotes_"+i).value
				},
				{
					"name" : "age",
					"value" : "" + age
				},
				{
					"name" : "email",
					"value" :  document.querySelector("#ticketemail_"+i).value
				}
			],
			"variants" : [
				{
					"name" : "bits",
					"value" : price[1]
				},
				{
					"name" : "food",
					"value" : food
				},
				{
					"name" : "late",
					"value" : late
				},
				{
					"name" : "early",
					"value" : early
				},
				{
					"name" : "notask",
					"value" : notask
				},
				{
					"name" : "othertask",
					"value" : othertask
				}
			]
		}
		ticketsobj.push(ticketobj)
		if (isNaN(age)) {
			document.querySelector("#js_ticket_costwrapper_"+i).classList.add("hidden");
		} else {
			document.querySelector("#js_ticket_costwrapper_"+i).classList.remove("hidden");
			document.querySelector("#js_ticket_cost_"+i).textContent = price[0] / 100;
		}
	}
	return [ticketsobj,totalprice];
}

function calcTicketPrice(age,biz=false,tourist=false) {
	pricing = JSON.parse(document.querySelector("#js_ticketholders").dataset.pricing);
	if (isNaN(age)) {
		return [0,''];
	}
	base = pricing.base;
	modifier = 0;
	ttype = '';

	if ( false == biz && false == tourist ) {
		for (var i = 0; i < pricing.types.length; i++) {
			if ('7bit' === pricing.types[i].slug) {
				start = 25
			} else {
				start = parseInt(pricing.types[i].start)
			}
			if (age >= start) {
				ttype = pricing.types[i].slug
				modifier = pricing.types[i].price_modifier
			}
		}
	} else {
		for (var i = 0; i < pricing.types.length; i++) {
			if ('9bit' === pricing.types[i].slug && biz == true) {
				ttype = pricing.types[i].slug
				modifier = pricing.types[i].price_modifier
			}
			if ('8bit' === pricing.types[i].slug && tourist == true) {
				ttype = pricing.types[i].slug
				modifier = pricing.types[i].price_modifier
			}
		}
	}
	return [base + modifier,ttype]
}

function makeConto() {
	console.log('--make conto--');
	ticketholderFields();
	showHideVouchers();
	maybeDisableButton();
	contoFields = document.querySelectorAll('[id^="fld_product_"]');
	conto = disclaimerFields();
	conto['products'] = [];
	conto['total'] = 0;
	conto['vouchers'] = collectVoucherCodes();
	conto['notes'] = document.querySelector('#remarks').value
	contoFields.forEach(item => {
		// Er is maar 1 optie per product mogelijk
		for (var i = 0; i < item.options.length; i++) {
			if ( ! item.options[i].selected ) {
				continue;
			}
			nr = parseInt(item.options[i].value);
			
			for (var i = 0; i < nr; i++) {

				p = {
					"product_handle" :  item.dataset.handle,
					"voucher" : false,
					"product_name" : item.dataset.pname,
                    "product_category" : item.dataset.pcategory,
					"price" : parseInt(item.dataset.price)
				}

				if ( 1 == parseInt(item.dataset.hasvariants) ) {
					p['variants'] = [
						{
							"name" : item.dataset.name,
							"value" : item.dataset.value
						}
					]
				}

				conto['products'].push(p);
				conto['total'] += parseInt(item.dataset.price);
			}
		}
	});

	tickets = collectTickets();

	conto['products'] = conto['products'].concat(tickets[0]);
	conto['total'] += tickets[1];
	conto['total_post_discount'] = conto['total']
	let vouchereffect = calcVoucherEffect(conto);
	let voucherstring = "(" + Object.keys(vouchereffect).join(', ') + ")";
	
	for (k in vouchereffect) {
		conto['total_post_discount'] -= vouchereffect[k];
	}
	document.querySelector('#js_total').textContent = conto['total'] / 100;
	document.querySelector('#js_stilltopay_total').textContent = conto['total_post_discount'] / 100;
	if (conto['total'] !== conto['total_post_discount']) {
		document.querySelector('#js_voucherinfo').classList.remove("hidden");
		document.querySelector('#js_stilltopay').classList.remove("hidden");
		document.querySelector('#js_voucherinfo').textContent = "Kortingen met vouchers " + voucherstring +": - € " + (conto['total'] - conto['total_post_discount']) / 100;
	} else {
		document.querySelector('#js_stilltopay').classList.add("hidden");
		document.querySelector('#js_voucherinfo').classList.add("hidden");
	}
	console.log(conto);
}

function calcVoucherEffect(c) {
	out = {};
	for (j in known_vouchers) {
		let product_value = 0;
		let candidate = false;
		for (i in c['products']) {
			
			if (known_vouchers[j]['applicable_to'] === c['products'][i]['product_category'] && c['products'][i]['voucher'] === false && c['products'][i]['price'] > product_value) {
				
				product_value = c['products'][i]['price'];
				candidate = i;
			}
		}
		if (Boolean(candidate)) {
			c['products'][candidate]['voucher'] = j
			out[j] = c['products'][candidate]['price'];
		}
	}
	return out;
}

function ticketholderFields() {
    let n_tickets = parseInt(document.querySelector('#tickets_n').value);
    let ticketholders = document.querySelector('#js_ticketholders');
    
    if (n_tickets > 0) {
        ticketholders.classList.remove("sr-only");
    } else {
        ticketholders.classList.add("sr-only");
    }
    let ticketholder_boxes = document.querySelectorAll('#js_ticketholders_inner .js_ticketholder');
    n_ticketholder_boxes = ticketholder_boxes.length;

    if (n_tickets > 0) {
        document.querySelector("#js_ticketcount").innerHTML = "("+n_tickets+")";
        while (n_tickets > n_ticketholder_boxes) {
            
            new_box = document.querySelector('#js_ticketholdertemplate').cloneNode(true);
            new_box.id = "js_ticketholder_" + n_ticketholder_boxes;

            var idToChange = new_box.querySelectorAll('[id$="_tpl"]');
            var nameToChange = new_box.querySelectorAll('[name$="_tpl"]');
            var forToChange = new_box.querySelectorAll('[for$="_tpl"]');

            const re = /(.+?)_tpl/gi;

            idToChange.forEach(item => {
                item.id = item.id.replace(re, '$1_' + n_ticketholder_boxes );
            });
            nameToChange.forEach(item => {
                item.name = item.name.replace(re, '$1_' + n_ticketholder_boxes );
            });
            forToChange.forEach(item => {
                item.setAttribute("for",item.getAttribute("for").replace(re, '$1_' + n_ticketholder_boxes ));
            });

			new_box.querySelectorAll('input').forEach(item => {
				item.addEventListener('change', event => {
					makeConto();
				})
			});
			new_box.querySelectorAll('textarea').forEach(item => {
				item.addEventListener('change', event => {
					makeConto();
				})
			});

            new_box.classList.remove("hidden");
            document.querySelector('#js_ticketholders_inner').append(new_box);
            n_ticketholder_boxes += 1;
        }
    } else {
        document.querySelector("#js_ticketcount").innerHTML = "";
    }
	while (n_tickets < n_ticketholder_boxes) {
		// console.log("ticketholder boxes: "+n_ticketholder_boxes);
		document.querySelector('#js_ticketholders_inner').lastChild.remove();
		n_ticketholder_boxes -= 1;
	}
	maybeDisableButton();
}

function ticketholderAdultFields() {
    return true;
}

function validateField(fld,val) {
	return {
		'valid' : true,
		'reason' : 'a_reason',
		'explain' : 'There was a reason'
	}
}

function submitOrder(fields) {
	return {}
}
