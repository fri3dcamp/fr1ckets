document.addEventListener('DOMContentLoaded', function(event) {
	document.querySelectorAll(".js_json_toggle").forEach( item => {
		setButtontext(item, item.dataset.id);
		item.addEventListener('click', event => {
			toggleVisible(item.dataset.id);
			setButtontext(item, item.dataset.id);
		});
	});
});

function setButtontext(el,targetid) {
	if (document.querySelector("#"+targetid).classList.contains("hidden")) {
		el.textContent = "Show";
	} else {
		el.textContent = "Hide";
	}
}

function toggleVisible(id) {
	document.querySelector("#"+id).classList.toggle("hidden");
}