# Verkoopvoorwaarden tickets.fri3d.be
Je koopt je tickets en andere items uit de ticketshop van [Fri3d vzw](https://www.fri3d.be/wat/). Daarmee ga je akkoord met deze voorwaarden.

## Verkoop en terugbetaling

Je mag je gekochte ticket(s) doorgeven, maar je mag ze niet verkopen aan meer dan de aankoopprijs.

Als je tickets doorgeeft aan iemand anders, laat dit dan weten aan tickets@fri3d.be, zodat we de gegevens kunnen aanpassen.

Tickets worden niet terugbetaald.

Je krijgt geen fysiek (papieren) ticket. Breng je digitaal ticket mee naar Fri3d Camp.

Bestelde tickets en andere items moeten binnen de 14 dagen volledig betaald worden, anders wordt de bestelling geannuleerd. De exacte betaaldeadline wordt gecommuniceerd via je bestelling (site en mail).

## Toegang

Dieren zijn niet toegelaten, met uitzondering van assistentiedieren.

Iedereen heeft een ticket nodig, ook als je een workshop of activiteit geeft.

Iedereen met een ticket moet zich houden aan de [gedragscode](https://fri3d.be/deelnemen/excellent/). Wie dat niet doet, kan uitgesloten worden van Fri3d Camp. 

Elke deelnemer jonger dan 16 moet vergezeld zijn door een volwassene die de verantwoordelijkheid voor de jonge deelnemer volledig opneemt.

Caravans en campers hebben een camperticket nodig.

Auto's mogen alleen op de door de organisatie aangeduide parkings gezet worden.

## Aansprakelijkheid

Je neemt deel aan Fri3d Camp op eigen risico. Fri3d vzw is niet aansprakelijk voor enige schade.



