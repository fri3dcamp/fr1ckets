# endpoints servicing the ticketshop
# fr1ckets, the fri3dcamp devs, 2022
from flask import g, request
from flask_restful import Api, Resource, reqparse
from fr1ckets import app, api_app, basic_auth_app
from fr1ckets.model import client, product, logic, util, conto, voucher, session, community_shift

class OrgaProductCollectionResource(Resource):
	@basic_auth_app.required
	@util.db_save
	def post(self):
		_products = product.schema.load(request.json, many=True)

		for _product in _products:
			try:
				_existing_product = product.find(_product['name'])
			except product.ProductNotFoundError:
				product.create(_product)
			else:
				_product['handle'] = _existing_product['handle']
				product.update(_product)
		return '', 200
api_app.add_resource(OrgaProductCollectionResource, '/api/orga/products')

class OrgaClientCollectionResource(Resource):
	@basic_auth_app.required
	@util.db_save
	def post(self):
		_clients = client.schema.load(request.json, many=True)

		for _client in _clients:
			try:
				_existing_client = client.find(_client['email'])
			except client.ClientNotFoundError:
				client.create(_client)
			else:
				_client['handle'] = _existing_client['handle']
				client.update(_client)
		return '', 200
api_app.add_resource(OrgaClientCollectionResource, '/api/orga/clients')

class OrgaCommunityShiftCollectionResource(Resource):
	@basic_auth_app.required
	@util.db_save
	def post(self):
		_community_shifts = community_shift.schema.load(request.json, many=True)

		for _community_shift in _community_shifts:
			try:
				_existing_community_shift = community_shift.get(_community_shift['handle'])
			except community_shift.CommunityShiftNotFoundError:
				community_shift.create(_community_shift)
			except KeyError:
				community_shift.create(_community_shift)
			else:
				_community_shift['handle'] = _existing_community_shift['handle']
				community_shift.update(_community_shift)
		return '', 200
api_app.add_resource(OrgaCommunityShiftCollectionResource, '/api/orga/community/shifts')
