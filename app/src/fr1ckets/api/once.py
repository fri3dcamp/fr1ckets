# endpoints servicing the community shifts
# fr1ckets, the fri3dcamp devs, 2024
from flask import g, request, make_response
from flask_restful import Api, Resource, reqparse
from fr1ckets import app, api_app, ApiError
from fr1ckets.model import conto, client, util
from marshmallow import Schema, fields
from fr1ckets.util.readablecode import create_code


class OnceContoReadableHandlesResource(Resource):
	@util.db_save
	def get(self):

		past_handles = []

		for entry in conto.list():
			readable = create_code()
			while readable in past_handles:
				readable = create_code()

			past_handles.append(readable)

			entry['readable_code'] = readable

			conto.update(entry)

		return '', 201


api_app.add_resource(OnceContoReadableHandlesResource, '/api/once/conto_readable_handles')


class OnceClientReadableHandlesResource(Resource):
	@util.db_save
	def get(self):

		past_handles = []

		for entry in client.list():
			readable = create_code()
			while readable in past_handles:
				readable = create_code()

			past_handles.append(readable)

			entry['readable_code'] = readable

			client.update(entry)

		return '', 201


api_app.add_resource(OnceClientReadableHandlesResource, '/api/once/client_readable_handles')
