# endpoints servicing the community shifts
# fr1ckets, the fri3dcamp devs, 2024
from flask import g, request, make_response
from flask_restful import Api, Resource, reqparse
from werkzeug.exceptions import HTTPException
from fr1ckets import app, api_app, ApiError
from fr1ckets.model import logic, order, product, session, community_shift, conto, client, util
from fr1ckets.model.util import gen_handle, db_save
from fr1ckets.util import mail, helper
from marshmallow import Schema, fields, EXCLUDE, ValidationError
from datetime import UTC
from marshmallow import Schema, fields


class CommunityShiftPersonSchema(Schema):
	readable_code = fields.Str()
	name = fields.Str()
	email = fields.Email()
	expected = fields.Bool(default=False)

class CommunityShiftSchema(Schema):
	shifts = fields.List(fields.Nested(community_shift.CommunityShiftSchema), missing=[])
	people = fields.List(fields.Nested(CommunityShiftPersonSchema, missing=[]))

class CommunityShiftCollectionResource(Resource):
	def get(self):
		return community_shift.schema.dump(community_shift.list(), many=True), 200

api_app.add_resource(CommunityShiftCollectionResource, '/api/community/shifts')


class CommunityShiftClientCollectionResource(Resource):
	def get(self, client_nonce):

		# get a list of shifts
		shifts = community_shift.list()
		# and all choices thusfar
		choices = community_shift.list_choices()

		# get this client_handle's contextualized sessions
		_client = client.find(client_nonce, 'nonce')
		session_handles = conto.get_sessions_for_client(_client['handle'])
		sessions = [logic.contextualize_session(session.get(h)) for h in session_handles]

		# and filter out tickets which are allowed to help
		people = logic.community_people(sessions, only_paid=False)
		people = [p for p in people if p['allowed'] == True]

		# now redact other tickets
		people_code_by_handle = {p['conto_handle']: p['readable_code'] for p in people}
		for shift in shifts:
			shift['signed_up'] = []
			for choice in choices[shift['handle']]:
				if choice in people_code_by_handle.keys():
					shift['signed_up'].append(people_code_by_handle[choice])
				else:
					shift['signed_up'].append('REDACTED')

		ret = {
			'people': people,
			'shifts': shifts,
		}
		return CommunityShiftSchema().dump(ret), 200

api_app.add_resource(CommunityShiftClientCollectionResource, '/api/community/shifts/client/<client_nonce>')

class CommunityShiftReadableCodeCollectionResource(Resource):
	def get(self, readable_code):

		# get a list of shifts
		shifts = community_shift.list()
		# and all choices thusfar
		choices = community_shift.list_choices()

		# get this readable_code's client's contextualized sessions
		code_is_conto_entry = False
		try:
			lookup_conto_entry = conto.find(readable_code)
			lookup_session = session.get(lookup_conto_entry['session_handle'])
			lookup_client = client.get(lookup_session['client_handle'])
			code_is_conto_entry = True
		except Exception:
			lookup_client = client.find(readable_code, key='readable_code')

		session_handles = conto.get_sessions_for_client(lookup_client['handle'])
		sessions = [logic.contextualize_session(session.get(h)) for h in session_handles]

		# and filter out tickets which are allowed to help
		people = logic.community_people(sessions, only_paid=False)
		people = [p for p in people if p['allowed'] == True]

		# if this a specific entry, cull the rest
		if code_is_conto_entry:
			people = [p for p in people if p['readable_code'] == readable_code]
			assert len(people) > 0

		# now redact other tickets
		people_code_by_handle = {p['conto_handle']: p['readable_code'] for p in people}
		for shift in shifts:
			shift['signed_up'] = []
			for choice in choices[shift['handle']]:
				if choice in people_code_by_handle.keys():
					shift['signed_up'].append(people_code_by_handle[choice])
				else:
					shift['signed_up'].append('REDACTED')

		ret = {
			'people': people,
			'shifts': shifts,
		}
		return CommunityShiftSchema().dump(ret), 200

api_app.add_resource(CommunityShiftReadableCodeCollectionResource, '/api/community/shifts/readable_code/<readable_code>')


class CommunityShiftFullError(Exception):
	pass
class CommunityShiftDuplicateError(Exception):
	pass
class CommunityShiftDoubleBookedError(Exception):
	pass
class CommunityShiftNotApplicableError(Exception):
	pass

class CommunityShiftJoinResource(Resource):
	@util.db_save
	def get(self, shift_handle, person_readable_code):

		all_choices = community_shift.list_choices()
		intended_shift = community_shift.get(shift_handle)
		person_conto_entry = conto.find(person_readable_code)
		print("person_conto_entry:")
		print(person_conto_entry)
		person_session = logic.contextualize_session(
			session.get(person_conto_entry['session_handle'])
		)
		print("session:")
		print(person_session)
		person_session_people = logic.community_people([person_session], only_paid=False)
		print("person_session_people:")
		print(person_session_people)
		person_all_shifts = [
			community_shift.get(handle) for handle in
				community_shift.shifts_for_person(person_conto_entry['handle'])
		]

		# is this person eligible to join any shift
		if person_conto_entry['handle'] not in [p['conto_handle'] for p in person_session_people]:
			raise CommunityShiftNotApplicableError

		# is this shift full
		if len(all_choices[shift_handle]) >= intended_shift['n_total']:
			raise CommunityShiftFullError

		# did this person already join this shift
		if shift_handle in [shift['handle'] for shift in person_all_shifts]:
			raise CommunityShiftDuplicateError

		# does the intended shift overlap with any currently joined shifts
		intended_shift_start = intended_shift['start']
		intended_shift_end = intended_shift_start + intended_shift['duration']
		for _shift in person_all_shifts:
			shift_start = _shift['start']
			shift_end = shift_start + _shift['duration']
			if intended_shift_end <= shift_start:
				pass
			elif intended_shift_start >= shift_end:
				pass
			else:
				raise CommunityShiftDoubleBookedError

		# cool, hit it
		community_shift.join(shift_handle, person_conto_entry['handle'])

		return '', 204

api_app.add_resource(CommunityShiftJoinResource, '/api/community/shift/<shift_handle>/join/<person_readable_code>')

class CommunityShiftLeaveResource(Resource):
	@util.db_save
	def get(self, shift_handle, person_readable_code):

		all_choices = community_shift.list_choices()
		intended_shift = community_shift.get(shift_handle)
		person_conto_entry = conto.find(person_readable_code)
		print("person_conto_entry:")
		print(person_conto_entry)
		person_session = logic.contextualize_session(
			session.get(person_conto_entry['session_handle'])
		)
		print("session:")
		print(person_session)
		person_session_people = logic.community_people([person_session], only_paid=False)
		print("person_session_people:")
		print(person_session_people)
		person_all_shifts = [
			community_shift.get(handle) for handle in
				community_shift.shifts_for_person(person_conto_entry['handle'])
		]

		# is this person eligible to leave any shift
		if person_conto_entry['handle'] not in [p['conto_handle'] for p in person_session_people]:
			raise CommunityShiftNotApplicableError

		# did this person ever join this shift
		print(person_all_shifts)
		if shift_handle not in [shift['handle'] for shift in person_all_shifts]:
			raise CommunityShiftNotApplicableError

		# cool, hit it
		community_shift.leave(shift_handle, person_conto_entry['handle'])

		return '', 204

api_app.add_resource(CommunityShiftLeaveResource, '/api/community/shift/<shift_handle>/leave/<person_readable_code>')
