# endpoints servicing the ticketshop
# fr1ckets, the fri3dcamp devs, 2022
from flask import g, request, make_response
from flask_restful import Api, Resource, reqparse
from werkzeug.exceptions import HTTPException
from fr1ckets import app, api_app, ApiError
from fr1ckets.model import logic, order, product, session
from fr1ckets.model.util import gen_handle, db_save
from fr1ckets.util import mail, helper
from marshmallow import Schema, fields, EXCLUDE, ValidationError
from datetime import UTC
from functools import wraps, lru_cache
import time
import qrcode
import qrcode.image.svg

def clock(wrapped):
	@wraps(wrapped)
	def wrapper(*args, **kwargs):
		start = time.time()
		ret = wrapped(*args, **kwargs)
		return ret
	return wrapper

class GetClientSchema(Schema):
	email = fields.Str()
	can_order = fields.Bool()
	can_order_from = fields.AwareDateTime(
		format='iso', required=True, default_timezone=UTC)

class GetSchema(Schema):
	client = fields.Nested(GetClientSchema)
	products = fields.List(fields.Nested(product.ProductSchema))

get_args = reqparse.RequestParser()
get_args.add_argument('email', type=str, required=True)
class GetResource(Resource):
	def get(self):
		return '👋', 200

	def post(self):
		args = get_args.parse_args()
		return GetSchema().dump(logic.hello(args['email'])), 200

api_app.add_resource(GetResource, '/api/shop/get')

class OrderProductVariantSchema(Schema):
	name = fields.Str(required=True)
	value = fields.Str()
class OrderProductFieldSchema(Schema):
	name = fields.Str(required=True)
	value = fields.Raw(missing='')

class OrderProductSchema(Schema):
	product_handle = fields.Str(required=True)
	variants = fields.List(
		fields.Nested(OrderProductVariantSchema, unknown=EXCLUDE),
		missing=[])
	fields = fields.List(
		fields.Nested(OrderProductFieldSchema, unknown=EXCLUDE),
		missing=[])

class OrderSchema(Schema):
	email = fields.Email(required=True)
	notes = fields.Str(missing='')
	products = fields.List(
		fields.Nested(OrderProductSchema, unknown=EXCLUDE),
		missing=[])
	vouchers = fields.List(fields.Str(), missing=[])
	disclaimer_animal = fields.Bool(missing=False)
	disclaimer_have_accompanying_adult = fields.Bool(missing=False)
	disclaimer_be_excellent = fields.Bool(missing=False)

order_schema = OrderSchema()

class OrderResource(Resource):
	@clock
	@db_save
	def post(self):
		try:
			_order = order_schema.load(request.json, unknown=EXCLUDE)
		except ValidationError as e:
			raise ApiError(message='EBADVALUES', extra=e.normalized_messages())
		_session = logic.order(_order)
		result = logic.contextualize_session(_session)

		ordered_things = [p['product_og_name'] for p in _session['products']]
		app.logger.warning(f"someone ordered for {_session['total']}: {', '.join(ordered_things)}")

		# only on order, not on quote, send the email confirming this order
		mail.session_created(result)

		return result, 200

api_app.add_resource(OrderResource, '/api/shop/order')

"""
a clone of OrderResource which does not save to DB
"""
class QuoteResource(Resource):
	"""
	very much not @db_save here
	"""
	@clock
	def post(self):
		try:
			_order = order_schema.load(request.json, unknown=EXCLUDE)
		except ValidationError as e:
			raise ApiError(message='EBADVALUES', extra=e.normalized_messages())
		_session = logic.order(_order)
		result = logic.contextualize_session(_session)
		del result['handle']		# this would not make sense
		del result['payment_code']	# this would not make sense
		del result['payment_code_human']	# this would not make sense
		del result['nonce']			# this would not make sense
		return result, 200

api_app.add_resource(QuoteResource, '/api/shop/quote')


class VoucherQuerySchema(Schema):
	code = fields.Str()


class VoucherSchema(Schema):
	found = fields.Bool()
	applicable_to = fields.Str()


voucher_query_schema = VoucherQuerySchema()
voucher_schema = VoucherSchema()

class VoucherCheckResource(Resource):
	def post(self):
		q = voucher_query_schema.load(request.json)
		return logic.check_voucher(q['code']), 200

api_app.add_resource(VoucherCheckResource, '/api/shop/voucher')


def generate_svg_qr(amount, code):
	template = """BCD
001
1
SCT
ARSPBE22
Fri3d vzw
BE17973374409021
EUR{amount}

{code}
"""

	contents = template.format(
		amount=f"{amount:.02f}",
		code=code
	)

	img = qrcode.make(contents,
		image_factory=qrcode.image.svg.SvgPathFillImage)

	return img.to_string(encoding='unicode')


class QRTestResource(Resource):
	@clock
	def get(self):
		svg = generate_svg_qr(0.20, helper.humanize_payment_code('490331389778'))
		resp = make_response(svg, 200)
		resp.headers.extend({
			'Content-Type': 'image/svg+xml',
		})
		return resp

api_app.add_resource(QRTestResource, '/api/shop/session/qrtest')

class QRResource(Resource):
	@clock
	def get(self, session_nonce):
		try:
			_session = session.find(session_nonce)
			_session = logic.contextualize_session(_session)
		except:
			raise ApiError(message='ENOTFOUND')
		svg = generate_svg_qr(
			_session['due'] / 100,
			_session['payment_code_human']
		)
		resp = make_response(svg, 200)
		resp.headers.extend({
			'Content-Type': 'image/svg+xml',
		})
		return resp

api_app.add_resource(QRResource, '/api/shop/session/qr/<session_nonce>')
