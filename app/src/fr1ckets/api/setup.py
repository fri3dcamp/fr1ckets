# general API dot connecting
# fr1ckets, the fri3dcamp devs, 2022
from fr1ckets import app, api_app
import json
import datetime

class DatetimeAwareJSONEncoder(json.JSONEncoder):
	"""
	normal JSON will poop when encountering a datetime (f.e.
	as an API call retval), so stringify it here
	"""
	def default(self, obj):
		if isinstance(obj, datetime.datetime):
			return obj.isoformat()
		return json.JSONEncoder.default(self, obj)

app.config['RESTFUL_JSON'] = {
	'cls' : DatetimeAwareJSONEncoder,
}

