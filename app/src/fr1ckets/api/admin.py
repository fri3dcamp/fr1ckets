# endpoints servicing the ticketshop
# fr1ckets, the fri3dcamp devs, 2022
from flask import g, request
from flask_restful import Api, Resource, reqparse
from fr1ckets import app, api_app, basic_auth_app
from fr1ckets.model import client, product, logic, util, conto, voucher, session


class ClientCollectionResource(Resource):
	def get(self):
		return client.list(), 200

	@util.db_save
	def post(self):
		_client = client.schema.load(request.json)
		client.create(_client)
		return client.schema.dump(client.get(_client['handle'])), 201

api_app.add_resource(ClientCollectionResource, '/api/admin/clients')

class ClientResource(Resource):
	def get(self, handle):
		return client.schema.dump(client.get(handle)), 200

	@util.db_save
	def delete(self, handle):
		client.delete(handle)
		return '', 204

api_app.add_resource(ClientResource, '/api/admin/client/<handle>')

class ClientFindResource(Resource):
	def get(self, email):
		return client.schema.dump(client.find(email)), 200

api_app.add_resource(ClientFindResource, '/api/admin/client/find/<email>')


class ClientContoResource(Resource):
	def get(self, handle):
		output = []
		for session_handle in conto.get_sessions_for_client(handle):
			output.append({
				'session': session.schema.dump(session.get(session_handle)),
				'entries': conto.schema.dump(
					conto.get_entries_for_session(session_handle),
					many=True
				),
			})
		return output, 200

api_app.add_resource(ClientContoResource, '/api/admin/client/<handle>/conto')

class ProductCollectionResource(Resource):
	def get(self):
		return product.list(), 200
	@util.db_save
	def post(self):
		_product = product.schema.load(request.json)
		product.create(_product)
		return _product, 201

api_app.add_resource(ProductCollectionResource, '/api/admin/products')

class ProductResource(Resource):
	def get(self, handle):
		return product.get(handle), 200

	@util.db_save
	def delete(self, handle):
		product.delete(handle)
		return '', 204

api_app.add_resource(ProductResource, '/api/admin/product/<handle>')

class VoucherCollectionResource(Resource):
	def get(self):
		return voucher.list(), 200

	@util.db_save
	def post(self):
		return logic.create_voucher(voucher.schema.load(request.json)), 200

api_app.add_resource(VoucherCollectionResource, '/api/admin/vouchers')


class VoucherResource(Resource):
	def get(self, handle):
		return voucher.get(handle), 200

	@util.db_save
	def delete(self, handle):
		voucher.delete(handle)
		return '', 204

api_app.add_resource(VoucherResource, '/api/admin/voucher/<handle>')
