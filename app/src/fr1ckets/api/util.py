# endpoints servicing the ticketshop, utilities
# fr1ckets, the fri3dcamp devs, 2022
import json
from flask import g, request
from flask_restful import Api, Resource, reqparse
from fr1ckets import app, api_app
from fr1ckets.model import client

class HealthResource(Resource):
	def get(self):
		# see if we can talk to the database, consider ourselves
		# as OK if we can
		_clients = client.list()
		assert _clients
		return 'healthy', 200

api_app.add_resource(HealthResource, '/api/health')
