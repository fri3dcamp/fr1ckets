# background tasks, invoked with Celery via the queue
# fr1ckets, the fri3dcamp devs, 2024
from fr1ckets import app, celery_app, mail_app
from flask_mail import Mail, Message

@celery_app.task
def mail(addr_from, addr_to, subject, content_text, content_html):
	app.logger.debug(f'mailing from {addr_from} to {addr_to}')
	app.logger.warning(content_html)

	msg = Message(
		subject,
		sender=addr_from,
		recipients=[
			addr_to
		]
	)

	msg.body = content_text
	msg.html = content_html

	mail_app.send(msg)
