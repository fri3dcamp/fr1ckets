# order model, only validation
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields
from fr1ckets.model.util import gen_handle, gen_nonce, DBMixinJson
import sqlite3

class OrderVariantSchema(Schema):
	name = fields.Str()
	value = fields.Str()

class OrderFieldSchema(Schema):
	name = fields.Str()
	value = fields.Str()

class OrderProductSchema(Schema):
	product_handle = fields.Str()
	variants = fields.List(fields.Nested(OrderVariantSchema), missing=[])
	fields = fields.List(fields.Nested(OrderFieldSchema), missing=[])

class OrderSchema(Schema):
	email = fields.Email()
	session = fields.Str(load_default=gen_handle)
	products = fields.List(fields.Nested(OrderProductSchema), missing=[])

schema = OrderSchema()
