# conto model
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields, ValidationError
from fr1ckets.model.util import gen_handle, gen_friendly, gen_nonce, DBMixinJson
from fr1ckets.util.readablecode import create_code


class ContoEntryVariantSchema(Schema):
	name = fields.Str()
	value = fields.Str()


class ContoEntryFieldSchema(Schema):
	name = fields.Str()
	value = fields.Raw(missing='')


class ContoEntrySchema(Schema):
	handle = fields.Str(load_default=gen_handle)
	removed = fields.Bool(missing=False)
	readable_code = fields.Str(missing=create_code)
	session_handle = fields.Str(allow_none=True)	# on delete set null
	product_handle = fields.Str(allow_none=True)	# on delete set null
	client_handle = fields.Str(allow_none=True)		# on delete set null
	payment_handle = fields.Str(allow_none=True)	# on delete set null
	variants = fields.List(fields.Nested(ContoEntryVariantSchema), missing=[])
	fields = fields.List(fields.Nested(ContoEntryFieldSchema), missing=[])


class DBContoEntrySchema(ContoEntrySchema, DBMixinJson):
	do_not_push = [ 'handle', 'session_handle', 'client_handle', 'product_handle', 'payment_handle' ]


schema = ContoEntrySchema()
db_schema = DBContoEntrySchema()


class ContoEntryNotFoundError(Exception):
	pass


def create(data):

	for _ in range(1024):
		try:
			find(data['readable_code'], key='readable_code')
		except ContoEntryNotFoundError:
			break
		data['readable_code'] = create_code()
	else:
		raise Exception

	q = """
		INSERT INTO conto (
			handle,
			session_handle,
			client_handle,
			product_handle,
			payment_handle,
			data
			)
		VALUES (
			:handle,
			:session_handle,
			:client_handle,
			:product_handle,
			:payment_handle,
			:data
		);
		"""

	g.db_cur.execute(q, db_schema.dump(data))

def list():
	q = """
		SELECT
			handle,
			session_handle,
			client_handle,
			product_handle,
			payment_handle,
			data
		FROM
			conto;
		"""

	g.db_cur.execute(q)

	try:
		return db_schema.load(g.db_cur.fetchall(), many=True)
	except ValidationError:
		raise ContoEntryNotFoundError


def find(value, key='readable_code'):

	q = f"""
		SELECT
			handle,
			session_handle,
			client_handle,
			product_handle,
			payment_handle,
			data
		FROM
			conto
		WHERE json_extract(data, '$.{key}') == ?;
		"""

	g.db_cur.execute(q, (value,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise ContoEntryNotFoundError
	except Exception as e:
		raise e


def get(handle):
	q = """
		SELECT
			handle,
			session_handle,
			client_handle,
			product_handle,
			payment_handle,
			data
		FROM
			conto
		WHERE
			handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	try:
		return db_schema.load(g.db_cur.fetchone())
	except ValidationError:
		raise ContoEntryNotFoundError


def update(data):
	q = """
		UPDATE
			conto
		SET
			session_handle=:session_handle,
			client_handle=:client_handle,
			product_handle=:product_handle,
			payment_handle=:payment_handle,
			data=:data
		WHERE
			handle=:handle;
		"""

	g.db_cur.execute(q, db_schema.dump(data))

	if g.db_cur.rowcount != 1:
		raise ContoEntryNotFoundError


def get_sessions_for_client(client_handle):
	q = """
		SELECT DISTINCT
			session_handle
		FROM
			conto
		WHERE
			client_handle = ?;
		"""
	
	g.db_cur.execute(q, (client_handle,))

	return [ row['session_handle'] for row in g.db_cur.fetchall() ]


def get_all_clients_sessions():
	q = """
		SELECT DISTINCT
			cl.handle as client_handle,
			s.handle as session_handle
		FROM
			client cl
			INNER JOIN conto co ON cl.handle = co.client_handle
			INNER JOIN session s ON co.session_handle = s.handle
		ORDER BY co.handle DESC;
		"""
	
	g.db_cur.execute(q)

	return g.db_cur.fetchall()

def get_entries_for_session(session_handle):
	
	q = """
		SELECT
			handle,
			session_handle,
			client_handle,
			product_handle,
			payment_handle,
			data
		FROM
			conto
		WHERE
			session_handle = ?;
		"""

	g.db_cur.execute(q, (session_handle,))

	return db_schema.load(g.db_cur.fetchall(), many=True)

def get_paid_products():
	
	q = """
		SELECT
			handle,
			session_handle,
			client_handle,
			product_handle,
			payment_handle,
			data
		FROM
			conto
		WHERE
			payment_handle is NULL and
			session_handle in 
			(
				SELECT 
					session_handle 
				FROM 
					conto
				WHERE
					payment_handle is not null
			);
		"""

	g.db_cur.execute(q)

	return db_schema.load(g.db_cur.fetchall(), many=True)
