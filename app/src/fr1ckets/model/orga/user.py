# orga user database model
# fr1ckets, the fri3dcamp devs, 2024
from flask import g
from marshmallow import Schema, fields, ValidationError, post_load
from datetime import UTC
from fr1ckets.model.util import gen_handle, gen_nonce, gen_payment_code, DBMixinJson
from bcrypt import checkpw, hashpw, gensalt


class OrgaUser:
	"""
	an organizational user

	flask-login compatible
	"""
	# if this user exists, it is authed
	is_anonymous = False
	is_active = True
	is_authenticated = True

	def __init__(self, handle, email, auth, roles):
		self.handle = handle or gen_handle()
		self.email = email
		self.auth = auth
		self.roles = roles

	def check_auth(self, password):
		return checkpw(password.encode('utf-8'), self.auth.encode('utf-8'))

	def reset_auth(self, password=None):
		"""
		reset this user's password, generating a new one if needs be
		"""
		new_password = password or gen_nonce()
		self.auth = hashpw(new_password.encode('utf-8'), gensalt())
		return new_password

	def get_id(self):
		return self.handle


class OrgaUserSchema(Schema):
	handle = fields.Str(load_default=gen_handle)
	email = fields.Email()
	auth = fields.Str()
	roles = fields.List(fields.Str())

	@post_load
	def make_obj(self, data, **kwargs):
		"""
		orga users are special in that we always load up an OrgaUser object,
		there is no need for an inverse to this as marshmallow can handle
		objects with correctly named members during dumping
		"""
		return OrgaUser(**data)


class DBOrgaUserSchema(OrgaUserSchema, DBMixinJson):
	pass


schema = OrgaUserSchema()
db_schema = DBOrgaUserSchema()


class OrgaUserNotFoundError(Exception):
	pass


class OrgaUserExistsError(Exception):
	pass


def get(handle):

	q = """
		SELECT
			handle,
			data
		FROM
			orga_user
		WHERE
			handle = ?;
		"""

	g.db_cur.execute(q, (handle,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise OrgaUserNotFoundError


def find(email):

	q = """
		SELECT
			handle,
			data
		FROM
			orga_user
		WHERE json_extract(data, '$.email') == ?;
		"""

	g.db_cur.execute(q, (email,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise OrgaUserNotFoundError
	except Exception as e:
		raise e


def list():

	q = """
		SELECT
			handle,
			data
		FROM
			orga_user;
		"""

	g.db_cur.execute(q)
	rows = g.db_cur.fetchall()

	try:
		return db_schema.load(rows, many=True)
	except ValidationError:
		raise OrgaUserNotFoundError


def create(data):

	q = """
		INSERT INTO orga_user (
			handle,
			data
			)
		VALUES (
			:handle,
			:data
		);
		"""

	g.db_cur.execute(q, db_schema.dump(data))


def delete(handle):

	q = """
		DELETE FROM orga_user
		WHERE handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	if g.db_cur.rowcount != 1:
		raise OrgaUserNotFoundError


def update(data):
	q = """
		UPDATE
			orga_user
		SET
			data=:data
		WHERE
			handle=:handle;
		"""

	g.db_cur.execute(q, db_schema.dump(data))
	if g.db_cur.rowcount != 1:
		raise OrgaUserNotFoundError
