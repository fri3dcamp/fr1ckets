# a schema for a note, used here and there
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields
from datetime import UTC, datetime
from fr1ckets.model.util import gen_handle, gen_nonce, DBMixinJson, now

class NoteSchema(Schema):
	author = fields.Str()
	location = fields.Str(default='unknown')
	contents = fields.Str()
	created_at = fields.AwareDateTime(format='iso', default_timezone=UTC, missing=now)
