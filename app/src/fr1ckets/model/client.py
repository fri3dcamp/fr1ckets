# client database model
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields, ValidationError
from datetime import UTC
from fr1ckets.model.util import gen_handle, gen_nonce, gen_payment_code, DBMixinJson
from fr1ckets.util.readablecode import create_code


class ClientSchema(Schema):
	handle = fields.Str(load_default=gen_handle)
	email = fields.Email()
	readable_code = fields.Str(missing=create_code)
	nonce = fields.Str(load_default=gen_nonce)
	payment_code = fields.Str(load_default=gen_payment_code)
	can_order_from = fields.AwareDateTime(
		format='iso', required=True, default_timezone=UTC)
	notes = fields.Str(missing='')
	reminder_expires_at = fields.AwareDateTime(
		format='iso', default_timezone=UTC, missing=None)


class DBClientSchema(ClientSchema, DBMixinJson):
	pass


schema = ClientSchema()
db_schema = DBClientSchema()


class ClientNotFoundError(Exception):
	pass


class ClientExistsError(Exception):
	pass


def get(handle):

	q = """
		SELECT
			handle,
			data
		FROM
			client
		WHERE
			handle = ?;
		"""

	g.db_cur.execute(q, (handle,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise ClientNotFoundError


def find(value, key='email'):

	q = f"""
		SELECT
			handle,
			data
		FROM
			client
		WHERE json_extract(data, '$.{key}') == ?;
		"""

	g.db_cur.execute(q, (value,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise ClientNotFoundError
	except Exception as e:
		raise e

def find_by_nonce(nonce):

	q = """
		SELECT
			handle,
			data
		FROM
			client
		WHERE json_extract(data, '$.nonce') == ?;
		"""

	g.db_cur.execute(q, (nonce,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise ClientNotFoundError
	except Exception as e:
		raise e


def find_by_payment_code(payment_code):

	q = """
		SELECT
			handle,
			data
		FROM
			client
		WHERE
			json_extract(data, '$.payment_code') == ?;
		"""

	g.db_cur.execute(q, (payment_code,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise ClientNotFoundError


def list():

	q = """
		SELECT
			handle,
			data
		FROM
			client;
		"""

	g.db_cur.execute(q)
	rows = g.db_cur.fetchall()

	try:
		return db_schema.load(rows, many=True)
	except ValidationError:
		raise ClientNotFoundError


def create(data):

	for _ in range(1024):
		try:
			find(data['readable_code'], key='readable_code')
		except ClientNotFoundError:
			break
		data['readable_code'] = create_code()
	else:
		raise Exception

	q = """
		INSERT INTO client (
			handle,
			data
			)
		VALUES (
			:handle,
			:data
		);
		"""

	g.db_cur.execute(q, db_schema.dump(data))


def delete(handle):

	q = """
		DELETE FROM client
		WHERE handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	if g.db_cur.rowcount != 1:
		raise ClientNotFoundError


def update(data):
	q = """
		UPDATE
			client
		SET
			data=:data
		WHERE
			handle=:handle;
		"""

	g.db_cur.execute(q, db_schema.dump(data))
	if g.db_cur.rowcount != 1:
		raise ClientNotFoundError
