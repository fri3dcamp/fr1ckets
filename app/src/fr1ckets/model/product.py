# product database model
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields, ValidationError
from fr1ckets.model.util import gen_handle, gen_nonce, DBMixinJson

class ProductVariantOptionSchema(Schema):
	name = fields.Str()
	slug = fields.Str()
	price_modifier = fields.Dict(
		keys=fields.Str(),
		values=fields.Integer()
	)

class ProductVariantSchema(Schema):
	name = fields.Str()
	description = fields.Str()
	options = fields.List(fields.Nested(ProductVariantOptionSchema), missing=[])

class ProductFieldSchema(Schema):
	name = fields.Str()
	description = fields.Str()
	required = fields.Boolean()

class ProductSchema(Schema):
	handle = fields.Str(load_default=gen_handle)
	name = fields.Str()
	price = fields.Dict(
		keys=fields.Str(),
		values=fields.Integer()
	)
	description = fields.Str()
	info = fields.Dict()
	category = fields.Str(missing="uncategorized")
	availability = fields.Str(missing='available')
	variants = fields.List(fields.Nested(ProductVariantSchema), missing=[])
	fields = fields.List(fields.Nested(ProductFieldSchema), missing=[])

class DBProductSchema(ProductSchema, DBMixinJson):
	pass

schema = ProductSchema()
db_schema = DBProductSchema()

class ProductNotFoundError(Exception):
	pass
class ProductExistsError(Exception):
	pass

def get(handle):

	q = """
		SELECT
			handle,
			data
		FROM
			product
		WHERE handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except:
		raise ProductNotFoundError

def find(name):

	q = """
		SELECT
			handle,
			data
		FROM
			product
		WHERE
			json_extract(data, '$.name') == ?;
		"""

	g.db_cur.execute(q, (name,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise ProductNotFoundError
	except Exception as e:
		raise e

def delete(handle):

	q = """
		DELETE FROM
			product
		WHERE
			handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	if g.db_cur.rowcount != 1:
		raise ProductNotFoundError

def list():

	q = """
		SELECT
			handle,
			data
		FROM
			product;
		"""

	g.db_cur.execute(q)
	rows = g.db_cur.fetchall()

	try:
		return db_schema.load(rows, many=True)
	except:
		raise ProductNotFoundError


def create(data):

	q = """
		INSERT INTO product (
			handle,
			data
			)
		VALUES (
			:handle,
			:data
		);
		"""

	g.db_cur.execute(q, db_schema.dump(data))


def available_categories():

	q = """
		SELECT
			DISTINCT json_extract(data, '$.category') as category
		FROM
			product;
		"""

	g.db_cur.execute(q)

	return [row['category'] for row in g.db_cur.fetchall()]


def update(data):

	q = """
		UPDATE
			product
		SET
			data=:data
		WHERE
			handle=:handle;
		"""

	g.db_cur.execute(q, db_schema.dump(data))
	if g.db_cur.rowcount != 1:
		raise ProductNotFoundError
