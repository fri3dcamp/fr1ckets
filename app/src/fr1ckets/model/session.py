# session database model
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields, ValidationError, pre_load
from datetime import UTC
from fr1ckets.model.util import gen_handle, gen_nonce, gen_payment_code, DBMixinJson
from fr1ckets.model.note import NoteSchema


class SessionSchema(Schema):
	handle = fields.Str(load_default=gen_handle)
	client_handle = fields.Str()
	nonce = fields.Str(load_default=gen_nonce)
	notes = fields.Str(missing='')
	payment_code = fields.Str(load_default=gen_payment_code)
	vouchers = fields.List(fields.Str(), missing=[])
	created_at = fields.AwareDateTime(format='iso', default_timezone=UTC)
	expires_at = fields.AwareDateTime(format='iso', default_timezone=UTC)
	notes = fields.Str(missing="")
	removed = fields.Bool(missing=False)
	orga_notes = fields.List(fields.Nested(NoteSchema), missing=[])


class DBSessionSchema(SessionSchema, DBMixinJson):
	pass


schema = SessionSchema()
db_schema = DBSessionSchema()


class SessionNotFoundError(Exception):
	pass


class SessionExistsError(Exception):
	pass


def find(nonce):

	q = """
		SELECT
			handle,
			data
		FROM
			session
		WHERE
			json_extract(data, '$.nonce') == ?;
		"""

	g.db_cur.execute(q, (nonce,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise SessionNotFoundError
	except Exception as e:
		raise e


def find_by_payment_code(payment_code):

	q = """
		SELECT
			handle,
			data
		FROM
			session
		WHERE
			json_extract(data, '$.payment_code') == ?;
		"""

	g.db_cur.execute(q, (payment_code,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise SessionNotFoundError


def get(handle):

	q = """
		SELECT
			handle,
			data
		FROM
			session
		WHERE
			handle = ?;
		"""

	g.db_cur.execute(q, (handle,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise SessionNotFoundError


def list():

	q = """
		SELECT
			handle,
			data
		FROM
			session;
		"""

	g.db_cur.execute(q)
	rows = g.db_cur.fetchall()

	try:
		return db_schema.load(rows, many=True)
	except ValidationError:
		raise SessionNotFoundError


def create(data):

	q = """
		INSERT INTO session (
			handle,
			data
			)
		VALUES (
			:handle,
			:data
		);
		"""

	g.db_cur.execute(q, db_schema.dump(data))


def delete(handle):

	q = """
		DELETE FROM session
		WHERE handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	if g.db_cur.rowcount != 1:
		raise SessionNotFoundError


def update(data):
	q = """
		UPDATE
			session
		SET
			data=:data
		WHERE
			handle=:handle;
		"""
	
	g.db_cur.execute(q, db_schema.dump(data))
	if g.db_cur.rowcount != 1:
		raise SessionNotFoundError
