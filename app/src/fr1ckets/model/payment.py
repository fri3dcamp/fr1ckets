# payment database model
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields, ValidationError
from datetime import UTC
from fr1ckets.model.util import gen_handle, gen_nonce, now, now_ascii, DBMixinJson


class PaymentSchema(Schema):
	handle = fields.Str(load_default=gen_handle)
	notes = fields.Str(missing='')
	payment_code = fields.Str(missing='')
	amount = fields.Integer(missing=0)
	# reference is a unique string given by the bank for each transaction
	reference = fields.Str(missing=gen_nonce)
	created_at = fields.AwareDateTime(format='iso', default_timezone=UTC, missing=now)


class DBPaymentSchema(PaymentSchema, DBMixinJson):
	pass


schema = PaymentSchema()
db_schema = DBPaymentSchema()


class PaymentNotFoundError(Exception):
	pass


class PaymentExistsError(Exception):
	pass


def exists(_payment):

	q = """
		SELECT
			handle
		FROM
			payment
		WHERE
			json_extract(data, '$.reference') == :reference AND
			json_extract(data, '$.amount') == :amount;
		"""

	g.db_cur.execute(q, _payment)

	return g.db_cur.fetchone() is not None


def get(handle):

	q = """
		SELECT
			handle,
			data
		FROM
			payment
		WHERE
			handle = ?;
		"""

	g.db_cur.execute(q, (handle,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise PaymentNotFoundError


def list():

	q = """
		SELECT
			handle,
			data
		FROM
			payment;
		"""

	g.db_cur.execute(q)
	rows = g.db_cur.fetchall()

	try:
		return db_schema.load(rows, many=True)
	except ValidationError:
		raise PaymentNotFoundError


def create(data):

	q = """
		INSERT INTO payment (
			handle,
			data
			)
		VALUES (
			:handle,
			:data
		);
		"""

	g.db_cur.execute(q, db_schema.dump(data))


def delete(handle):

	q = """
		DELETE FROM payment
		WHERE handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	if g.db_cur.rowcount != 1:
		raise PaymentNotFoundError
