# community_shift database model
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields, ValidationError, pre_load
from datetime import UTC
from fr1ckets.model.util import gen_handle, gen_nonce, gen_payment_code, DBMixinJson
from fr1ckets.model.note import NoteSchema


class CommunityShiftSchema(Schema):
	handle = fields.Str(load_default=gen_handle)
	post = fields.Str(required=True)
	name = fields.Str(required=True)
	visible = fields.Bool(missing=True)
	important = fields.Bool(missing=True)
	start = fields.AwareDateTime(format='iso')
	duration = fields.TimeDelta(format='iso', required=True)
	signed_up = fields.List(fields.Str, missing=[])
	n_total = fields.Integer(required=True)
	n_signed_up = fields.Function(lambda o: len(o['signed_up']))
	n_open = fields.Function(lambda o: o['n_total'] - len(o['signed_up']))
	notes = fields.Str(missing="")


class DBCommunityShiftSchema(CommunityShiftSchema, DBMixinJson):
	# add the computed fields
	do_not_push = ['handle', 'signed_up', 'n_open', 'n_signed_up']


schema = CommunityShiftSchema()
db_schema = DBCommunityShiftSchema()


class CommunityShiftNotFoundError(Exception):
	pass


class CommunityShiftExistsError(Exception):
	pass


def get(handle):

	q = """
		SELECT
			handle,
			data
		FROM
			community_shift
		WHERE
			handle = ?;
		"""

	g.db_cur.execute(q, (handle,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise CommunityShiftNotFoundError


def list():

	q = """
		SELECT
			handle,
			data
		FROM
			community_shift;
		"""

	g.db_cur.execute(q)
	rows = g.db_cur.fetchall()

	try:
		return db_schema.load(rows, many=True)
	except ValidationError:
		raise CommunityShiftNotFoundError


def create(data):

	q = """
		INSERT INTO community_shift (
			handle,
			data
			)
		VALUES (
			:handle,
			:data
		);
		"""

	g.db_cur.execute(q, db_schema.dump(data))


def delete(handle):

	q = """
		DELETE FROM community_shift
		WHERE handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	if g.db_cur.rowcount != 1:
		raise CommunityShiftNotFoundError


def update(data):
	q = """
		UPDATE
			community_shift
		SET
			data=:data
		WHERE
			handle=:handle;
		"""

	g.db_cur.execute(q, db_schema.dump(data))
	if g.db_cur.rowcount != 1:
		raise CommunityShiftNotFoundError


def list_choices():
	q = """
		SELECT
			ccs.handle as handle,
			ccs.conto_handle as conto_handle,
			cs.handle as community_shift_handle
		FROM
			community_shift cs
			LEFT OUTER JOIN conto_community_shift ccs ON cs.handle = ccs.community_shift_handle;
		"""
	g.db_cur.execute(q)
	rows = g.db_cur.fetchall()

	out = {}
	for row in rows:
		if row['community_shift_handle'] not in out:
			out[row['community_shift_handle']] = []
		if row['conto_handle']:
			out[row['community_shift_handle']].append(row['conto_handle'])

	return out


def shifts_for_person(conto_entry_handle):
	q = """
		SELECT
			community_shift_handle
		FROM
			conto_community_shift
		WHERE
			conto_handle = ?;
		"""

	g.db_cur.execute(q, (conto_entry_handle,))
	rows = g.db_cur.fetchall()

	return [row['community_shift_handle'] for row in rows]

def join(shift_handle, conto_handle):
	q = """
		INSERT INTO
			conto_community_shift
			(conto_handle, community_shift_handle)
		VALUES
			(:conto_handle, :shift_handle);
		"""

	g.db_cur.execute(q, {
		'conto_handle': conto_handle,
		'shift_handle': shift_handle,
	})


def leave(shift_handle, conto_handle):
	q = """
		DELETE FROM
			conto_community_shift
		WHERE
			conto_handle = :conto_handle AND
			community_shift_handle = :shift_handle;
		"""

	g.db_cur.execute(q, {
		'conto_handle': conto_handle,
		'shift_handle': shift_handle,
	})
