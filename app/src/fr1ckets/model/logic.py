# general logic layer
# fr1ckets, the fri3dcamp devs, 2022
from fr1ckets import app, ApiError
from fr1ckets.model import client, product, conto, session, voucher, payment, community_shift
from fr1ckets.model.util import gen_handle, now
from fr1ckets.background import tasks
from fr1ckets.util import helper, mail
from datetime import datetime, timedelta, UTC
from functools import reduce


def normalize_email(_in):
	# user name is case sensitive, hostname is not
	try:
		user, host = _in.split('@')
		return f'{user}@{host.lower()}'
	except:
		return _in
		raise ValueError


"""
look up whether the given email can currently order

gets the client (or if not existing, the fallback), and return a tuple:
	* datetime; the datetime from which this email can place an order
	* boolean; if that datetime has already passed
	* boolean; if the client was actually found or the fallback was used
	* Client; the found client (actual or fallback)

excepts the email to be normalized
"""
def email_can_order(email, fallback_email='fallback@something.invalid'):

	found = False

	try:
		_client = client.find(email)
		found = True
	except client.ClientNotFoundError:
		# look at fallback instead
		_client = client.find(fallback_email)

	return (
		_client['can_order_from'],
		_client['can_order_from'] <= datetime.now(UTC),
		found,
		_client,
	)

"""
an email address is saying hello, probably to place an order

we look up the email in the clients, using the fallback if the
supplied email is not found, basing our answer on whatever we
end up with

if the client is cleared to order, we return the product list
too, otherwise it'll remain empty

we assume a fallback exists
"""
def hello(email):

	email = normalize_email(email)

	can_order_from, can_order, found, _client = email_can_order(email)

	return {
		'client': {
			'email': email,
			'can_order': can_order,
			'can_order_from': can_order_from,
		},
		'products': product.list() if can_order else [],
	}

class OrderNotPossibleError(Exception):
	pass

"""
Return the label for a variant option, based on the product handle and the variant slug.
Supports only a single variant (first variant is used).
"""
def get_variant_name(handle,variant_slug):
	p = product.get(handle)
	if p['variants']:
		for o in p['variants'][0]['options']:
			if variant_slug == o['slug']:
				return o['name']
	return ''

"""
process an order, taking in a list of referenced products along with
their variant choices and field input, doing integrity checks, and turning
that into a session
"""
def order(_order):

	# clean-up, vouchers might be in "xxx-xxx-..." notation
	_order['vouchers'] = [
		''.join(voucher.split('-'))
		for voucher in _order['vouchers']
	]

	# check that all the needed disclaimers are checked
	for check in ['animal', 'have_accompanying_adult', 'be_excellent']:
		if not _order[f'disclaimer_{check}']:
			app.logger.warning('client did not check all disclaimers')
			raise ApiError(message='EDISCLAIMERS')

	can_order_from, can_order, found, _client = email_can_order(_order['email'])

	if not can_order:
		# too early for you
		raise ApiError(message='ETOOSOON')

	if not found:
		# needs a new client
		_client = client.schema.load({
			'email': _order['email'],
			'can_order_from': str(can_order_from),
		})
		client.create(_client)
		_client = client.get(_client['handle'])

	_session = session.schema.load({
		'notes': _order['notes'],
		'client_handle': _client['handle'],
		'vouchers': [],
	})

	conto_entries = []

	# fetch all products from database
	# TODO is this a good idea, perfwise
	known_products = {p['handle'] : p for p in product.list()}

	# there has to be one item in the order at least
	if not len(_order['products']):
		app.logger.warning(f"product list in order is empty")
		raise ApiError(message='ECARTEMPTY')

	# voucher processing, useable_vouchers is initially loaded (and checked)
	# from the codes in the request, and as they are used during processing they
	# are moved to used_vouchers (so ideally, at the end all the vouchers in
	# useable_vouchers are moved to used_vouchers)
	used_vouchers = []
	# build on a _set_ of all mentioned vouchers
	useable_vouchers = []
	for voucher_code in set(_order['vouchers']):
		try:
			useable_vouchers.append(voucher.find(voucher_code))
		except voucher.VoucherNotFoundError:
			raise ApiError(
				message='EVOUCHERBAD',
				extra={
					'notfound': [voucher_code],
				})
	unuseable_vouchers = []
	# make sure they are all unused
	for _voucher in useable_vouchers:
		if _voucher['used'] is not False:
			unuseable_vouchers.append(_voucher)
	if len(unuseable_vouchers):
		raise ApiError(
			message='EVOUCHERBAD',
			extra={
				'expired': [v['code'] for v in unuseable_vouchers],
			})

	# go over all the ordered products, check
	for requested_product in _order['products']:

		# do we know this product?
		if requested_product['product_handle'] not in known_products:
			app.logger.warning(f"product '{requested_product['product_handle']}' is not known")
			raise ApiError(message='EPRODUCTUNKNOWN', extra=requested_product['product_handle'])

		known_product = known_products[requested_product['product_handle']]

		"""
		requested_product is the product as it appears in the request,
		known_product is the one with the same handle we found in database
		this is what they look like at this point:

		known_product = {
			'handle' : 'some handle',
			'name' : 'some name',
			'price' : int,
			'variants' : [
				{
					'name' : 'a',
					'options' : [
						{
							'name' : 'a number one',
							'slug' : 'a1',
							'price_modifier' : sint,
						},
						...
					],
					...
				},
			],
			'fields' : [
				{
					'name' : 'field one',
					'description' : 'some desc',
					'required' : boolean,
				},
				...
			],
			...
		}

		requested_product = {
			'product_handle' : 'some handle',
			'variants' : [
				{
					'name' : 'a',		# THIS REFERS TO THE VARIANT NAME
					'value' : 'a1',		# THIS TO THE OPTION SLUG
				},
				...
			],
			'fields' : [
				{
					'name' : 'field one',	# THIS TO THE NAME
					'value' : 'filled in data by user',
				},
			],
		}
		"""

		# turn the list of dicts in requested_product['variants'] and ['fields']
		# into dicts keyed by name and valued by value, just for easy lookups
		requested_product_variants = {
			variant['name'] : variant['value']
			for variant in requested_product['variants']
		}
		requested_product_fields = {
			field['name'] : field['value']
			for field in requested_product['fields']
		}

		# go over all the variants we know about, see if they are supplied
		# (and are set to known options)
		for known_variant in known_product['variants']:
			known_variant_options = [option['slug'] for option in known_variant['options']]

			if known_variant['name'] not in requested_product_variants:
				app.logger.warning(f"product '{known_product['name']}'; variant '{known_variant['slug']}' not found in request")
				raise ApiError(message='EVARIANTNEEDED')

			if requested_product_variants[known_variant['name']] not in known_variant_options:
				app.logger.warning(f"product '{known_product['name']}'; variant '{known_variant['slug']}' has unknown option '{requested_product_variants[known_variant['slug']]}'")
				raise ApiError(message='EVARIANTUNKNOWN')

		# go over all the fields we know about, see if they are supplied
		# (and check requiredness)
		for known_field in known_product['fields']:

			if known_field['required']:
				if known_field['name'] not in requested_product_fields:
					app.logger.warning(f"product '{known_product['name']}'; required field '{known_field['name']}' not found in request")
					raise ApiError('EFIELDNEEDED')

		# if we have an unused voucher for this, move it from
		# useable to used
		for _voucher in useable_vouchers:
			if _voucher['applicable_to'] == known_product['category']:
				# mark voucher as used
				used_vouchers.append(_voucher)
				useable_vouchers.remove(_voucher)
				break

		# make a conto entry out of it
		conto_entries.append({
			'session_handle': _session['handle'],
			'client_handle': _client['handle'],
			'product_handle': known_product['handle'],
			'payment_handle': None,
			# FIXME this'll add superfluous variants/fields
			'variants': requested_product['variants'],
			'fields': requested_product['fields'],
		})

	if len(useable_vouchers):
		app.logger.info(f'order has {len(useable_vouchers)} unused vouchers')
		raise ApiError(
			message='EVOUCHERBAD',
			extra={
				'superfluous': [v['code'] for v in useable_vouchers],
			})

	_session['vouchers'] = [_voucher['code'] for _voucher in used_vouchers]
	_session['created_at'] = datetime.now(UTC)
	_session['expires_at'] = _session['created_at'] + timedelta(
		days=app.config['SESSION_EXPIRING_DAYS'])

	session.create(_session)
	for e in conto.schema.load(conto_entries, many=True):
		conto.create(conto.schema.load(e))

	for _voucher in used_vouchers:
		_voucher['used'] = True
		voucher.update(_voucher)

	return _session


"""
prices are represented as
{
  'vat_rate' : <integer cents at this vat rate (vat included)>,
  ...
}
this class allows for adding/substracting them
"""
class ComplexPrice:
	def __init__(self, p={}):
		self.p = p

	def __add__(self, other):
		me = self.p
		you = other.p
		return ComplexPrice({
			k: (me[k] if k in me else 0) + (you[k] if k in you else 0)
			for k in set(me.keys()) | set(you.keys())
		})

	def __sub__(self, other):
		me = self.p
		you = other.p
		return ComplexPrice({
			k: (me[k] if k in me else 0) - (you[k] if k in you else 0)
			for k in set(me.keys()) | set(you.keys())
		})

	def total(self):
		return sum(self.p.values())

	def dict(self):
		return self.p


"""
given a session, look up which products were bought in this session,
figure out what the cost should be (taking variants into account), and
assemble enough information that this all could be shown to a user
"""
def contextualize_session(_session):

	# FIXME Jef data/code breach
	_session['flags'] = {
		'contains_business_ticket' : False
	}

	# get all the items in this session
	entries = conto.get_entries_for_session(_session['handle'])

	_session['client'] = client.get(entries[0]['client_handle'])

	# look up all the products referenced in this session, and load them up
	product_handles = set([e['product_handle'] for e in entries if e['product_handle']])
	relevant_products = {h: product.get(h) for h in product_handles}

	# look up all the payments referenced in this session, and load them up
	payment_handles = set([e['payment_handle'] for e in entries if e['payment_handle']])
	relevant_payments = {h: payment.get(h) for h in payment_handles}

	"""
	within the given product, look up which variant has the
	specified name, and which of that variant's options has the
	specified slug, returning both
	"""
	def find_chosen_variant_option(product, name, value):
		found_variant = None
		for variant in product['variants']:
			if variant['name'] == name:
				found_variant = variant
				break

		# FIXME needs something better than assert
		assert found_variant

		found_option = None
		for option in found_variant['options']:
			if option['slug'] == value:
				found_option = option
				break

		# FIXME needs something better than assert
		assert found_option

		return found_variant, found_option

	"""
	within the given product, look up which field has the
	specified name, and return it
	"""
	def find_chosen_field(product, name):
		found_field = None
		for field in product['fields']:
			if field['name'] == name:
				found_field = field
				break

		# FIXME needs something better than assert
		assert found_field

		return found_field

	# for all entries, we'll store a prettyfied version here
	_session['products'] = []

	# a running cost total of this session
	total = 0

	for entry in entries:

		if not entry['product_handle']:
			# not a product
			continue

		desc = {
			'variants': [],
			'fields': [],
		}

		relevant_product = relevant_products[entry['product_handle']]
		entry_total = ComplexPrice(relevant_product['price'])

		for entry_variant in entry['variants']:
			variant, option = find_chosen_variant_option(
				relevant_product,
				entry_variant['name'],
				entry_variant['value']
			)
			desc['variants'].append({
				'name': variant['name'],
				'description': variant['description'],
				'value': option['slug'],
			})

			# FIXME data/code breach
			if '9bit' == option['slug']:
				_session['flags']['contains_business_ticket'] = True

			# this is where we adjust this product's base price with
			# the chosen variant's modifier
			entry_total += ComplexPrice(option['price_modifier'])

		for entry_field in entry['fields']:
			field = find_chosen_field(
				relevant_product,
				entry_field['name']
			)
			desc['fields'].append({
				'name': field['name'],
				'description': field['description'],
				'value': entry_field['value'],
			})

		if not entry['removed']:
			total += entry_total.total()

		desc['voucher'] = None
		desc['price'] = entry_total.dict()
		desc['product_og_name'] = relevant_product['name']
		desc['category'] = relevant_product['category']
		desc['description'] = relevant_product['description']
		desc['product_handle'] = relevant_product['handle']
		desc['conto_handle'] = entry['handle']
		desc['readable_code'] = entry['readable_code']
		desc['removed'] = entry['removed']
		desc['readable_code'] = entry['readable_code']
		desc['client_readable_code'] = _session['client']['readable_code']
		desc['shifts'] = community_shift.shifts_for_person(entry['handle'])
		desc['session_nonce'] = _session['nonce']
		desc['client_nonce'] = _session['client']['nonce']

		_session['products'].append(desc)

	# now we have a list of items and their calculated price, we can
	# apply the vouchers on the costliest applicable items, of all
	# which are currently not removed (so we may not use all vouchers)
	for voucher_code in _session['vouchers']:
		_voucher = voucher.find(voucher_code)
		# make a list of (unvouchered) items of this type
		applicable_items = [
			desc for desc in _session['products']
			if _voucher['applicable_to'] == desc['category']
			and desc['voucher'] is None
			and desc['removed'] is False
		][:]
		# if we can't find applicable items (they might have been
		# marked as removed), skip this voucher
		if not len(applicable_items):
			continue
		# sort it to price, ascending
		applicable_items.sort(key=lambda desc: ComplexPrice(desc['price']).total())
		# tag the last item with this voucher and zero its price
		vouchered_item = applicable_items[-1]
		vouchered_item['voucher'] = _voucher['code']
		total -= ComplexPrice(vouchered_item['price']).total()
		vouchered_item['price'] = {}

	_session['total'] = total
	_session['payment_code_human'] = helper.humanize_payment_code(
		_session['payment_code'])
	_session['deadline_display'] = helper.readable_date(_session['expires_at'], addtime=False)

	# check payments, see how much is still due
	_session['payments'] = list(relevant_payments.values())
	_session['paid'] = sum([p['amount'] for p in _session['payments']])
	_session['due'] = _session['total'] - _session['paid']

	# is this session over it's payment time?
	_session['expired'] = False

	if not _session['removed'] and _session['due'] > 0:
		if _session['expires_at'] <= now():
			_session['expired'] = True

	return _session


"""
given a client, enrich with the contextualized sessions of that client,
and with some calculated fields
"""
def contextualize_client(_client):

	_client['sessions'] = [
		contextualize_session(session.get(s))
		for s in conto.get_sessions_for_client(_client['handle'])
	]

	# if the client has expired (non-removed) sessions, see if we should
	# remind them of this
	contains_expired_sessions = reduce(
		lambda a, b: a or b,
		[s['expired'] for s in _client['sessions']]
	) if len(_client['sessions']) else False
	_client['remindable'] = False
	_client['removable'] = False
	if contains_expired_sessions:
		if not _client['reminder_expires_at']:
			_client['remindable'] = True
		elif _client['reminder_expires_at'] <= now():
			_client['remindable'] = True
			_client['removable'] = True

	return _client


"""
remind a client of any outstanding expired amounts
"""
def remind_client(_client):

	_client = contextualize_client(_client)

	# do we actually need to do this
	if not _client['remindable']:
		return

	mail.payment_reminder(_client)

	_client['reminder_expires_at'] = now() + timedelta(days=app.config['REMINDER_TIMEOUT_DAYS'])

	new_note = {
		'contents': f"sent payment reminder to client, new date {_client['reminder_expires_at']}",
		'author': "system",
	}
	for _session in _client['sessions']:
		if not _session['removed'] and _session['expired']:
			_session['orga_notes'].append(new_note)
			session.update(_session)

	client.update(_client)

def create_voucher(_voucher):

	# first see if there is a voucher with the same code
	try:
		found_voucher = voucher.find(_voucher['code'])
	except voucher.VoucherNotFoundError:
		pass
	else:
		# it already exists, can't have two
		raise ApiError(message='EVOUCHEREXISTS')

	# check to see if this new voucher refers to a known product
	if _voucher['applicable_to'] not in product.available_categories():
		raise ApiError(message='EPRODUCTUNKNOWN')

	voucher.create(_voucher)

	return voucher.get(_voucher['handle'])


def check_voucher(code):
	code = ''.join(code.split('-'))
	# first see if there is a voucher with the same code
	try:
		found_voucher = voucher.find(code)
		assert found_voucher['used'] is False
	except Exception:
		return {
			'found':  False,
			'applicable_to': '',
		}

	return {
		'found': True,
		'applicable_to': found_voucher['applicable_to'],
	}


"""
Check that the specified session is fully paid, and if so, mail the client.
"""
def resolve_session(_session):

	_session = contextualize_session(_session)

	if _session['due'] <= 0:
		app.logger.warning(f"session paid (due {_session['due']}, sending link")
		mail.session_fully_paid(_session)
		return True
	else:
		app.logger.warning(f"session not fully paid (due {_session['due']}, not sending link")
		return False


"""
Save a payment to the specified session/client.

Runs resolve_session() which might let the client know that the session
has been fully paid.
"""
def process_payment(_client, _session, _payment):

	payment.create(_payment)

	conto_line = conto.schema.load({
		'session_handle': _session['handle'] if 'handle' in _session else None,
		'product_handle': None,
		'client_handle': _client['handle'] if 'handle' in _client else None,
		'payment_handle': _payment['handle'],
	})

	conto.create(conto_line)

	# see if we should send out mails already
	return resolve_session(_session)


class PaymentExistsError(Exception):
	pass
class PaymentNotFoundError(Exception):
	pass
class PaymentManualError(Exception):
	pass

"""
Process a payment coming from an automated setup, meaning it's probably a CSV
copypasted from the bank's website, which generally gives us a timestamp, a
payment code and an amount. Since the user might want to dump the same
but growing CSV to us in its entirety, we need to be able to discern already-
read data. We do this by looking at the combination of timestamp, payment
code and amount.

For actually new payments, we look for the session with that payment code,
and save it to that session with process_payment(). If not found, we'll look
at clients with that payment_code, which is a bit of legacy cruft that
needs to go.
"""
def process_payment_automated(_payment):

	# we might have seen this automated payment before, then we can skip it
	if payment.exists(_payment):
		app.logger.info('already have this payment, skipping')
		raise PaymentExistsError

	payment_code = _payment['payment_code']

	_session = None
	_client = None

	try:
		# first, let's see if we can find the session which has
		# this payment code
		_session = session.find_by_payment_code(payment_code)
		_client = client.get(_session['client_handle'])
	except session.SessionNotFoundError:
		# didn't find it, so look for a client with this payment
		# code (which is backwards compat stuff, needs to go)
		# FIXME jef
		app.logger.warning(f"looking for payment code, did not find session")
		try:
			_client = client.find_by_payment_code(payment_code)
			raise PaymentManualError
		except PaymentManualError:
			raise PaymentManualError
		except:
			app.logger.warning("looking for payment code, did not find client")
			raise PaymentNotFoundError
	except:
		raise PaymentNotFoundError

	# yeah we need _something_ to go on
	if not (_session or _client):
		raise PaymentNotFoundError

	return process_payment(_client, _session, _payment)

def community_person(entry):
	out = {
		'readable_code': entry['readable_code'],
		'client_readable_code': entry['client_readable_code'],
		'conto_handle': entry['conto_handle'],
		'client_nonce': entry['client_nonce'],
		'session_nonce': entry['session_nonce'],
		#'shifts': entry['shifts'],
		'age': None,
		'email': None,
		'name': None,
		'allowed': True,
		'expected': True,
		'expected_reason' : 'unknown'
	}
	if not entry['product_og_name'] == 'ticket':
		return None

	# skip removed ones
	if entry['removed'] == True:
		return None

	# find the specific ticket type
	bits = False
	for _variant in entry['variants']:
		if _variant['name'] == 'bits':
			bits = _variant['value']


	# and the person's details
	for _field in entry['fields']:
		if _field['name'] == 'name':
			out['name'] = _field['value']
		elif _field['name'] == 'age':
			out['age'] = _field['value']
		elif _field['name'] == 'email':
			out['email'] = _field['value']

	# too young? skip
	if int(out['age']) < 16:
		# return None
		out['allowed'] = False
		out['expected'] = False
		out['expected_reason'] = 'age'
		return out

	# are we expecting them to help out?
	if bits not in ['6bit', '7bit']:
		out['expected'] = False
		if bits == '9bit':
			out['expected_reason'] = 'biz'
		else:
			out['expected_reason'] = 'tourist'

	return out


"""
given a list of contextualized sessions, filter out a list
of people which could help the community
"""
def community_people(contextualized_sessions, only_paid=True):

	people = []

	for _session in contextualized_sessions:
		# skip unpaid ones
		if _session['due'] > 0 and only_paid:
			continue

		# and removed ones
		if _session['removed'] == True:
			continue

		# find the tickets
		for _product in _session['products']:
			person = community_person(_product)
			if person:
				people.append(person)

	return people

def emailstocodes():
	lines = {}
	clients = [contextualize_client(c) for c in client.list()]
	for _client in clients:
		hasproducts = False
		for _session in _client['sessions']:
			if _session['due'] > 0 :
				continue
			for p in _session['products']:
				if not p['removed']:
					hasproducts = True
				for f in p['fields']:
					if f['name'] == 'email' and f['value']:	
						key = f"{f['value'].lower()}_{_client['readable_code']}"
						lines[key] = {
							'email' : f['value'].lower(),
							'code' : _client['readable_code'],
							'nonce' : _client['nonce']
						}
		if hasproducts == True:
			key = f"{_client['email'].lower()}_{_client['readable_code']}"
			lines[key] = {
				'email' : _client['email'].lower(),
				'code' : _client['readable_code'],
				'nonce' : _client['nonce']
			}

	lines = list(lines.values())
	lines.sort(key=lambda x: x['email'], reverse=False)
	return lines