# voucher database model
# fr1ckets, the fri3dcamp devs, 2022
from flask import g
from marshmallow import Schema, fields, ValidationError
from datetime import UTC
from fr1ckets.model.util import gen_handle, gen_nonce, gen_voucher_code, gen_payment_code, DBMixinJson, now


class VoucherSchema(Schema):
	handle = fields.Str(load_default=gen_handle)
	applicable_to = fields.Str(missing='missing')
	notes = fields.Str(missing='')
	code = fields.Str(load_default=gen_voucher_code)
	used = fields.Bool(missing=False)
	created_at = fields.AwareDateTime(format='iso', default_timezone=UTC, missing=now)


class DBVoucherSchema(VoucherSchema, DBMixinJson):
	pass


schema = VoucherSchema()
db_schema = DBVoucherSchema()


class VoucherNotFoundError(Exception):
	pass


class VoucherExistsError(Exception):
	pass


def get(handle):

	q = """
		SELECT
			handle,
			data
		FROM
			voucher
		WHERE
			handle = ?;
		"""

	g.db_cur.execute(q, (handle,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise VoucherNotFoundError


def find(code):

	q = """
		SELECT
			handle,
			data
		FROM
			voucher
		WHERE json_extract(data, '$.code') == ?;
		"""

	g.db_cur.execute(q, (code,))
	row = g.db_cur.fetchone()

	try:
		return db_schema.load(row)
	except ValidationError:
		raise VoucherNotFoundError
	except Exception as e:
		raise e


def list():

	q = """
		SELECT
			handle,
			data
		FROM
			voucher
		ORDER BY
			json_extract(data, '$.created_at') DESC;
		"""

	g.db_cur.execute(q)
	rows = g.db_cur.fetchall()

	try:
		return db_schema.load(rows, many=True)
	except ValidationError:
		raise VoucherNotFoundError


def create(data):

	q = """
		INSERT INTO voucher (
			handle,
			data
			)
		VALUES (
			:handle,
			:data
		);
		"""

	try:
		find(data['code'])
	except VoucherNotFoundError:
		pass
	else:
		raise VoucherExistsError

	g.db_cur.execute(q, db_schema.dump(data))


def update(_voucher):

	q = """
		UPDATE
			voucher
		SET
			data=:data
		WHERE
			handle=:handle;
		"""

	g.db_cur.execute(q, db_schema.dump(_voucher))

	if g.db_cur.rowcount != 1:
		raise VoucherNotFoundError


def delete(handle):

	q = """
		DELETE FROM voucher
		WHERE handle = ?;
		"""

	g.db_cur.execute(q, (handle,))

	if g.db_cur.rowcount != 1:
		raise VoucherNotFoundError
