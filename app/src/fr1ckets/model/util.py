# db utilities
# fr1ckets, the fri3dcamp devs, 2022
from json import loads, dumps
from marshmallow import Schema, pre_load, post_dump, ValidationError
from ulid import ULID
from flask import g
from string import ascii_letters, digits
from random import choice, randint
from functools import wraps
from datetime import datetime, UTC


def gen_handle():
	return str(ULID())

def gen_voucher_code():
	return ''.join(
		[
			choice(ascii_letters) 
			for _
			in range(32)
		]
	).upper()

def gen_nonce():
	return ''.join(
		[
			choice(ascii_letters + digits)
			for _
			in range(32)
		]
	)

def gen_friendly():
	nouns_plural = [ "hackers", "flowers", "cops", "bees", "routers", "wasps", "tents", "trees", "lasers", "leds", "oscillators", "modems", "signals", "foxes" ]
	adjectives = [ "splendid", "electronic", "cyber", "soldered", "recycled", "flabberghasted" ]
	verbs_third_plural = [ "hack", "solder", "route", "laser", "code", "beep", "play", "analyze" ]
	verbs_gerund = [ "waving", "hacking", "soldering", "routing", "lasering", "coding", "beeping", "playing" ]

	forms = [
		[["do"], nouns_plural, verbs_third_plural],
		[["are"], nouns_plural, verbs_gerund],
		[["are"], nouns_plural, verbs_gerund],
		[["are"], adjectives, nouns_plural, verbs_gerund],
		[["can"], nouns_plural, verbs_third_plural, nouns_plural],
		[adjectives, nouns_plural, verbs_third_plural, nouns_plural],
	]

	return ' '.join([choice(p) for p in choice(forms)])


def gen_payment_code():
	code = randint(0, 9999999999)
	mod = code % 97
	mod = 97 if mod == 0 else mod

	return f"{code:010d}{mod:02d}"


def now():
	return datetime.now(UTC)


def now_ascii():
	return datetime.now(UTC).isoformat()


def db_save(fn):
	"""
	wrapper for API calls which wish to save DB operations,
	providing they didn't crash
	"""
	@wraps(fn)
	def wrapped(*args, **kwargs):
		g.db_commit = True
		return fn(*args, **kwargs)
	return wrapped


class DBMixinJson(Schema):
	"""
	A mixin to load/store schema fields into one JSON blob in database.

	Say we have a nice Schema describing a Doodad:

	class DoodadSchema(marshmallow.Schema):
		handle = fields.Uuid()
		foo = fields.Str()
		bar = fields.Int()
		quux = fields.Bool()

	And our doodad table in database looks like:

	CREATE TABLE doodad (
		'handle' TEXT PRIMARY KEY,
		'data' JSON
	)

	Our goal is that the handle field gets stored/loaded to/from the
	handle row, but the foo/bar/quux fields get shoved into a JSON
	object stored/loaded to/from the data column.

	You can do this by subclassing this mixin:

	class DBDoodadSchema(DoodadSchema, DBMixinJson):
		# name of the db column where we'll dump/load the JSON
		json_field = 'data'
		# list of fields which should go to their own columns
		do_not_push = [ 'handle' ]

	And anywhere it gets stored to/loaded from the database:

	q = "INSERT INTO doodad (handle, data) VALUES (:handle, :data);"
	cur.execute(q, DBDoodadSchema().dump(my_doodad))

	...

	cur.execute("SELECT handle, data FROM doodad WHERE handle='boink';")
	my_doodad = DBDoodadSchema().load(cur.fetchone())
	"""

	json_field = 'data'
	do_not_push = ['handle']

	@pre_load
	def unwrap(self, data, **kwargs):
		# pull the JSON out of that field, deserialize it
		# if the database automatically unserializes JSON (sqlite doesn't)
		# we should drop the loads() here
		if not data:
			raise ValidationError("working on None")
		json_data = loads(data.pop(self.json_field))
		# go over the keys in there, pull them up into data
		for k in json_data:
			if k in self.do_not_push:
				continue
			data[k] = json_data[k]
		return data

	@post_dump
	def wrap(self, data, **kwargs):
		if not data:
			raise ValidationError("working on None")
		dest = {}
		# loop over all the keys, push what isn't marked as do_not_push
		# to the json field
		for k in [k for k in data]:		# use original keys, we'll pop()
			if k in self.do_not_push:
				continue
			dest[k] = data.pop(k)
		# if the database automatically serializes JSON (sqlite doesn't)
		# we should drop the dumps() here
		data[self.json_field] = dumps(dest)
		return data
