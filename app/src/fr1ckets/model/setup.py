# db setup/teardown code
# fr1ckets, the fri3dcamp devs, 2022
from flask import g, request
import sqlite3
from fr1ckets import app
from fr1ckets.model.util import gen_nonce
from fr1ckets.model.orga import user

"""
Setup/teardown a database connection/cursor for every request.

The cursor has a freshly opened transaction, if the request
wants to see any db writes it does committed, it should set
g.db_commit to True, prompting db_wrapup() to actually commit.
Leaving this False/undefined (or crashing) will cause it to
rollback.

In database terms, we're running sqlite in WAL mode [0], and
by nature its transactions are SERIALIZABLE [1]. We ask for
a manual (albeint deferred) transaction which requires an
explicit COMMIT/ROLLBACK with isolation_level=DEFERRED. This
should be more canonical than the usual starting with auto-
commit (isolation_level=None) and executing a "BEGIN DEFERRED"
ourselves. The docs [2] are little unclear if this is equiv,
but pysqlite's connection.c seems to agree.

[0] https://www.sqlite.org/wal.html
[1] https://www.sqlite.org/lang_transaction.html 2.1
[2] https://docs.python.org/3/library/sqlite3.html#sqlite3-controlling-transactions
"""

def dict_factory(cursor, row):
	# a small helper to return dicts
	fields = [ col[0] for col in cursor.description ]
	return { key : value for key, value in zip(fields, row) }


@app.before_request
def db_setup():

	# a good nginx conf shouldn't run us for static files, but make sure here
	if request and request.path.startswith('/static'):
		return

	g.db_con = sqlite3.connect(
			app.config['DB_PATH'],
			detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
			isolation_level='DEFERRED',
			timeout=20)

	if app.config['DEBUG']:
		g.db_con.set_trace_callback(print)
	g.db_con.execute('PRAGMA journal_mode=WAL;')
	g.db_con.execute('PRAGMA foreign_keys=ON;')
	g.db_con.row_factory = dict_factory

	g.db_cur = g.db_con.cursor()

@app.teardown_request
def db_wrapup(error):

	# a good nginx conf shouldn't run us for static files, but make sure here
	if request and request.path.startswith('/static'):
		return

	db_con = getattr(g, 'db_con', None)
	db_commit = getattr(g, 'db_commit', None)

	if db_con:
		if db_commit and not error:
			# explicit commit and no errors is the only way
			# we'll actually commit
			db_con.commit()
		else:
			db_con.rollback()
		db_con.close()
	else:
		app.logger.error("can't find database connection in request")

"""
check if the database at app.conf['DB_PATH'] is alive and well, by verifying
that there is a "client" table in there

if not, it is re-initialized from app.conf['DB_SQL_PATH']
"""
def db_check():
	db_con = sqlite3.connect(
			app.config['DB_PATH'],
			detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
			isolation_level='DEFERRED',
			timeout=20)

	if app.config['DEBUG']:
		db_con.set_trace_callback(print)
	db_con.row_factory = dict_factory

	db_cur = db_con.cursor()

	q = """
		SELECT
			handle
		FROM
			client
		LIMIT 1;
		"""

	try:
		db_cur.execute(q)
		app.logger.info(f"using database {app.config['DB_PATH']}")
		return
	except sqlite3.OperationalError:
		pass

	app.logger.warning(f"database {app.config['DB_PATH']} does not seem to be populated")
	app.logger.warning(f"loading {app.config['DB_PATH']} from {app.config['DB_SQL_PATH']}")

	create_sql = ''
	with open(app.config['DB_SQL_PATH'], 'r') as f:
		create_sql = f.read()

	db_cur.executescript(create_sql)

	app.logger.warning(f"loaded {app.config['DB_PATH']} from {app.config['DB_SQL_PATH']}")
