DROP TABLE IF EXISTS conto_community_shift;
DROP TABLE IF EXISTS conto;
DROP TABLE IF EXISTS payment;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS voucher;
DROP TABLE IF EXISTS orga_user;
DROP TABLE IF EXISTS community_shift;


CREATE TABLE community_shift (
	'handle' TEXT PRIMARY KEY,
	'data' JSON
);

CREATE TABLE orga_user (
	'handle' TEXT PRIMARY KEY,
	'data' JSON
);

CREATE TABLE voucher (
	'handle' TEXT PRIMARY KEY,
	'data' JSON
);

CREATE TABLE session (
	'handle' TEXT PRIMARY KEY,
	'data' JSON
);

CREATE TABLE client (
	'handle' TEXT PRIMARY KEY,
	'data' JSON
);

CREATE TABLE product (
	'handle' TEXT PRIMARY KEY,
	'data' JSON
);

CREATE TABLE payment (
	'handle' TEXT PRIMARY KEY,
	'data' JSON
);

CREATE TABLE conto (
	'handle' TEXT PRIMARY KEY,
	'session_handle' TEXT,
	'client_handle' TEXT,
	'product_handle' TEXT,
	'payment_handle' TEXT,
	'data' JSON,
	FOREIGN KEY('session_handle') REFERENCES session ('handle') ON DELETE SET NULL,
	FOREIGN KEY('client_handle') REFERENCES client ('handle') ON DELETE SET NULL,
	FOREIGN KEY('product_handle') REFERENCES product ('handle') ON DELETE SET NULL,
	FOREIGN KEY('payment_handle') REFERENCES payment ('handle') ON DELETE SET NULL
);

CREATE TABLE conto_community_shift (
	'handle' TEXT PRIMARY KEY,
	'conto_handle' TEXT,
	'community_shift_handle' TEXT,
	FOREIGN KEY('conto_handle') REFERENCES conto ('handle') ON DELETE SET NULL,
	FOREIGN KEY('community_shift_handle') REFERENCES community_shift ('handle') ON DELETE SET NULL
);
