#!/usr/bin/env python3
# tests for fr1ckets
# fr1ckets, the fri3dcamp devs, 2022
import unittest
import pprint
import fr1ckets
import logging
import os
import tempfile
import sqlite3
import itertools
from datetime import datetime, UTC, timedelta
from fr1ckets.model.client import ClientNotFoundError, ClientExistsError, list as admin_client_list
from fr1ckets.model.product import ProductNotFoundError, ProductExistsError, list as product_list
from fr1ckets.model.voucher import VoucherNotFoundError, VoucherExistsError, list as voucher_list
from fr1ckets.model.logic import OrderNotPossibleError, ComplexPrice as Price
from string import ascii_letters
from random import choice

P = pprint.pprint
NOW = datetime.now(UTC).isoformat()
FALLBACK_EMAIL = 'fallback@something.invalid'
FALLBACK_CAN_ORDER_FROM = datetime.fromisoformat('1900-01-01T00:00:00+00:00').isoformat()
PRODUCTS = {}
DISCLAIMERS = [
	'disclaimer_animal',
	'disclaimer_have_accompanying_adult',
	'disclaimer_be_excellent',
]

def random_email():
	user = ''.join([ choice(ascii_letters) for _ in range(16) ])
	return f'{user}@testing.invalid'

def random_string():
	return ''.join([choice(ascii_letters) for _ in range(64)])

def order_defaults():
	return {d: True for d in DISCLAIMERS}

def structured_comment(ann):

	if not len(ann) != '12':
		return False

	number = int(ann[0:10])
	checksum = int(ann[10:12])

	if number == 0:
		if checksum == 97:
			return True
	else:
		if (number % 97) == checksum:
			return True

	return False

def products_strip_from_receipt(products):
	return [
		{
			'product_handle': e['product_handle'],
			'variants': [
				{
					'name': v['name'],
					'value': v['value'],
				} for v in e['variants']
			],
			'fields': [
				{
					'name': f['name'],
					'value': f['value'],
				} for f in e['fields']
			],
		}
		for e in products
	]

def products_select_carthesian(available_products):

	chosen_products = []
	chosen_total = Price()

	for what in available_products:

		variant_matrix = []

		for variant in what['variants']:
			options = []
			for option in variant['options']:
				options.append({
					'name': variant['name'],
					'value':  option['slug'],
					'price_modifier': option['price_modifier'],
				})
			variant_matrix.append(options)

		for combination in list(itertools.product(*variant_matrix)):
			chosen_price = Price(what['price'])
			for _choice in combination:
				chosen_price += Price(_choice['price_modifier'])

			fields = []
			for field in what['fields']:
				fields.append({
					'name': field['name'],
					'value': random_string() if field['required'] else 'was not required',
				})

			chosen_item = {
				'product_handle': what['handle'],
				'variants': [
					{
						'name': c['name'],
						'value': c['value'],
					}
					for c in combination
				],
				'fields': fields,
			}

			chosen_total += chosen_price
			chosen_products.append(chosen_item)
	
	return chosen_products, chosen_total

class Unexpected(Exception):
	def __init__(self, status_code):
		self.status_code = status_code

class TempKnownClient:
	def __init__(self, cls, email, can_order_from=None):
		self.cls = cls
		self.email = email
		self.can_order_from = can_order_from or NOW
	
	def __enter__(self):
		self._client = self.cls.admin_client_create(self.email, self.can_order_from)
		return self._client

	def __exit__(self, type, value, traceback):
		if isinstance(value, Exception):
			return
		self.cls.admin_client_delete(self._client['handle'])

class TempUnknownClient:
	def __init__(self, cls, email):
		self.cls = cls
		self.email = email

	def __enter__(self):
		pass

	def __exit__(self, type, value, traceback):
		if isinstance(value, Exception):
			return
		self._client = self.cls.admin_client_find(self.email)
		self.cls.admin_client_delete(self._client['handle'])

class ProductVariant:
	def __init__(self, name, description=None, category=None):
		self.data = {}
		self.data['name'] = name
		self.data['description'] = description or f"description of {name}"
		self.data['options'] = []

	def add_option(self, name, slug=None, price_modifier={}):
		self.data['options'].append({
			'name' : name,
			'slug' : slug or name.replace(' ', '-'),
			'price_modifier' : price_modifier
		})

	def render(self):
		return self.data

class Product:
	def __init__(self, name, description=None, price={}, category=None):
		self.data = {}
		self.data['name'] = name
		self.data['category'] = category or name
		self.data['description'] = description or f"description of {name}"
		self.data['price'] = price
		self.data['fields'] = []
		self.variants = []

	def add_variant(self, name, description=None):
		v = ProductVariant(name, description)
		self.variants.append(v)
		return v

	def add_field(self, name, description=None, required=False):
		self.data['fields'].append({
			'name' : name,
			'description' : description or f"description of {name}",
			'required' : required,
		})

	def render(self):
		self.data['variants'] = [ v.render() for v in self.variants ]
		return self.data

p = Product('a complex product', description='full-option', price={'6': 1000, '21': 10000}, category="complex")
v = p.add_variant('colour', 'colour of the product')
v.add_option('red', price_modifier={'21': -100})
v.add_option('blue', slug='special_slug')
v.add_option('green', price_modifier={'21': 100})
v = p.add_variant('angle', 'how should the lines be drawn?')
v.add_option('parallel')
v.add_option('perpendicular')
p.add_field('thoughts on colour', description='this is required', required=True)
p.add_field('thoughts on angle', description='this is not required', required=False)
PRODUCTS['complex'] = p

p = Product('a simple product', description='as simple as possible', price={'21': 1000}, category="simple")
PRODUCTS['simple'] = p


class TempProduct:
	def __init__(self, cls, product):
		self.cls = cls
		self.product = product

	def __enter__(self):
		self._product = self.cls.admin_product_create(self.product)
		return self._product

	def __exit__(self, type, value, traceback):
		if isinstance(value, Exception):
			return
		self.cls.admin_product_delete(self._product['handle'])

class TempProducts:
	def __init__(self, cls, products):
		self.cls = cls
		self.products = products

	def __enter__(self):
		self._products = []
		for p in self.products:
			self._products.append(self.cls.admin_product_create(p))
		return self._products

	def __exit__(self, type, value, traceback):
		if isinstance(value, Exception):
			return
		for p in self._products:
			self.cls.admin_product_delete(p['handle'])

class TempVoucher:
	def __init__(self, cls, product_category):
		self.cls = cls
		self.product_category = product_category

	def __enter__(self):
		self._voucher = self.cls.admin_voucher_create({
			'applicable_to': self.product_category,
		})
		return self._voucher

	def __exit__(self, type, value, traceback):
		if isinstance(value, Exception):
			return
		self.cls.admin_voucher_delete(self._voucher['handle'])

class TestCase(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		cls.db_fd, cls.db_name = tempfile.mkstemp()
	
	@classmethod
	def tearDownClass(cls):
		os.close(cls.db_fd)
		os.unlink(cls.db_name)

	def setUp(self):
		fr1ckets.app.config.from_object('config')
		# throw exceptions directly rather than in-band
		fr1ckets.app.config['PROPAGATE_EXCEPTIONS'] = True
		fr1ckets.app.config['DB_PATH'] = self.db_name
		self.app = fr1ckets.app.test_client()
		with fr1ckets.app.app_context():
			from fr1ckets.api import admin
			fr1ckets.app.logger.setLevel(logging.WARNING)
			fr1ckets.model.setup.db_check()
			fr1ckets.model.setup.db_setup()

	def tearDown(self):
		pass

	def admin_client_list(self):
		rv = self.app.get('/api/admin/clients')
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_client_get(self, handle):
		rv = self.app.get(f'/api/admin/client/{handle}')
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_client_find(self, email):
		rv = self.app.get(f'/api/admin/client/find/{email}')
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_client_create(self, email, can_order_from):
		rv = self.app.post('/api/admin/clients', json={
			'email' : email,
			'can_order_from' : can_order_from,
		})
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_client_delete(self, handle):
		rv = self.app.delete(f'/api/admin/client/{handle}')
		if rv.status_code != 204:
			raise Unexpected(rv.status_code)

	def admin_product_list(self):
		rv = self.app.get('/api/admin/products')
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_product_get(self, handle):
		rv = self.app.get(f'/api/admin/product/{handle}')
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_product_create(self, product):
		rv = self.app.post('/api/admin/products', json=product)
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_product_delete(self, handle):
		rv = self.app.delete(f'/api/admin/product/{handle}')
		if rv.status_code != 204:
			raise Unexpected(rv.status_code)

	def admin_voucher_list(self):
		rv = self.app.get('/api/admin/vouchers')
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_voucher_get(self, handle):
		rv = self.app.get(f'/api/admin/voucher/{handle}')
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_voucher_create(self, voucher):
		rv = self.app.post('/api/admin/vouchers', json=voucher)
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def admin_voucher_delete(self, handle):
		rv = self.app.delete(f'/api/admin/voucher/{handle}')
		if rv.status_code != 204:
			raise Unexpected(rv.status_code)

	def admin_client_conto_get(self, handle):
		rv = self.app.get(f'/api/admin/client/{handle}/conto')
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def shop_get(self, email):
		rv = self.app.post('/api/shop/get', json={'email': email})
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def shop_voucher_check(self, code):
		rv = self.app.post('/api/shop/voucher', json={'code': code})
		if (rv.status_code // 100) != 2:
			raise Unexpected(rv.status_code)
		return rv.get_json()

	def shop_order(self, order, inhibit_defaults=False):
		o = order.copy()
		if not inhibit_defaults:
			o.update(order_defaults())
		rv = self.app.post('/api/shop/order', json=o)
		# no checking here, test code should check specific retvals
		return rv.get_json()

	def shop_quote(self, order, inhibit_defaults=False):
		o = order.copy()
		if not inhibit_defaults:
			o.update(order_defaults())
		rv = self.app.post('/api/shop/quote', json=o)
		# no checking here, test code should check specific retvals
		return rv.get_json()

	"""
	runs first, set the fallback client
	"""
	def test_aaa_database_initial(self):
		self.admin_client_create(FALLBACK_EMAIL, FALLBACK_CAN_ORDER_FROM)

	"""
	can we create a client and get it back?
	"""
	def test_admin_client_get(self):
		email = random_email()

		with TempKnownClient(self, email, NOW) as created_client:
			read_back_client = self.admin_client_get(created_client['handle'])
			assert created_client == read_back_client

	"""
	can we create a client, spot it, delete it, not spot it?
	"""
	def test_admin_client_list(self):
		email = random_email()

		emails_before = self.admin_client_list()

		emails_after_create = []
		with TempKnownClient(self, email, NOW) as created_client:
			emails_after_create = self.admin_client_list()

		emails_after_delete = self.admin_client_list()

		assert email not in [ c['email'] for c in emails_before ]
		assert email in [ c['email'] for c in emails_after_create ]
		assert email not in [ c['email'] for c in emails_after_delete ]

	"""
	can we create a product and get it back?
	"""
	def test_admin_product_get(self):
		with TempProduct(self, PRODUCTS['complex'].render()) as created_product:
			read_back_product = self.admin_product_get(created_product['handle'])
			assert created_product == read_back_product

	"""
	can we create a product, spot it, delete it, not spot it?
	"""
	def test_admin_product_list(self):
		products_before = self.admin_product_list()
		handle = None

		products_after_create = []
		with TempProduct(self, PRODUCTS['complex'].render()) as created_product:
			handle = created_product['handle']
			products_after_create = self.admin_product_list()

		products_after_delete = self.admin_product_list()

		assert handle not in [ c['handle'] for c in products_before ]
		assert handle in [ c['handle'] for c in products_after_create ]
		assert handle not in [ c['handle'] for c in products_after_delete ]

	"""
	shop; given a populated shop and a unregistered user who is currently allowed to order
	(due to the fallback), are we given the full product list and told we can order
	"""
	def test_shop_unknown_can_order_via_fallback(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempProducts(self, catalogue) as created_products:
			gotten = self.shop_get(email)
			assert gotten['client']['email'] == email
			assert gotten['client']['can_order'] == True
			assert gotten['client']['can_order_from'] == FALLBACK_CAN_ORDER_FROM
			assert gotten['products'] == created_products

	"""
	shop; given a populated shop and a registered user who is currently allowed to order,
	are we given the full product list and told we can order
	"""
	def test_shop_known_can_order_on_time(self):
		email = random_email()
		past = (datetime.now(UTC) - timedelta(seconds=1)).isoformat()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempProducts(self, catalogue) as created_products:
			with TempKnownClient(self, email, past) as created_client:
				gotten = self.shop_get(email)
				assert gotten['client']['email'] == email
				assert gotten['client']['can_order'] == True
				assert gotten['client']['can_order_from'] == past
				assert gotten['products'] == created_products

	"""
	shop; given a populated shop and a registered user who is not yet allowed to order,
	will we get an empty product list and be told we can't order yet
	"""
	def test_shop_known_cant_order_too_early(self):
		email = random_email()
		future = (datetime.now(UTC) + timedelta(seconds=1)).isoformat()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempProducts(self, catalogue) as created_products:
			with TempKnownClient(self, email, future) as created_client:
				gotten = self.shop_get(email)
				assert gotten['client']['email'] == email
				assert gotten['client']['can_order'] == False
				assert gotten['client']['can_order_from'] == future
				assert gotten['products'] == []

	"""
	shop; can we make an order as a unknown client and look up admin bookkeeping
	"""
	def test_shop_unknown_order_shows_in_conto(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempUnknownClient(self, email):

			with TempProducts(self, catalogue) as created_products:

				# order one of all products
				ordering_products = []
				for product in created_products:
					variants = []
					for variant in product['variants']:
						variants.append({
							'name' : variant['name'],
							'value' : variant['options'][0]['slug'],
						})
					fields = []
					for field in product['fields']:
						fields.append({
							'name' : field['name'],
							'value' : f"a value for {field['name']}",
						})
					ordering_products.append({
						'product_handle' : product['handle'],
						'variants' : variants,
						'fields' : fields,
					})

				notes = random_string()
				receipt = self.shop_order({
					'email' : email,
					'products' : ordering_products,
					'notes': notes,
				})

				# did we get what we need from the receipt, the session_handle
				assert 'handle' in receipt
				assert len(receipt['handle'])

				# can we find this previously unknown client in database now
				created_client = self.admin_client_find(email)
				assert created_client

				conto = self.admin_client_conto_get(created_client['handle'])

				# do we see our single session in the conto
				assert len(conto) == 1
				session = conto[0]
				assert session['session']['handle'] == receipt['handle']

				# do our purchased products match up
				assert len(session['entries']) == len(ordering_products)
				for ordering_product in ordering_products:
					# do we find the ordered product in the entries
					entry = None
					for e in session['entries']:
						if e['product_handle'] == ordering_product['product_handle']:
							entry = e
							break
					assert entry

					# does the meta info match
					assert entry['variants'] == ordering_product['variants']
					assert entry['fields'] == ordering_product['fields']

				# do our notes
				assert receipt['notes'] == notes
				assert session['session']['notes'] == notes

		# FIXME; now empty due to ON DELETE SET NULL, but future?
		conto = self.admin_client_conto_get(created_client['handle'])
		assert not len(conto)


	"""
	shop; can we make an order as a known client and look up admin bookkeeping
	"""
	def test_shop_known_order_shows_in_conto(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempKnownClient(self, email) as created_client:

			# should start off with an empty conto
			conto = self.admin_client_conto_get(created_client['handle'])
			assert not len(conto)

			with TempProducts(self, catalogue) as created_products:

				# order one of all products
				ordering_products = []
				for product in created_products:
					variants = []
					for variant in product['variants']:
						variants.append({
							'name' : variant['name'],
							'value' : variant['options'][0]['slug'],
						})
					fields = []
					for field in product['fields']:
						fields.append({
							'name' : field['name'],
							'value' : f"a value for {field['name']}",
						})
					ordering_products.append({
						'product_handle' : product['handle'],
						'variants' : variants,
						'fields' : fields,
					})

				notes = random_string()
				receipt = self.shop_order({
					'email': email,
					'notes': notes,
					'products': ordering_products,
				})

				# did we get what we need from the receipt, the session handle
				assert 'handle' in receipt
				assert len(receipt['handle'])

				conto = self.admin_client_conto_get(created_client['handle'])

				# do we see our single session in the conto
				assert len(conto) == 1
				session = conto[0]
				assert session['session']['handle'] == receipt['handle']

				# do our purchased products match up
				assert len(session['entries']) == len(ordering_products)
				for ordering_product in ordering_products:
					# do we find the ordered product in the entries
					entry = None
					for e in session['entries']:
						if e['product_handle'] == ordering_product['product_handle']:
							entry = e
							break
					assert entry

					# does the meta info match
					assert entry['variants'] == ordering_product['variants']
					assert entry['fields'] == ordering_product['fields']

				# do our notes
				assert receipt['notes'] == notes
				assert session['session']['notes'] == notes

		# FIXME; now empty due to ON DELETE SET NULL, but future?
		conto = self.admin_client_conto_get(created_client['handle'])
		assert not len(conto)

	"""
	shop; can we make an order with improperly filled disclaimers
	"""
	def test_shop_known_cant_order_bad_disclaimers(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempKnownClient(self, email) as created_client:
			with TempProducts(self, catalogue) as created_products:
				product = None
				for p in created_products:
					if p['name'] == PRODUCTS['simple'].render()['name']:
						product = p
				assert product

				order = {
					'email' : email,
					'products' : [
						{
							'product_handle' : product['handle'],
						}
					],
				}
				disclaimers = [
					'pay_on_time',
					'have_accompanying_adult',
					'be_excellent',
				]

				# no disclaimers at all
				tweaked_order = order.copy()
				ret = self.shop_order(tweaked_order, inhibit_defaults=True)
				assert (ret['code'] // 100) == 4
				assert ret['message'] == 'EDISCLAIMERS'
				# all disclaimers are false
				tweaked_order = order.copy()
				tweaked_order.update({
					f'disclaimer_{d}': False
					for d in disclaimers
				})
				ret = self.shop_order(tweaked_order, inhibit_defaults=True)
				assert (ret['code'] // 100) == 4
				assert ret['message'] == 'EDISCLAIMERS'

				# meddle with individual disclaimers
				for disclaimer in disclaimers:
					tweaked_order = order.copy()
					tweaked_order.update({
						f'disclaimer_{d}': True
						for d in disclaimers
					})
					# flip this specific one to false
					tweaked_order[f'disclaimer_{disclaimer}'] = False
					ret = self.shop_order(tweaked_order, inhibit_defaults=True)
					assert (ret['code'] // 100) == 4
					assert ret['message'] == 'EDISCLAIMERS'

					tweaked_order = order.copy()
					tweaked_order.update({
						f'disclaimer_{d}': True
						for d in disclaimers
					})
					# forget this specific one
					tweaked_order[f'disclaimer_{disclaimer}'] = False
					ret = self.shop_order(tweaked_order, inhibit_defaults=True)
					assert (ret['code'] // 100) == 4
					assert ret['message'] == 'EDISCLAIMERS'


	"""
	shop; can we make an order (a single simple item)
	"""
	def test_shop_known_can_order_simple_item(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempKnownClient(self, email) as created_client:
			with TempProducts(self, catalogue) as created_products:
				product = None
				for p in created_products:
					if p['name'] == PRODUCTS['simple'].render()['name']:
						product = p
				assert product

				self.shop_order({
					'email' : email,
					'products' : [
						{
							'product_handle' : product['handle'],
						}
					],
				})

	"""
	shop; can we make an order (a single complex item)
	"""
	def test_shop_known_can_order_complex_item(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempKnownClient(self, email) as created_client:
			with TempProducts(self, catalogue) as created_products:
				product = None
				for p in created_products:
					if p['name'] == PRODUCTS['complex'].render()['name']:
						product = p
				assert product

				variants = []
				for variant in product['variants']:
					variants.append({
						'name' : variant['name'],
						'value' : variant['options'][0]['slug'],
					})
				fields = []
				for field in product['fields']:
					fields.append({
						'name' : field['name'],
						'value' : f"a value for {field['name']}",
					})

				self.shop_order({
					'email' : email,
					'products' : [
						{
							'product_handle' : product['handle'],
							'variants' : variants,
							'fields' : fields,
						}
					],
				})

	"""
	shop; is making an empty order forbidden
	"""
	def test_shop_known_cant_order_empty_cart(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempKnownClient(self, email) as created_client:
			with TempProducts(self, catalogue) as created_products:

				ret = self.shop_order({
					'email' : email,
					'products' : [],
				})
				assert ret['code'] // 100 == 4
				assert ret['message'] == 'ECARTEMPTY'

	"""
	shop; is making an order with an unknown product handle forbidden
	"""
	def test_shop_known_cant_order_unknown_product(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempKnownClient(self, email) as created_client:
			with TempProducts(self, catalogue) as created_products:
				# set up a product
				product = None
				for p in created_products:
					if p['name'] == PRODUCTS['complex'].render()['name']:
						product = p
				assert product

				variants = []
				for variant in product['variants']:
					variants.append({
						'name' : variant['name'],
						'value' : variant['options'][0]['slug'],
					})
				fields = []
				for field in product['fields']:
					fields.append({
						'name' : field['name'],
						'value' : f"a value for {field['name']}",
					})

				# order but reverse the handle
				ret = self.shop_order({
					'email' : email,
					'products' : [
						{
							'product_handle' : product['handle'][::-1],
							'variants' : variants,
							'fields' : fields,
						}
					],
				})
				assert ret['code'] // 100 == 4
				assert ret['message'] == 'EPRODUCTUNKNOWN'

	"""
	shop; is making an order with an empty required field forbidden
	disabled until we resolve how to handle other types (int 0 clashes)
	"""
	def DISABLED_SEE_OTHERTYPES_DISCUSSION_test_shop_known_order_empty_required_field(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempKnownClient(self, email) as created_client:
			with TempProducts(self, catalogue) as created_products:
				# set up a product
				product = None
				for p in created_products:
					if p['name'] == PRODUCTS['complex'].render()['name']:
						product = p
				assert product

				variants = []
				for variant in product['variants']:
					variants.append({
						'name': variant['name'],
						'value': variant['options'][0]['slug'],
					})

				seen_required_field = False
				fields = []
				for field in product['fields']:
					if field['required']:
						seen_required_field = True
					fields.append({
						'name': field['name'],
						'value': f"",	# empty
					})
				assert seen_required_field

				# order but reverse the handle
				try:
					self.shop_order({
						'email': email,
						'products': [
							{
								'product_handle': product['handle'],
								'variants': variants,
								'fields': fields,
							}
						],
					})
				except OrderNotPossibleError as e:
					pass
				else:
					raise Unexpected('expected an error here')


	"""
	shop; can we make an order as an unknown client, and does turn this unknown client
	into a known client with the proper data
	"""
	def test_shop_does_order_make_unknown_known(self):
		email = random_email()
		catalogue = [ PRODUCTS['simple'].render(), PRODUCTS['complex'].render() ]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:
				product = None
				for p in created_products:
					if p['name'] == PRODUCTS['simple'].render()['name']:
						product = p
				assert product

				self.shop_order({
					'email' : email,
					'products' : [
						{
							'product_handle' : product['handle'],
						}
					],
				})

				# okay, can we find the client now
				_client = self.admin_client_find(email)

				# and does it have the correct values
				assert _client['email'] == email	# our supplied email
				assert _client['can_order_from'] == FALLBACK_CAN_ORDER_FROM


	"""
	a nominal order by an unknown client
	"""
	def test_shop_unknown_nominal_order(self):
		email = random_email()
		catalogue = [PRODUCTS['simple'].render(), PRODUCTS['complex'].render()]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:


				ordered_products, presumed_total = products_select_carthesian(created_products)

				order = {
					'email' : email,
					'products': ordered_products,
				}

				quote = self.shop_quote(order)

				# the quote has to match
				assert quote['total'] == presumed_total.total()
				quoted_products = products_strip_from_receipt(quote['products'])
				assert quoted_products == order['products']
				assert 'payment_code' not in quote
				assert 'nonce' not in quote
				assert 'handle' not in quote

				receipt = self.shop_order(order)

				# the receipt has to match
				assert receipt['total'] == presumed_total.total()
				receipt_products = products_strip_from_receipt(receipt['products'])
				assert receipt_products == order['products']
				assert 'payment_code' in receipt
				assert 'handle' in receipt
				assert 'nonce' in receipt

				# check that the payment code is valid
				assert structured_comment(receipt['payment_code']) == True

	"""
	a nominal order by an unknown client with extra garbage in the order should be accepted
	"""
	def test_shop_unknown_nominal_order_extra_garbage_accepted(self):
		email = random_email()
		catalogue = [PRODUCTS['simple'].render(), PRODUCTS['complex'].render()]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:


				ordered_products, presumed_total = products_select_carthesian(created_products)

				order = {
					'email' : email,
					'products': ordered_products,
					'drunk': [ 'why', 'would', 'you', 'think', 'that' ],
					'long_day': True,
					'but': 1337,
				}

				quote = self.shop_quote(order)

				# the quote has to match
				assert quote['total'] == presumed_total.total()
				quoted_products = products_strip_from_receipt(quote['products'])
				assert quoted_products == order['products']

				receipt = self.shop_order(order)

				# the receipt has to match
				assert receipt['total'] == presumed_total.total()
				receipt_products = products_strip_from_receipt(receipt['products'])
				assert receipt_products == order['products']

	"""
	test if a voucher can be created and is available for use
	"""
	def test_shop_voucher(self):
		catalogue = [PRODUCTS['simple'].render(), PRODUCTS['complex'].render()]

		with TempProducts(self, catalogue) as created_products:
			relevant_product = created_products[0]
			with TempVoucher(self, relevant_product['category']) as voucher:
				assert voucher['applicable_to'] == relevant_product['category']
				assert voucher['used'] == False

	"""
	test if a voucher can be used and is marked as such
	"""
	def test_shop_unknown_voucher_is_used(self):
		email = random_email()
		catalogue = [PRODUCTS['simple'].render()]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:
				relevant_product = created_products[0]
				with TempVoucher(self, relevant_product['category']) as voucher:

					# voucher should show up now as free-to-use for admin
					used_voucher = self.admin_voucher_get(voucher['handle'])
					assert used_voucher['code'] == voucher['code']
					assert used_voucher['used'] == False

					# voucher should show up now as free-to-use for client
					checked_voucher = self.shop_voucher_check(voucher['code'])
					assert checked_voucher['found'] == True
					assert checked_voucher['applicable_to'] == relevant_product['category']

					# get an order going, only one simple product
					ordered_products, _ = products_select_carthesian(created_products)

					# order something (only one thing here) and use a voucher for that thing
					order = {
						'email': email,
						'products':  ordered_products,
						'vouchers': [voucher['code']],
					}

					# get a quote, total should be zero and that thing should have a voucher
					quote = self.shop_quote(order)
					assert quote['vouchers'][0] == voucher['code']
					assert quote['products'][0]['voucher'] == voucher['code']
					assert quote['products'][0]['price'] == {}
					assert quote['total'] == 0

					# order this, total should be zero and that thing should have a voucher
					receipt = self.shop_order(order)
					assert receipt['vouchers'][0] == voucher['code']
					assert receipt['products'][0]['voucher'] == voucher['code']
					assert receipt['products'][0]['price'] == {}
					assert receipt['total'] == 0

					# voucher should show up now as used for admin
					used_voucher = self.admin_voucher_get(voucher['handle'])
					assert used_voucher['code'] == voucher['code']
					assert used_voucher['used'] == True

					# voucher should show up now as used for client
					checked_voucher = self.shop_voucher_check(voucher['code'])
					assert checked_voucher['found'] == False
					assert checked_voucher['applicable_to'] == ''

	"""
	test if a voucher cannot be double spent
	"""
	def test_shop_unknown_voucher_double_spend(self):
		email = random_email()
		catalogue = [PRODUCTS['simple'].render()]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:
				relevant_product = created_products[0]
				with TempVoucher(self, relevant_product['category']) as voucher:

					# get an order going, only one simple product
					ordered_products, _ = products_select_carthesian(created_products)

					# order something (only one thing here) and use a voucher for that thing
					order = {
						'email': email,
						'products':  ordered_products,
						'vouchers': [voucher['code']],
					}

					# order this, total should be zero and that thing should have a voucher
					receipt = self.shop_order(order)
					assert receipt['total'] == 0

					# now try as another client, leave the rest standing
					email = random_email()
					with TempUnknownClient(self, email):
						# order something (only one thing here) and use a voucher for that thing
						order = {
							'email': email,
							'products':  ordered_products,
							'vouchers': [voucher['code']],
						}
						# quote/order this, total should be zero and that thing should have a voucher
						quote = self.shop_quote(order)
						assert 'code' in quote
						assert quote['code'] // 100 == 4
						assert quote['message'] == 'EVOUCHERBAD'
						assert 'expired' in quote['extra']
						assert quote['extra']['expired'] == [voucher['code']]
						receipt = self.shop_order(order)
						assert 'code' in receipt
						assert receipt['code'] // 100 == 4
						assert receipt['message'] == 'EVOUCHERBAD'
						assert 'expired' in receipt['extra']
						assert receipt['extra']['expired'] == [voucher['code']]

	"""
	test if a nonsensical voucher is handled well
	"""
	def test_shop_unknown_voucher_nonsense(self):
		email = random_email()
		catalogue = [PRODUCTS['simple'].render()]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:
				relevant_product = created_products[0]
				with TempVoucher(self, relevant_product['category']) as voucher:

					# get an order going, only one simple product
					ordered_products, _ = products_select_carthesian(created_products)

					nonsense = 'utter_nonsense'

					# order something (only one thing here) and use a voucher for that thing
					order = {
						'email': email,
						'products':  ordered_products,
						'vouchers': [nonsense],
					}
					# quote/order this, total should be zero and that thing should have a voucher
					quote = self.shop_quote(order)
					assert 'code' in quote
					assert quote['code'] // 100 == 4
					assert quote['message'] == 'EVOUCHERBAD'
					assert 'notfound' in quote['extra']
					assert quote['extra']['notfound'] == [nonsense]
					receipt = self.shop_order(order)
					assert 'code' in receipt
					assert receipt['code'] // 100 == 4
					assert receipt['message'] == 'EVOUCHERBAD'
					assert 'notfound' in receipt['extra']
					assert receipt['extra']['notfound'] == [nonsense]

	"""
	test if a valid voucher for a product that is not in the order is handled well
	"""
	def test_shop_unknown_voucher_superfluous(self):
		email = random_email()
		catalogue = [PRODUCTS['simple'].render(), PRODUCTS['complex'].render()]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:
				relevant_product = created_products[0]
				with TempVoucher(self, relevant_product['category']) as voucher:
					# get an order going, only the other product
					ordered_products, _ = products_select_carthesian([created_products[1]])
					# order something (only one thing here) and use a voucher for that thing
					order = {
						'email': email,
						'products':  ordered_products,
						'vouchers': [voucher['code']],
					}
					# quote/order this, total should be zero and that thing should have a voucher
					quote = self.shop_quote(order)
					assert 'code' in quote
					assert quote['code'] // 100 == 4
					assert quote['message'] == 'EVOUCHERBAD'
					assert 'superfluous' in quote['extra']
					assert quote['extra']['superfluous'] == [voucher['code']]
					receipt = self.shop_order(order)
					assert 'code' in receipt
					assert receipt['code'] // 100 == 4
					assert receipt['message'] == 'EVOUCHERBAD'
					assert 'superfluous' in receipt['extra']
					assert receipt['extra']['superfluous'] == [voucher['code']]


	"""
	ensure vouchers go to costliest applicable items
	"""
	def test_shop_unknown_voucher_cost_logic(self):
		email = random_email()
		catalogue = [PRODUCTS['simple'].render(), PRODUCTS['complex'].render()]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:
				relevant_product = created_products[1]
				with TempVoucher(self, relevant_product['category']) as voucher:
					ordered_products, presumed_total = products_select_carthesian(created_products)

					costs_per_product = {}
					for item in ordered_products:
						# relevant product description
						product = list(
							filter(
								lambda p: p['handle'] == item['product_handle'],
								created_products
							)
						)[0]
						base_price = Price(product['price'])
						for variant in item['variants']:
							# look up the variant in the description
							chosen_variant = list(
								filter(
									lambda v: v['name'] == variant['name'],
									product['variants']
								)
							)[0]
							# and that variant's chosen option
							chosen_option = list(
								filter(
									lambda o: o['slug'] == variant['value'],
									chosen_variant['options']
								)
							)[0]
							# so we can modify the price
							base_price += Price(chosen_option['price_modifier'])

						key = item['product_handle']
						if key not in costs_per_product:
							costs_per_product[key] = []
						costs_per_product[key].append(base_price)

					# we have a large number of items on order, and a voucher for
					# a complex one, so substract the costliest instance of the complex
					# ones from our estimated cost, and that should match
					presumed_total -= sorted(
						costs_per_product[relevant_product['handle']],
						key=lambda c: c.total()
					)[-1]

					order = {
						'email' : email,
						'products': ordered_products,
						'vouchers': [voucher['code']]
					}

					quote = self.shop_quote(order)

					# the quote has to match
					assert quote['total'] == presumed_total.total()
					quoted_products = products_strip_from_receipt(quote['products'])
					assert quoted_products == order['products']

					receipt = self.shop_order(order)

					# the receipt has to match
					assert receipt['total'] == presumed_total.total()
					receipt_products = products_strip_from_receipt(receipt['products'])
					assert receipt_products == order['products']


	"""
	appendable template for nominal order by unknown client
	"""
	def test_shop_unknown_nominal_order_template(self):
		email = random_email()
		catalogue = [PRODUCTS['simple'].render(), PRODUCTS['complex'].render()]

		with TempUnknownClient(self, email):
			with TempProducts(self, catalogue) as created_products:


				ordered_products, presumed_total = products_select_carthesian(created_products)

				order = {
					'email' : email,
					'products': ordered_products,
				}

				quote = self.shop_quote(order)

				# the quote has to match
				assert quote['total'] == presumed_total.total()
				quoted_products = products_strip_from_receipt(quote['products'])
				assert quoted_products == order['products']

				receipt = self.shop_order(order)

				# the receipt has to match
				assert receipt['total'] == presumed_total.total()
				receipt_products = products_strip_from_receipt(receipt['products'])
				assert receipt_products == order['products']


	"""
	runs last, explode if the tests didn't clean up after themselves
	"""
	def test_zzz_database_remains(self):
		with fr1ckets.app.app_context():
			fr1ckets.app.logger.setLevel(logging.DEBUG)
			fr1ckets.model.setup.db_setup()

			clients = admin_client_list()
			assert len(clients) == 1	# the fallback client

			products = product_list()
			assert len(products) == 0

			vouchers = voucher_list()
			assert len(vouchers) == 0

if __name__ == '__main__':
	unittest.main()
