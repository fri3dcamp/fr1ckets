# WSGI entry point
# fr1ckets, the fri3dcamp devs, 2022
from fr1ckets import app

if __name__ == '__main__':
	app.run('0.0.0.0', port=8080, debug=True)
