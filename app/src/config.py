# description of file
# fr1ckets, the fri3dcamp devs, 2022
import os

DB_PATH = os.getenv('APP_DB_PATH') or '/db/unconfigured.db'
DB_SQL_PATH = os.getenv('APP_DB_SQL_PATH') or '/src/fr1ckets.sql'
SECRET_KEY = os.getenv('APP_SECRET_KEY') or 'onetwothreefourfive'
SESSION_EXPIRING_DAYS = int(os.getenv('SESSION_EXPIRING_DAYS', '14'))
REMINDER_TIMEOUT_DAYS = int(os.getenv('REMINDER_TIMEOUT_DAYS', '7'))
CELERY_BROKER_URL = 'redis://queue'
MAIL_ADDRESS_FROM = os.getenv('MAIL_ADDRESS_FROM') or 'shop@example.org'
MAIL_SERVER = os.getenv('MAIL_SERVER') or 'smtp.example.org'
MAIL_PORT = int(os.getenv('MAIL_PORT', '25'))
MAIL_USERNAME = os.getenv('MAIL_USERNAME') or MAIL_ADDRESS_FROM
MAIL_PASSWORD = os.getenv('MAIL_PASSWORD') or 'hunter2'
MAIL_USE_TLS = bool(int(os.getenv('MAIL_USE_TLS', '0')))
SHOP_CURRENTLY_OPEN = bool(int(os.getenv('SHOP_CURRENTLY_OPEN', '0')))
OUR_BANK_ACCOUNT = os.getenv('OUR_BANK_ACCOUNT') or 'BE01-1234-5678-9'
BASIC_AUTH_USERNAME = os.getenv('APP_ORGA_USERNAME') or 'orga'
BASIC_AUTH_PASSWORD = os.getenv('APP_ORGA_PASSWORD') or 'orga_password'
ORGA_ADMIN_EMAIL = os.getenv('ORGA_ADMIN_EMAIL') or 'admin@something.invalid'
ORGA_ADMIN_PASSWORD = os.getenv('ORGA_ADMIN_PASSWORD') or 'admin_password'
